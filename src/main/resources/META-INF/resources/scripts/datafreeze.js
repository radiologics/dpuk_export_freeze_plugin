/*
 * This is a placeholder for your JavaScript, if needed.
 */
var dataTypes = new Array();
var dataFreeze = {
	groups:[],
	visits:[],
	sessionfilters:[]
};

function prependLoader(div_id,msg){
	//if(div_id.id==undefined){
	//	var div=document.getElementById(div_id);
	//}else{
	//	var div=div_id;
	//}
	//var loader_div = document.createElement("div");
	//loader_div.innerHTML=msg;
	//div.parentNode.insertBefore(loader_div,div);
	//return new XNATLoadingGIF(loader_div);
	return ;
};

function xnatsubjectDataDataFreezeController() {
	var myComponent = {};
	myComponent["xnat:demographicData/age"]=true;
	myComponent["xnat:demographicData/gender"]=true;
	myComponent["xnat:demographicData/handedness"]=true;
	myComponent["xnat:demographicData/education"]=true;
	return myComponent;
};


function constructJSONStr() {
	var someDataTypeSelected = false;
	for	(var i= 0; i< dataTypes.length; i++) {
	    if (dataTypes[i] != "xnat:subjectData") {
		    var dataTypeSpecificFunctionName = dataTypes[i].replace(":","")+"HaveIBeenSelected";
			var dataTypeIsSelected = window[dataTypeSpecificFunctionName]();
			if (dataTypeIsSelected) {
				someDataTypeSelected = true;
				break;
			}
		}
     }
    if (!someDataTypeSelected) {
		xmodal.alert("No datatype has been selected to be shared. This may happen if there are no datatypes available for the group and visit combination or if there are unrecognized scan-types which have not been mapped.");
		return false;
	}
	for	(var i= 0; i< dataTypes.length; i++) {
		var dataTypeSpecificFunctionName = dataTypes[i].replace(":","")+"DataFreezeController";
		var dataTypeRepresentation = window[dataTypeSpecificFunctionName]();
		if (dataTypes[i] == "xnat:subjectData") {
			dataFreeze['demographics'] = dataTypeRepresentation;
		}else if (dataTypes[i] == "xnat:petSessionData") {
			for (var j=0; j<dataTypeRepresentation.length;j++ ) {
			   dataFreeze.sessionfilters.push(dataTypeRepresentation[j]);
			}
		}else {
			if (!(dataTypeRepresentation == null)) {
			 dataFreeze.sessionfilters.push(dataTypeRepresentation);
			}
		}

   }
   var inputEntry = document.createElement("input");
   inputEntry.type="hidden";
   inputEntry.name="json_result";
   inputEntry.value=JSON.stringify(dataFreeze);
   var divElement=document.getElementById("result_json_div");
   divElement.appendChild(inputEntry);
   return true;
};

function removeChild(parentId,childId) {
	var parentElement = document.getElementById(parentId);
	if (!(parentElement == null)) {
		var childElement = document.getElementById(childId);
		if (!(childElement == null)) {
			parentElement.removeChild(childElement);
		}
	}
};