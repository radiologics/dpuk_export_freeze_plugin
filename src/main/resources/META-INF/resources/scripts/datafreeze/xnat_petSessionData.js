<!-- datafreeze/petSessionData.js -->
var dpukPETScanTypes = ["Select","AC","NAC","Map"]
var xnatpetSessionDatas = [];
var totalPetTracers = 0;

var xnatpetSessionDataController={
	  populate: function () {
	         for (var i=0; i<xnatpetSessionDatas.length;i++) {
				var tracer = xnatpetSessionDatas[i].tracers[0];
			    var checkBoxId = this.datatype+ "_experimentSelectionOpt_"+i;
				var checkBox = document.getElementById(checkBoxId);
				if (!(checkBox == null) && checkBox.checked) { //PET Session of a specific tracer has been selected
					xnatpetSessionDatas[i].include=1;
					for (var j=0; j<xnatpetSessionDatas[i].totalscans;j++) {
						var scancheckBoxId = this.datatype +"_"+tracer+"_Opt["+j+"][0]";
						var scancheckBox = document.getElementById(scancheckBoxId);
						if (!(scancheckBox == null) && scancheckBox.checked) {
						    xnatpetSessionDatas[i].scantypes.push(scancheckBox.value);
							var dpukScanTypeSelectedId = this.datatype +"_"+tracer+"_Opt["+j+"][1]";
							var selectBox = document.getElementById(dpukScanTypeSelectedId);
							var mapKey = scancheckBox.value;
							if ((typeof selectBox === 'undefined') || selectBox == null ) {
								var mapValue = mapKey;
							}else {
								var mapValue = selectBox.options[selectBox.selectedIndex].text
							}
							xnatpetSessionDatas[i].mappings[mapKey]=mapValue;
						}
					}
				}else {
					xnatpetSessionDatas[i].include=0;
				}
			}
		}
};

function xnatpetSessionDataHaveIBeenSelected() {
   var iHaveBeenSelected = false;
   var xsiType = "xnat:petSessionData";
   for	(var index = 0; index < totalPetTracers; index++) {
	   var idStr = xsiType+ "_experimentSelectionOpt_" + index;
	   var inputElement = document.getElementById(idStr);
	   if (inputElement != null) {
		   if (inputElement.checked) {
			 iHaveBeenSelected = true;
		   }
	   }
   }
   return iHaveBeenSelected;
};


function xnatpetSessionDataDataFreezeController() {
	xnatpetSessionDataController.populate();
	var xnatpetSessionArray=[];
	for (var i=0; i<xnatpetSessionDatas.length;i++) {
		   if (xnatpetSessionDatas[i].include == 1) {
			  xnatpetSessionDatas[i].datatype=xnatpetSessionDataController.datatype;
			  xnatpetSessionDatas[i].resources=xnatpetSessionDataController.resources;
			  xnatpetSessionDatas[i].filtertype=xnatpetSessionDataController.filtertype;
			  xnatpetSessionArray.push(xnatpetSessionDatas[i]);
		   }
	}
   return xnatpetSessionArray;
};

function PetSessionDataFreezeManagerInit(project,groups,visits,xsiType,experimentName){
  var datafreezeManager = new PetSessionDataFreezeManager(project,groups,visits,xsiType,experimentName);
  xnatpetSessionDataController.datatype=xsiType;
  xnatpetSessionDataController.filtertype="PETSessions";
  xnatpetSessionDataController.resources=["DICOM"];
  xnatpetSessionDataController.fields={};
  datafreezeManager.populatePETData();
};

function	populateSelectWithDPUKPETScanType(selectEntry, xsiType, index,tracer, j, scanType) {
			for	(var iindex = 0; iindex < dpukPETScanTypes.length; iindex++) {
				var option = document.createElement("option");
				if (iindex==0) {
					option.selected=true;
				}
				option.value=dpukPETScanTypes[iindex];
				option.innerHTML = dpukPETScanTypes[iindex];
				selectEntry.appendChild(option);
			}
			selectEntry.onchange = function(){updatePETRow(this,xsiType,index,tracer,scanType,j);};

		};

function	matchesDPUKPETScanType(scanType) {
			var matches = 0;
			var iindex=0;
			for	(var iindex = 0; iindex < dpukPETScanTypes.length; iindex++) {
				if (scanType == dpukPETScanTypes[iindex]) {
					matches = 1;
					break;
				}
			}
			return matches;
		};

function updatePETRow(selectElement,xsiType,tracerIndex,tracerName,scanType,scanRow) {
			//If selected Index is not 0, update the row with a checkbox
			//Update the section check box to checked
			var divId = getDivIdForTracerAndScan(xsiType,tracerName,scanRow);
			if (selectElement.selectedIndex == 0) {
				var inputID = xsiType+"_"+tracerName+"_Opt["+scanRow+"][0]";

//			    var inputID = xsiType+"_Opt["+scanRow+"][0]";
                var thisInput = document.getElementById(inputID);
                thisInput.disabled = "disabled";
                thisInput.checked = false;
				/*
				removeCheckBox(xsiType,tracerName,scanRow);
				addPETHrefElement(xsiType,tracerName,scanRow,divId);
				*/
			}else {
			    // toggle scan row selector
				var inputID = xsiType+"_"+tracerName+"_Opt["+scanRow+"][0]";

//                var inputID = xsiType+"_Opt["+scanRow+"][0]";
                var thisInput = document.getElementById(inputID);
                thisInput.disabled = false;
                thisInput.checked = true;

                // toggle PET session container selector
				var spanId =getSpanIdForTracer(xsiType, tracerName);
				removeChild(spanId,getIExclamationId(xsiType,tracerName));
				addCheckBoxForTracerBySpanId(spanId,xsiType, tracerName,tracerIndex);

				/*
                var divElement = document.getElementById(divId);
                removeChild(divId,getIdForTracerScanRowHref(xsiType,tracerName,scanRow));
                addCheckBoxForPETScan(xsiType, tracerName, scanRow,scanType, divElement);
                */
			}
};

function addScanTypeTooltip (parentElement){
    var hrefElement = document.createElement("a");
    hrefElement.setAttribute("data-toggle","tooltip");
    hrefElement.setAttribute("title","What does this mean?");
    var iElement = document.createElement("i");
    iElement.setAttribute("class","fa  fa-question-circle");
    hrefElement.appendChild(iElement);
    hrefElement.setAttribute("onclick","javascript:showScanHelp()");
    parentElement.appendChild(hrefElement);
}

function	addPETHrefElement(xsiType,tracer,j,divId) {
	var divE = document.getElementById(divId);
	addPETHref(xsiType,tracer,j,divE);
};

function addPETHref(xsiType,tracer,j,divE) {
	var hrefElement = document.createElement("a");
	hrefElement.id = getIdForTracerScanRowHref(xsiType,tracer,j);
	hrefElement.setAttribute("data-toggle","tooltip");
	hrefElement.setAttribute("title","Ignore");
	hrefElement.setAttribute("onclick","javascript:showScanHelp()");
	var iElement = document.createElement("i");
	iElement.setAttribute("class","fa  fa-question-circle");
	hrefElement.appendChild(iElement);
	divE.appendChild(hrefElement);
};

function showScanHelp() {
	xModalMessage('Scan Type Mismatch', 'The scan-types do not match the standardized scan-types in DPUK Central. Please select an appropriate match.');
};


function addCheckBoxForTracerBySpanId(spanId,xsiType, tracerName, tracerIndex) {
	var spanE = document.getElementById(spanId);
    addCheckBoxForTracer(xsiType, tracerName, tracerIndex, spanE);
};

function addCheckBoxForTracer(xsiType, tracerName, tracerIndex, spanElement) {
		var idStr = xsiType+ "_experimentSelectionOpt_"+tracerIndex;
		var inputElement = document.getElementById(idStr);
		if (inputElement == null) {
			var inputElement = document.createElement("input");
			inputElement.type="checkbox";
			inputElement.checked="true";
			inputElement.name=idStr;
			inputElement.id=idStr;
			inputElement.value=tracerName;
			var label=document.createElement("label");
			label.setAttribute("for",idStr);
			spanElement.appendChild(inputElement);
			spanElement.appendChild(label);
		}
};

function addCheckBoxForPETScan(xsiType, tracer, j,scanType, divElement,disabled) {
		var idStr = xsiType+"_"+tracer+"_Opt["+j+"][0]";
		var inputElement = document.getElementById(idStr);
		if (inputElement == null) {
			var spanFilterElement = document.createElement("span");
			spanFilterElement.setAttribute("class","filter__element");
			spanFilterElement.id = "span_filter_element_" + tracer + "_" + j;
			var inputEntry  = document.createElement("input");
			inputEntry.type="checkbox";
			if (disabled) {
			    inputEntry.disabled = "disabled";
			} else {
    			inputEntry.checked="true";
            }
   			var idStr = xsiType+"_"+tracer+"_Opt["+j+"][0]";
			inputEntry.name = idStr;
			inputEntry.id = idStr;
			inputEntry.value = scanType;
			spanFilterElement.appendChild(inputEntry);
			var labelEntry  = document.createElement("label");
			labelEntry.setAttribute("for",idStr);
			spanFilterElement.appendChild(labelEntry);
			divElement.appendChild(spanFilterElement);
	}
}

function getDivIdForTracerAndScan(xsiType,tracer,scanRow){
 var divId = "div_" + xsiType + "_" + tracer + "_" + scanRow;
 return divId;
};

function getSpanIdForTracer(xsiType,tracer) {
  var id = "lead_row_span_" + xsiType + "_" + tracer;
  return id;
};

function getIExclamationId(xsiType,tracer) {
   var id = "lead_row_exclamation_" + xsiType + "_" + tracer;
   return id;
};

function getIdForTracerScanRowHref(xsiType, tracer, j){
  var id = "href_"+xsiType + "_" +tracer+"_"+j;
  return id;
};

function removeCheckBox(xsiType,tracer,j) {
 	var parentId = getDivIdForTracerAndScan(xsiType,tracer,j);
 	var childId = "span_filter_element_" + tracer + "_" + j;
    removeChild(parentId,childId);
};




function PetSessionDataFreezeManager(pID,groups,visits, xsiType,experimentName){
		this.projectID=pID;
		this.groups=groups;
		this.visits=visits;
		this.xsiType=xsiType;
		this.experimentName=experimentName;

		//this.initLoader=prependLoader("waitPanel","Loading PET Session content");

		this.populatePETData=function() {
				//this.initLoader=prependLoader("waitPanel","Loading content");
				//this.initLoader.render();
				this.initPETCallback={
					cache:false,
					success:this.completePETInit,
					failure:this.initPETFailure,
					scope:this
				};
				this.url = serverRoot + '/REST/projects/'+ pID + '/datafreeze?groups=' + groups +'&visits='+visits+'&xsitype='+xsiType+'&format=json';
				YAHOO.util.Connect.asyncRequest('GET',this.url,this.initPETCallback,null,this);
	};

		this.initPETFailure=function(o){
	     //       this.initLoader.close();
	        if (!window.leaving) {
	            xModalMessage('Unable to load', 'Failed to load details for PET Sessions.');
	        }
		};

		this.completePETInit=function(oResponse){
			   //	this.initLoader.close();
				try{
		            this.dataResultSet= eval("(" + oResponse.responseText +")");
		            this.renderPET();
		        }catch(e){
		            xModalMessage('Unable to load', 'Invalid list.<br/><br/>' + e.toString());
		        }
	};
		this.renderPET=function() {
			//var jsonStr="{"ResultSet":{"Result":[{"tracer_name":"FDG","session_counts":"3","series_description":"","type":"AC"},{"tracer_name":"FDG","session_counts":"3","series_description":"","type":"FC"},{"tracer_name":"FDG","session_counts":"3","series_description":"","type":"PC"},{"tracer_name":"PIB","session_counts":"1","series_description":"","type":"AC"},{"tracer_name":"PIB","session_counts":"1","series_description":"","type":"PC"}], "groups": "Control","xsiType": "xnat:petSessiData","displayTitle": "SCAN TYPES","visits": "Visit1,Baseline","totalRecords": "5","project": "TEST1"}}";
			var jsonObj
			var jsonStr = YAHOO.lang.JSON.stringify(this.dataResultSet);
			var parsedResponse = YAHOO.lang.JSON.parse(jsonStr);
			var rowArray = parsedResponse.ResultSet.Result;
			var transposedJSONStr="";
			var scanningTracerName="NULL";
			var scanDetailArray = new Array();
			var numberOfPetSessionWithTracer = 0;
			var tracerDetails=new Array();
			var xsiType_mgmt_div = document.getElementById("inner__content");
			totalPetTracers = rowArray.length;
			for	(var index = 0; index < rowArray.length; index++) {
				var tracer = rowArray[index]["tracer_name"];
				if (index == 0) {
					scanningTracerName = tracer;
				    numberOfPetSessionWithTracer = rowArray[index]["session_counts"];
					var scanType = {"type":rowArray[index]["type"],"series_description":rowArray[index]["series_description"]};
					scanDetailArray.push(scanType);
					if (rowArray.length == 1) {
						var rowsToArray ={"tracer":scanningTracerName,"session_counts":numberOfPetSessionWithTracer,"scans":scanDetailArray};
						tracerDetails.push(rowsToArray);
					}
				}else {
					if (scanningTracerName !== tracer) {
						//New row
						var rowsToArray ={"tracer":scanningTracerName,"session_counts":numberOfPetSessionWithTracer,"scans":scanDetailArray};
						tracerDetails.push(rowsToArray);
						numberOfPetSessionWithTracer = rowArray[index]["session_counts"];
						scanningTracerName = tracer;
						scanDetailArray = new Array();
					}else {
						var scanType = {"type":rowArray[index]["type"], "series_description":rowArray[index]["series_description"]};
						scanDetailArray.push(scanType);

						if (index == (rowArray.length - 1)) {
							var rowsToArray ={"tracer":scanningTracerName,"session_counts":numberOfPetSessionWithTracer,"scans":scanDetailArray};
							tracerDetails.push(rowsToArray);
						}

					}
				}
			}
			for	(var index = 0; index < tracerDetails.length; index++) {

				var tracer = tracerDetails[index]["tracer"];
				var sessionCount = tracerDetails[index]["session_counts"];
				var scansForTracer = tracerDetails[index]["scans"];
				var xnatPetSessionData = {"experimentcount":sessionCount};
				xnatPetSessionData.tracers=[];
				xnatPetSessionData.tracers.push(tracer);
				xnatPetSessionData.scantypes=[];
				xnatPetSessionData.mappings={};
				xnatPetSessionData.fields={};

				xnatPetSessionData.experimentcount=sessionCount;
				xnatPetSessionData.totalscans=scansForTracer.length;
				xnatpetSessionDatas.push(xnatPetSessionData);

  			var containNonStandardDPUKScanTypes = 0;
				var inputEntry, idStr, span;
				for	(var k = 0; k< scansForTracer.length; k++) {
					var jsonResultRowTypeValue = scansForTracer[k]["type"];
					if (matchesDPUKPETScanType(jsonResultRowTypeValue) == 0) {
						containNonStandardDPUKScanTypes = 1;
						break;
					}
				}

				var mainDiv = document.createElement("div");
				mainDiv.setAttribute("class","main__control--wrapper");
				var mainUl = document.createElement("ul");

				var liLeadRow = document.createElement("li");
				liLeadRow.setAttribute("class","lead__row");
				var divFirstCell = document.createElement("div");
				divFirstCell.setAttribute("class","first__cell");
				var spanFilterElement = document.createElement("span");
				var spanId = getSpanIdForTracer(xsiType,tracer);
				spanFilterElement.id = spanId;
				spanFilterElement.setAttribute("class","filter__element");
				divFirstCell.appendChild(spanFilterElement);
				if (containNonStandardDPUKScanTypes == 1) {
					var iExclamation = document.createElement("i");
					iExclamation.id = getIExclamationId(xsiType,tracer);
					iExclamation.setAttribute("class","fa fa-exclamation-triangle");
					spanFilterElement.appendChild(iExclamation);
				}else {
					if (scansForTracer.length == 0 || ((scansForTracer.length == 1) && scansForTracer[0]["type"]=="")) {
						var iExclamation = document.createElement("i");
						iExclamation.setAttribute("class","fa fa-exclamation-triangle");
						spanFilterElement.appendChild(iExclamation);
					}else {
						addCheckBoxForTracer(xsiType, tracer, index,spanFilterElement);
				  }
				}

				liLeadRow.appendChild(divFirstCell);
				var spanMainTitle = document.createElement("span");
				spanMainTitle.setAttribute("class","main__title");

				var textStr = this.experimentName + ' - ' + tracer +' Tracer';
				spanMainTitle.innerHTML=textStr;
				spanMainTitle.setAttribute("class","main__title");
				liLeadRow.appendChild(spanMainTitle);

				if (containNonStandardDPUKScanTypes == 1 ) {
					if (!(scansForTracer.length == 1 && scansForTracer[0]["type"]=="")) {
				    var spanInfo=document.createElement("span");
				    spanInfo.id = "spanInfo_" + xsiType + "_" + tracer;
					spanInfo.setAttribute("class","alert__text pink__text");
					spanInfo.innerHTML = "UNRECOGNIZED SCAN TYPES FOUND";
					liLeadRow.appendChild(spanInfo);

					var spanTooltip = document.createElement("span");
                    spanTooltip.setAttribute("class","alert__text");
                    addScanTypeTooltip(spanTooltip);
                    liLeadRow.appendChild(spanTooltip);
				   }
			    }
				var spanCountNo = document.createElement("span");
				spanCountNo.setAttribute("class","count__no");
				spanCountNo.innerHTML=sessionCount;
				liLeadRow.appendChild(spanCountNo);
				spanCountNo = document.createElement("span");
				spanCountNo.setAttribute("class","count");
				spanCountNo.innerHTML="COUNT:";
				liLeadRow.appendChild(spanCountNo);
				mainUl.appendChild(liLeadRow);


				var listEntryGrayRow=document.createElement("li");
				listEntryGrayRow.setAttribute("class","gray__rows");
				var ulEntry=document.createElement("ul");
				var listEntryTableTitleRow=document.createElement("li");
				listEntryTableTitleRow.setAttribute("class","table__title--row");
				var col1span=document.createElement("span");
				col1span.setAttribute("class","first__title column__title third");
				col1span.innerHTML="SCAN TYPES";
				listEntryTableTitleRow.appendChild(col1span);
				var col2span=document.createElement("span");
				col2span.setAttribute("class","column__title third");
				col2span.innerHTML="DESCRIPTION";
				listEntryTableTitleRow.appendChild(col2span);
				var col3span=document.createElement("span");
				col3span.setAttribute("class","column__title third");
				col3span.innerHTML="DPUK SCAN TYPE";
				listEntryTableTitleRow.appendChild(col3span);
				ulEntry.appendChild(listEntryTableTitleRow);


				if (scansForTracer.length == 0 || ((scansForTracer.length == 1) && scansForTracer[0]["type"]=="")) {
						var lEntryStdRow=document.createElement("li");
						lEntryStdRow.setAttribute("class","standard__row");
						var divEntryFirstCell = document.createElement("div");
						divEntryFirstCell.setAttribute("class","no-results");
						var span=document.createElement("span");
						span.innerHTML="None Found";
						divEntryFirstCell.appendChild(span);
						lEntryStdRow.appendChild(divEntryFirstCell);
						ulEntry.appendChild(lEntryStdRow);
				}else {
					for (var j=0;j<scansForTracer.length;j++) {
						var scanType=scansForTracer[j]["type"];
						var doesNotMatch=matchesDPUKPETScanType(scanType);

						var scanDescription=scansForTracer[j]["series_description"];
						var lEntryStdRow=document.createElement("li");
						lEntryStdRow.setAttribute("class","standard__row");
						var divEntryFirstCell = document.createElement("div");
						var divEntryFirstCellIdStr = getDivIdForTracerAndScan(xsiType,tracer,j);
						divEntryFirstCell.id = divEntryFirstCellIdStr;
						divEntryFirstCell.setAttribute("class","first__cell");

						if (doesNotMatch==0) {
							// addPETHref(xsiType,tracer,j,divEntryFirstCell);
							addCheckBoxForPETScan(xsiType,tracer,j,scanType,divEntryFirstCell,true); // add disabled checkbox
						}else {
							addCheckBoxForPETScan(xsiType, tracer,j, scanType, divEntryFirstCell,false);
					     }

							lEntryStdRow.appendChild(divEntryFirstCell);
							var divEntryStdCellThird = document.createElement("div");
							divEntryStdCellThird.setAttribute("class","standard__cell third");
							var span = document.createElement("span");
							if (doesNotMatch == 0) {
								span.setAttribute("class","pink__text");
							}
							span.innerHTML = scanType;
							divEntryStdCellThird.appendChild(span);
							lEntryStdRow.appendChild(divEntryStdCellThird);

							divEntryStdCellThird = document.createElement("div");
							divEntryStdCellThird.setAttribute("class","standard__cell third");
							span = document.createElement("span");
							if (doesNotMatch == 0) {
								span.setAttribute("class","pink__text");
							}
							span.innerHTML = scanDescription;
							divEntryStdCellThird.appendChild(span);
							lEntryStdRow.appendChild(divEntryStdCellThird);

							divEntryStdCellThird = document.createElement("div");
							divEntryStdCellThird.setAttribute("class","standard__cell third no-border");
							span = document.createElement("span");
							if (doesNotMatch == 0) {
								span.setAttribute("class","pink__text");
								var selectEntry  = document.createElement("select");
								selectEntry.setAttribute("class","select__list");
								idStr = xsiType+"_"+tracer+"_Opt["+j+"][1]";
								selectEntry.name = idStr;
								selectEntry.id = idStr;
								populateSelectWithDPUKPETScanType(selectEntry, xsiType, index,tracer, j,scanType);
								span.appendChild(selectEntry);
							}else {
								span.innerHTML = scanType;
							}
							divEntryStdCellThird.appendChild(span);
							lEntryStdRow.appendChild(divEntryStdCellThird);

							ulEntry.appendChild(lEntryStdRow);
						}
				}

				listEntryGrayRow.appendChild(ulEntry);
				mainUl.appendChild(listEntryGrayRow);
				mainDiv.appendChild(mainUl);
				xsiType_mgmt_div.appendChild(mainDiv);
			}
		};

}
