var centralhub = "";

var remoteConnectionJSON; //dont access this before credentialAccepted is called

function showRemoteLoginModal(JsonFileName,localDataFreezeProject,userLogin,centralhublocation){
				centralhub=centralhublocation;
                xmodal.open({
                    title: 'Authenticate on DPUK Central Hub',
                    width: 400,
                    height: 250,
                    scroll: false,
                    overflow: true,
                    esc: false,
                    enter: false,
                    content: $('#credentials_div').html(),
                    okLabel: 'Login',
                    okAction: function( obj ){
					     xmodal.loading.open({scroll: false,width: 380,height:100,title:'Searching For Cohorts on DPUK Central Hub',content:'In order to share a data freeze, you must have a study<br> registered on DPUK Central.<br>'+$('#xmodal-loading').html()});
                         var $loginModal = obj.$modal;
                         getDPUKCentralHubJSESSION(JsonFileName,localDataFreezeProject,userLogin,$loginModal.find('input.username').val() ,$loginModal.find('input.password').val(),centralhublocation );
                    },
                    okClose: false
                });
};

function getDPUKCentralHubJSESSION(JsonFileName,localDataFreezeProject,userLogin,username,password,centralhublocation) {
	var exporturl = serverRoot + '/data/remote/connection'+'?XNAT_CSRF='+csrfToken ;
	var inputData={"url":centralhublocation,"username":username,"password":password};
	$.ajax({
	  url:exporturl,
	  type:"POST",
	  data:JSON.stringify(inputData),
	  contentType:"application/json",
	  dataType:"json",
	  success: function(data){credentialAccepted(JsonFileName,localDataFreezeProject,userLogin,data);},
	  error: ajaxLoginFailed
	});
};


	function credentialAccepted(JsonFileName,localDataFreezeProject,userLogin,rConnectionJSON) {
	   var connection = {'jsessionid':rConnectionJSON.jsessionid,'url':rConnectionJSON.url,'username':rConnectionJSON.username};
	   remoteConnectionJSON={'connection':connection};
	   showCohorts(JsonFileName,localDataFreezeProject,userLogin);

	};
	
	function deleteCallFailed(o) {
		xmodal.loading.close();
		if (!window.leaving) {
		    xModalMessage('Unable to Delete Data Freeze', 'Unable to Delete Data Freeze');
		}

	};
	function ajaxCallFailed(o) {
		xmodal.loading.close();
		if (!window.leaving) {
		    xModalMessage('Error connecting to DPUK Central Hub', 'Error connecting to DPUK Central Hub');
		}

	};

	function ajaxLoginFailed(o) {
		xmodal.loading.close();
		if (!window.leaving) {
			//alert(o.status + " " + o.statusText + " " +  o.responseText);
			if (o.status == "401") {
			    xModalMessage('Login Failed', 'Invalid username/password combination. Please try again.');
			}else if (o.status == "404") {
			    xModalMessage('Login Failed', 'Unable to connect to DPUK Central Hub.');
			}else {
			    xModalMessage('Login Failed', 'Invalid username/password combination. Please try again.');
			}
		}
	};


	function showCohorts(JsonFileName,localDataFreezeProject,userLogin) {
		var cohorturl = serverRoot + '/data/remote/study/filter/false?XNAT_CSRF='+csrfToken ;
		$.ajax({
		  url:cohorturl,
		  type:"POST",
		  data:JSON.stringify(remoteConnectionJSON),
		  contentType:"application/json",
		  dataType:"json",
		  success: function(data) {displayCohorts(JsonFileName,localDataFreezeProject,userLogin,data);},
		  error: ajaxCallFailed
		});

	};

	function resetStudies() {
		$('#cohortSelectionElement').empty();
		var option = new Option("Select", "-1");
		$('#cohortSelectionElement').append($(option));
	};

      function displayCohorts(JsonFileName,localDataFreezeProject,userLogin,studiesJSON) {
		resetStudies();
		var studies = studiesJSON.ResultSet.Result;
		for (var i=0; i<studies.length;i++) {
			var option = new Option(studies[i].name, studies[i].id);
			$('#cohortSelectionElement').append($(option));
		}
		xmodal.loading.close();
		xmodal.close();
		xmodal.open({
			id: 'cohortSelectionModalId',
			title: 'Select Or Create Cohort To Share Into',
			width: 450,
			height: 250,
			scroll: false,
			overflow: true,
			esc: false,
			enter: false,
			content: $('#cohort_selection_div').html(),
			okLabel: 'Certify & Share Into Selected Cohort',
			okAction:	function( obj ){
              var cohortModal = xmodal.getModalObject('cohortSelectionModalId');
              createRemoteProjectAddRelation(JsonFileName,localDataFreezeProject,userLogin,cohortModal);

			},
			okClose: false
		});
	};

	function createRemoteProjectAddRelation(JsonFileName,localDataFreezeProject,userLogin,cohortModal) {
		//Create a Remote Project
		//Add a relation to the selected Cohort
		var studyToRelateTo = cohortModal.__modal.find('#cohortSelectionElement').val();
		if (studyToRelateTo === "-1") {
			cohortModal.__modal.find('#cohortSelectionElement').focus();
			xmodal.alert("Please select a cohort");
		}else {
			var remoteProjectId=XNAT.app.siteId+'DF'+localDataFreezeProject; //<NODE_ID>DF<LOCAL PROJECTID>
			var remoteProjectDescription = 'DataFreeze created on ' + XNAT.data.todaysDate.iso + ' by ' + userLogin +'. Source Project ' + localDataFreezeProject + ' Node: ' + window.location.href.toString().split('/app/')[0] ;
			var remoteProjectName = XNAT.app.siteId + '_DataFreeze_' + localDataFreezeProject;
			var remoteProjectJSON = JSON.parse(JSON.stringify(remoteConnectionJSON));
			remoteProjectJSON.accessibility='private';
			remoteProjectJSON.name=remoteProjectName;
			remoteProjectJSON.description=remoteProjectDescription;
			xmodal.loading.open('Creating remote project.....please wait');
			$.ajax({
			  url:serverRoot + '/data/remote/projects/' + remoteProjectId + '?XNAT_CSRF='+csrfToken,
			  type:"POST",
			  data:JSON.stringify(remoteProjectJSON),
			  contentType:"application/json",
			  dataType:"json",
			  success: function(data) {associateToStudy(JsonFileName,localDataFreezeProject,remoteProjectId,studyToRelateTo);},
			  error: ajaxCallFailed
			});
	  }
	};

	function associateToStudy(JsonFileName,localDataFreezeProject,remoteProjectId,studyToRelateTo) {
		var remoteProjectJSON = remoteConnectionJSON;
		remoteProjectJSON.project1=studyToRelateTo;
		remoteProjectJSON.project2=remoteProjectId;
		remoteProjectJSON.type="freeze";
		$.ajax({
		  url:serverRoot + '/data/remote/projectrelations?XNAT_CSRF='+csrfToken,
		  type:"POST",
		  data:JSON.stringify(remoteProjectJSON),
		  contentType:"application/json",
		  dataType:"json",
		  success: function(data) { setCohortLocally(JsonFileName,localDataFreezeProject,remoteProjectId,studyToRelateTo);},
		  error: ajaxCallFailed
		});
	};

	function setCohortLocally(fileName,localDataFreezeProject,remoteProjectId,studyToRelateTo) {
		 var url = serverRoot + '/data/projects/'+ localDataFreezeProject + '/resources/datafreeze/files/' + fileName +'?XNAT_CSRF='+csrfToken ;
		 $.ajax({url: url, dataType:"json", success:function(data){
														    var localProjectDataFreezeJSON = data;
															localProjectDataFreezeJSON.cohort = studyToRelateTo;
															overwriteJSONs(fileName,localProjectDataFreezeJSON,localDataFreezeProject,remoteProjectId);
			 										},
			 										failure: ajaxCallFailed});

	};

	function getCall(project,filename) {
		var saveurl = serverRoot + '/data/archive/projects/'+project+'/resources/datafreeze/files/'+filename ;
		var params= '?overwrite=true&inbody=true&XNAT_CSRF='+csrfToken;
		return saveurl+params;
	};

	function overwriteJSONs(filename,localProjectDataFreezeJSON,localDataFreezeProject,remoteProjectId) {
			try{
				var sourceProject = localProjectDataFreezeJSON.sproject;
				var destinationProject = localProjectDataFreezeJSON.dproject;
				var requests = Array();
				var jsonString=JSON.stringify(localProjectDataFreezeJSON);
				var url = getCall(sourceProject,filename);
				requests.push($.ajax({type: "PUT",url:url,contentType: "application/json",data: jsonString}));
				url = getCall(destinationProject,filename);
				requests.push($.ajax({type: "PUT",url:url,contentType: "application/json",data: jsonString}));
				var defer = $.when.apply($, requests);
				defer.then(function(){
					redirectToDataFreeze(localDataFreezeProject,remoteProjectId);
					});
			}catch(e){
				xModalMessage('Unable to load', 'Invalid list.<br/><br/>' + e.toString());
			}
	};


	function redirectToDataFreeze(localDataFreezeProject,remoteProjectId) {
		$.ajax({
		  url:serverRoot + '/data/exportremote/project/'+ localDataFreezeProject + '/project/' + remoteProjectId +'?XNAT_CSRF='+csrfToken,
		  type:"POST",
		  data:JSON.stringify(remoteConnectionJSON),
		  contentType:"application/json",
		  dataType:"json",
		  success: function(data) {
			  xmodal.loading.close();
			  xmodal.close();
			  xmodal.alert({
					title: 'Exporting to DPUK Central',
					content: 'Selected experiments are being exported to DPUK Central. An email notification will be sent once export is complete.',
					label: 'OK',
					action: function( obj ){
						  document.location.href=serverRoot + '/data/archive/projects/' + localDataFreezeProject;
					}
				});
			},
		  error: ajaxCallFailed
		});
		//xmodal.loading.close();
		//xmodal.close();
		//document.location.href=serverRoot + '/data/archive/projects/' + localDataFreezeProject;
	};

// BEGIN -  FUNCTIONS FOR DELETING A DATAFREEZE
function deleteLocalDataFreezeProject(localDataFreezeProjectId,sourceProject) {
    xmodal.confirm({
		title: 'Data Freeze Delete',
		content: 'Are you sure you want to delete?',
	    okAction: function( obj ){
			var url = serverRoot + '/data/projects/'+localDataFreezeProjectId+'?removeFiles=true&XNAT_CSRF='+csrfToken ;
			xmodal.loading.open('Deleting  ' + localDataFreezeProjectId + ' .... please wait');
			$.ajax({
			  url:url,
			  type:"DELETE",
			  success: function(data){getSourceProjectDataFreezeResources(localDataFreezeProjectId,sourceProject)},
			  error: deleteCallFailed
			});
		}
	});
};

function getSourceProjectDataFreezeResources(localDataFreezeProjectId,sourceProject) {
	var url = serverRoot + '/data/projects/'+sourceProject+'/resources/datafreeze/files?XNAT_CSRF='+csrfToken ;
	$.ajax({
	  url:url,
	  type:"GET",
	  success: function(data){getSourceProjectDataFreezeResource(data,localDataFreezeProjectId,sourceProject)},
	  error: deleteCallFailed
	});

};

function getSourceProjectDataFreezeResource(oResponse,localDataFreezeProjectId,sourceProject) {
  var jsonStr = JSON.stringify(oResponse);
  var parsedResponse = JSON.parse(jsonStr);
  var dataFreezesArray = parsedResponse.ResultSet.Result;
	if (dataFreezesArray == null || dataFreezesArray.length < 1) {
	   //Nothing to do for now
	}else {
		var i;
		for (i=0; i<dataFreezesArray.length;i++) {
			var fileName = dataFreezesArray[i]["Name"];
			if (fileName.startsWith(localDataFreezeProjectId+"_")) {
				updateFileStatus(dataFreezesArray[i]["URI"],sourceProject);
				break;
			}
		}
	}
};

function updateFileStatus(uri,sourceProject) {
	var url = serverRoot + uri +'?XNAT_CSRF='+csrfToken ;
	$.ajax({
	  url:url,
	  type:"GET",
	  success: function(data){putFile(data,uri,sourceProject)},
	  error: deleteCallFailed
	});


};

function putFile(oResponse,uri,sourceProject) {
	  var parsedResponse = JSON.parse(oResponse);
	  parsedResponse.status='DELETED';
	  var jsonString = JSON.stringify(parsedResponse);
	  var url = serverRoot + uri;
	  var params= '?inbody=true&overwrite=true&XNAT_CSRF='+csrfToken;
      $.ajax({async: false,type: "PUT",url:url+params,contentType: "application/json",data: jsonString, success: function(data){deleteCleanUp(data,sourceProject);},error: deleteCallFailed});

};

function deleteCleanUp(data,sourceProject){
	xmodal.loading.close();
    xmodal.alert({
		title: 'Data Freeze Delete',
		content: 'Deletion successful',
		label: 'OK',
		action: function( obj ){
			document.location.href=serverRoot + '/data/archive/projects/' + sourceProject;
		}
	});
}

// END -  FUNCTIONS FOR DELETING A DATAFREEZE


function createNewCohort(dpukCentralHubURL) {
  window.open(dpukCentralHubURL);
};
