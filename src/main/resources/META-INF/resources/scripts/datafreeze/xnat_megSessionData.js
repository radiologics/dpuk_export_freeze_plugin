var xnatmegSessionDataTotalRows =0;
var xnatmegSessionDataDataFreeze={
	 scantypes:[],
	 mappings:{},
     resources:["DICOM"],
     fields:{},
	  populate: function () {
		 var dataTypeSelectionId = this.datatype+ "_experimentSelectionOpt";
		 var dataTypeSelection = document.getElementById(dataTypeSelectionId);
		 if (!(dataTypeSelection == null) && dataTypeSelection.checked) {
	         for (var i=0; i<xnatmegSessionDataTotalRows;i++) {
				var checkBoxId = this.datatype+"_Opt["+i+"][0]";
				var checkBox = document.getElementById(checkBoxId);
				if (!(checkBox == null ) && checkBox.checked) {
					this.scantypes.push(checkBox.value);
				}
			 }
		  }
	    }
};

function xnatmegSessionDataDataFreezeController() {
  xnatmegSessionDataDataFreeze.populate();
  if (xnatmegSessionDataDataFreeze.scantypes.length == 0){
      return null;
   }else {
 	  return xnatmegSessionDataDataFreeze;
  }
}


function MEGSessionDataFreezeManagerInit(project,groups,visits,xsiType,experimentName,experimentCount){
  var datafreezeManager = new MEGSessionDataFreezeManager(project,groups,visits,xsiType,experimentName,experimentCount);
  xnatmegSessionDataDataFreeze.datatype=xsiType;
  xnatmegSessionDataDataFreeze.filtertype="MEGSessions"
  xnatmegSessionDataDataFreeze.experimentcount=experimentCount;
  datafreezeManager.populateMEGData();
}



function addCheckBoxForDataType(xsiType,span) {
					var inputElement = document.createElement("input");
					inputElement.type="checkbox";
					inputElement.checked="true";
					var idStr = xsiType+ "_experimentSelectionOpt";
					inputElement.name=idStr;
					inputElement.id=idStr;
					inputElement.value=xsiType;
					var label=document.createElement("label");
					label.setAttribute("for",idStr);
					span.appendChild(inputElement);
					span.appendChild(label);
};


function MEGSessionDataFreezeManager(pID,groups,visits, xsiType,experimentName,sessionCount){
		this.projectID=pID;
		this.groups=groups;
		this.visits=visits;
		this.xsiType=xsiType;
		this.experimentName=experimentName;
		this.sessionCount=sessionCount;
		//this.initLoader=prependLoader("waitPanel","Loading MR Session content");

		this.populateMEGData=function() {
				//this.initLoader=prependLoader("waitPanel","Loading content");
				//this.initLoader.render();
				this.initMEGCallback={
					cache:false,
					success:this.completeMEGInit,
					failure:this.initMEGFailure,
					scope:this
				};
				this.url = serverRoot + '/REST/projects/'+ pID + '/datafreeze?groups=' + groups +'&visits='+visits+'&xsitype='+xsiType+'&format=json';
				YAHOO.util.Connect.asyncRequest('GET',this.url,this.initMEGCallback,null,this);
	};

		this.initMEGFailure=function(o){
	            //this.initLoader.close();
	        if (!window.leaving) {
	            xModalMessage('Unable to load', 'Failed to load details for MEG Sessions.');
	        }
		};

		this.completeMEGInit=function(oResponse){
			   	//this.initLoader.close();
				try{
		            this.dataResultSet= eval("(" + oResponse.responseText +")");
		            this.renderMEG();
		        }catch(e){
		            xModalMessage('Unable to load', 'Invalid list.<br/><br/>' + e.toString());
		        }
	};

		this.renderMEG=function() {
			//var jsonStr = '{"ResultSet":{"Result":[{"series_description":"","type":"EPI_REST"},{"series_description":"","type":"Flair"},{"series_description":"","type":"T1"},{"series_description":"","type":"T2"}], "groups": "Baseline","xsiType": "xnat:mrSessionData","visits": "Visit1,Baseline","totalRecords": "4","project": "TEST1"}}';
			var jsonStr = YAHOO.lang.JSON.stringify(this.dataResultSet);
			var parsedResponse = YAHOO.lang.JSON.parse(jsonStr);

			var rowArray = parsedResponse.ResultSet.Result;
			var xsiType_mgmt_div = document.getElementById("inner__content");

				var mainDiv = document.createElement("div");
				mainDiv.setAttribute("class","main__control--wrapper");
				var mainUl = document.createElement("ul");
				mainUl.id = "ul-"+xsiType;
				var liLeadRow = document.createElement("li");
				liLeadRow.setAttribute("class","lead__row");
				var divFirstCell = document.createElement("div");
				divFirstCell.setAttribute("class","first__cell");
				var spanFilterElement = document.createElement("span");
				spanFilterElement.setAttribute("class","filter__element");
				addCheckBoxForDataType(xsiType,spanFilterElement);
				divFirstCell.appendChild(spanFilterElement);
				liLeadRow.appendChild(divFirstCell);
				var spanMainTitle = document.createElement("span");
				spanMainTitle.setAttribute("class","main__title");

				var textStr = experimentName;
				spanMainTitle.innerHTML=textStr;
				spanMainTitle.setAttribute("class","main__title");
				liLeadRow.appendChild(spanMainTitle);

				var spanCountNo = document.createElement("span");
				spanCountNo.setAttribute("class","count__no");
				spanCountNo.innerHTML=sessionCount;
				liLeadRow.appendChild(spanCountNo);
				spanCountNo = document.createElement("span");
				spanCountNo.setAttribute("class","count");
				spanCountNo.innerHTML="COUNT:";
				liLeadRow.appendChild(spanCountNo);
				mainUl.appendChild(liLeadRow);


				var xsiType_mgmt_ul = mainUl;

			var listEntryGrayRow=document.createElement("li");
			listEntryGrayRow.setAttribute("class","gray__rows");
			var ulEntry=document.createElement("ul");
			var listEntryTableTitleRow=document.createElement("li");
			listEntryTableTitleRow.setAttribute("class","table__title--row");
			var col1span=document.createElement("span");
			col1span.setAttribute("class","first__title column__title");
			col1span.innerHTML="SCAN TYPES";
			listEntryTableTitleRow.appendChild(col1span);
//			var col2span=document.createElement("span");
//			col2span.setAttribute("class","column__title");
//			col2span.innerHTML="DESCRIPTION";
//			listEntryTableTitleRow.appendChild(col2span);

			ulEntry.appendChild(listEntryTableTitleRow);
			var rowArray = parsedResponse.ResultSet.Result;
			for	(var index = 0; index < rowArray.length; index++) {
				var jsonResultRowTypeValue = rowArray[index]["type"];
				var jsonResultRowSeriesDescriptionValue = rowArray[index]["series_description"];

				var lEntryStdRow=document.createElement("li");
				lEntryStdRow.setAttribute("class","standard__row");
				var divEntryFirstCell = document.createElement("div");
				divEntryFirstCell.setAttribute("class","first__cell");
					var spanFilterElement = document.createElement("span");
					spanFilterElement.setAttribute("class","filter__element");
					var inputEntry  = document.createElement("input");
					inputEntry.type="checkbox";
					inputEntry.checked="true";

					var idStr = xsiType+"_Opt["+index+"][0]";
					inputEntry.name = idStr;
					inputEntry.id = idStr;
					inputEntry.value = jsonResultRowTypeValue;
					spanFilterElement.appendChild(inputEntry);
					var labelEntry  = document.createElement("label");
					labelEntry.setAttribute("for",idStr);
					spanFilterElement.appendChild(labelEntry);
					divEntryFirstCell.appendChild(spanFilterElement);

			    lEntryStdRow.appendChild(divEntryFirstCell);

				var divEntryStdCellThird = document.createElement("div");
				divEntryStdCellThird.setAttribute("class","standard__cell");
				var span = document.createElement("span");
			    span.innerHTML = jsonResultRowTypeValue;
				divEntryStdCellThird.appendChild(span);
			    lEntryStdRow.appendChild(divEntryStdCellThird);

//				divEntryStdCellThird = document.createElement("div");
//				divEntryStdCellThird.setAttribute("class","standard__cell");
//				span = document.createElement("span");
//			    span.innerHTML = rowArray[index]["series_description"];
//				divEntryStdCellThird.appendChild(span);
//			    lEntryStdRow.appendChild(divEntryStdCellThird);

				ulEntry.appendChild(lEntryStdRow);
		    }
			listEntryGrayRow.appendChild(ulEntry);
			xsiType_mgmt_ul.appendChild(listEntryGrayRow);

			xnatmegSessionDataTotalRows=rowArray.length;
			mainDiv.appendChild(mainUl);
			xsiType_mgmt_div.appendChild(mainDiv);
		};


}


