$(document).ready(function() {
    $(".select__button").click(function() {
        var cblist = $("input[name=filter\\[\\]]");
        cblist.attr("checked", !cblist.attr("checked"));
        $(".select__button span").html($(".select__button span").html() == 'Select All' ? 'Deselect All' : 'Select All');

    });
      $('.first__cell .fa').on({
          mouseover: function(){
              $(this).addClass('fa-times-circle-o');
          },
           mouseleave: function(){
              $(this).removeClass('fa-times-circle-o');
          },
          click: function(){
              $(this).off('mouseleave');
              $(this).off('qtip');
              $(this).removeClass('fa-question-circle');
              $(this).closest('li').addClass('selected');
              $(this).closest('li').children('.standard__cell').children('.select__list').prop("disabled", true);
          }
      }); 
      $('[data-toggle="tooltip"]').tooltip();            
    });