function showSaveDataFreezeModal(userLogin){
    xmodal.open({
        title: 'Save Data Freeze',
        width: 600,
        height: 400,
        scroll: false,
        overflow: true,
        esc: false,
        enter: false,
        okLabel: 'Save Data Freeze',
        okAction: function(){
            xmodal.loading.open({scroll: false,width: 400,height:100,title:'Saving Data Freeze....',content:'All selected data is being copied into a staging project on this DPUK Node.<br>'+$('#xmodal-loading').html()});
            shareData(userLogin);
        },
        okClose: false,
        template: $('#datafreeze-save')
    });
};


function shareData(userLoginId) {
    var shareDataManagerObj = new ShareDataManager(userLoginId);
    shareDataManagerObj.createProject();
};

function ShareDataManager(userLoginId){
	var projectID = document.getElementById("xnat:projectData/ID").value;
 	var projectName = document.getElementById("xnat:projectData/name").value;
 	var projectDescription = document.getElementById("xnat:projectData/description").value;
	var projectType='freeze';
	var userLoginId = userLoginId;

		this.createProject=function() {
				this.initCallback={
					cache:false,
					success:this.completeProjectInit,
					failure:this.initProjectCreateFailure,
					scope:this
				};
				this.url = serverRoot + '/data/projects/'+ projectID ;
				this.params = '?xnat:projectData/type='+ projectType + '&accessibility=private' + '&name=' + encodeURIComponent(projectName) +'&description='+encodeURIComponent(projectDescription)+'&XNAT_CSRF='+csrfToken;
				YAHOO.util.Connect.asyncRequest('PUT',this.url+this.params,this.initCallback,null,this);
  		};

		this.initProjectCreateFailure=function(o){
	        if (!window.leaving) {
	            xmodal.close(); // close "Loading" modal
	            xmodal.open({
	                width: 600,
                    height: 400,
                    title: 'Unable to save data freeze',
                    content: o.responseText + " (" + o.status + " " + o.statusText +")",
                    scroll: false,
                    overflow: true,
                    esc: false,
                    enter: false,
                    buttons: {
                        ok: {
                            label: 'OK',
                            isDefault: true,
                            close: true,
                            action: function(){
                                xmodal.closeAll();
                            }
                        }
                    }
	            });
	        }
		};

		this.completeProjectInit=function(oResponse){
				try{
		            this.setDestinationProject();
		        }catch(e){
		            xModalMessage('Unable to load', 'Invalid list.<br/><br/>' + e.toString());
		        }
    	};

		this.setDestinationProject=function() {
			var jsonStr = document.getElementById('userSelection').value;
			var jsonObj = YAHOO.lang.JSON.parse(jsonStr);
			jsonObj.dproject=projectID;
			delete jsonObj['subjectcount'];
			jsonObj.createdon=XNAT.data.todaysDate.iso;
			jsonObj.status="Not Shared";
			jsonObj.createdby=userLoginId;
			jsonObj.node=window.location.href.toString().split('/app/')[0];
			this.exportProject(jsonObj);
		};

		this.exportProject=function(userSelectionJsonObj) {
				var jsonString=JSON.stringify(userSelectionJsonObj);
				this.exporturl = serverRoot + '/data/exportlocal/project/'+userSelectionJsonObj.sproject+'/project/'+userSelectionJsonObj.dproject  ;
				this.params= '?XNAT_CSRF='+csrfToken;
				$.ajax({
				    type:"POST",
				    url:this.exporturl+this.params,contentType:"application/json",
				    data:jsonString,
				    success:function(data) {saveJSONs(userSelectionJsonObj);},
				    error:initProjectExportFailure
				});
  		};

  		function initProjectExportFailure(o){
            if (!window.leaving) {
                xmodal.close(); // close "Loading" modal
                xmodal.open({
                    width: 600,
                    height: 400,
                    title: 'Unable to save data freeze',
                    content: o.responseText + " (" + o.status + " " + o.statusText +")",
                    scroll: false,
                    overflow: true,
                    esc: false,
                    enter: false,
                    buttons: {
                        ok: {
                            label: 'OK',
                            isDefault: true,
                            close: true,
                            action: function(){
                                xmodal.closeAll();
                            }
                        }
                    }
                });
            }
        };

		function saveJSONs(userSelectionJsonObj) {
						try{
							var sourceProject = userSelectionJsonObj.sproject;
							var destinationProject = userSelectionJsonObj.dproject;
							var filename = destinationProject + '_' +userSelectionJsonObj.createdon + '_' + userSelectionJsonObj.createdby +'.json'
							var requests = Array();
							var jsonString=JSON.stringify(userSelectionJsonObj);
							var url = getCall(sourceProject,filename);
							requests.push($.ajax({type: "PUT",url:url,contentType: "application/json",data: jsonString}));
							url = getCall(destinationProject,filename);
							requests.push($.ajax({type: "PUT",url:url,contentType: "application/json",data: jsonString}));
						    var defer = $.when.apply($, requests);
						    defer.then(function(){
						      xmodal.loading.close();
						      document.location.href=serverRoot + '/data/archive/projects/' + projectID;
						      xmodal.close();
						    });
				        }catch(e){
				            xModalMessage('Unable to load', 'Invalid list.<br/><br/>' + e.toString());
				        }
		};

		function getCall(project,filename) {
			var saveurl = serverRoot + '/data/archive/projects/'+project+'/resources/datafreeze/files/'+filename ;
			var params= '?inbody=true&content=DATAFREEZE_JSON&format=JSON&XNAT_CSRF='+csrfToken;
			return saveurl+params;
		};
}