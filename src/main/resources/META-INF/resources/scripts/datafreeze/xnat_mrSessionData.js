var dpukMRScanTypes = ["Select","3D_T1","3D_T2","3D_FLAIR","2D_T1","2D_T2","2D_FLAIR","T2_STAR","SWI","REST","TASK","DTI","ASL"];
var xnatmrSessionDataTotalRows =0;
var xnatmrSessionDataDataFreeze={
	 scantypes:[],
     mappings:{},
     resources:["DICOM"],
     fields:{},
	  populate: function () {
		 var dataTypeSelectionId = this.datatype+ "_experimentSelectionOpt";
		 var dataTypeSelection = document.getElementById(dataTypeSelectionId);
		 if (!(dataTypeSelection == null) && dataTypeSelection.checked) {
	         for (var i=0; i<xnatmrSessionDataTotalRows;i++) {
				var checkBoxId = this.datatype+"_Opt["+i+"][0]";
				var checkBox = document.getElementById(checkBoxId);
				if (!(checkBox == null) && checkBox.checked) {
					this.scantypes.push(checkBox.value);
					var dpukScanTypeSelectedId = this.datatype+"_Opt["+i+"][1]";
					var selectBox = document.getElementById(dpukScanTypeSelectedId);
					var mapKey = checkBox.value;
					if ((typeof selectBox === 'undefined') || selectBox == null ) {
						var mapValue = mapKey;
					}else {
						var mapValue = selectBox.options[selectBox.selectedIndex].text
					}
					this.mappings[mapKey]=mapValue;
				}
			 }
		  }
	    }
};

function xnatmrSessionDataHaveIBeenSelected() {
   var iHaveBeenSelected = false;
   var xsiType = "xnat:mrSessionData";
   var idStr = xsiType+ "_experimentSelectionOpt";
   var inputElement = document.getElementById(idStr);
   if (inputElement != null) {
       if (inputElement.checked) {
		 iHaveBeenSelected = true;
	   }
   }
   return iHaveBeenSelected;
};

function xnatmrSessionDataDataFreezeController() {
  xnatmrSessionDataDataFreeze.populate();
  if (xnatmrSessionDataDataFreeze.scantypes.length == 0){
     return null;
  }else {
	  return xnatmrSessionDataDataFreeze;
  }
};


function MRSessionDataFreezeManagerInit(project,groups,visits,xsiType,experimentName,experimentCount){
  var datafreezeManager = new MRSessionDataFreezeManager(project,groups,visits,xsiType,experimentName,experimentCount);
  xnatmrSessionDataDataFreeze.datatype=xsiType;
  //xnatmrSessionDataDataFreeze.filtertype=experimentName;
  xnatmrSessionDataDataFreeze.filtertype="MRSessions";
  xnatmrSessionDataDataFreeze.experimentcount=experimentCount;
  datafreezeManager.populateData();
};


function	matchesDPUKScanType(scanType) {
			var matches = 0;
			for	(var iindex = 0; iindex < dpukMRScanTypes.length; iindex++) {
				if (scanType == dpukMRScanTypes[iindex]) {
					matches = 1;
					break;
				}
			}
			return matches;
		};


function MRSessionDataFreezeManager(pID,groups,visits, xsiType,experimentName,sessionCount){
		this.projectID=pID;
		this.groups=groups;
		this.visits=visits;
		this.xsiType=xsiType;
		this.experimentName=experimentName;
		this.sessionCount=sessionCount;
		//this.initLoader=prependLoader("waitPanel","Loading MR Session content");

		this.populateData=function() {
				//this.initLoader=prependLoader("waitPanel","Loading content");
				//this.initLoader.render();
				this.initCallback={
					cache:false,
					success:this.completeInit,
					failure:this.initFailure,
					scope:this
				};
				this.url = serverRoot + '/REST/projects/'+ pID + '/datafreeze?groups=' + groups +'&visits='+visits+'&xsitype='+xsiType+'&format=json';
				YAHOO.util.Connect.asyncRequest('GET',this.url,this.initCallback,null,this);
	};

		this.initFailure=function(o){
	            //this.initLoader.close();
	        if (!window.leaving) {
	            xModalMessage('Unable to load', 'Failed to load details for MR Sessions.');
	        }
		};

		this.completeInit=function(oResponse){
			   	//this.initLoader.close();
				try{
		            this.dataResultSet= eval("(" + oResponse.responseText +")");
		            this.render();
		        }catch(e){
		            xModalMessage('Unable to load', 'Invalid list.<br/><br/>' + e.toString());
		        }
	};

		this.render=function() {
			//var jsonStr = '{"ResultSet":{"Result":[{"series_description":"","type":"EPI_REST"},{"series_description":"","type":"Flair"},{"series_description":"","type":"T1"},{"series_description":"","type":"T2"}], "groups": "Baseline","xsiType": "xnat:mrSessionData","visits": "Visit1,Baseline","totalRecords": "4","project": "TEST1"}}';
			var jsonStr = YAHOO.lang.JSON.stringify(this.dataResultSet);
			var parsedResponse = YAHOO.lang.JSON.parse(jsonStr);

			var rowArray = parsedResponse.ResultSet.Result;
			var containNonStandardDPUKScanTypes = 0;
			var containStandardDPUKScanTypes = 0;
			
			//find nonstandard scans
			for	(index = 0; index < rowArray.length; index++) {
				var jsonResultRowTypeValue = rowArray[index]["type"];
				if(matchesDPUKScanType(jsonResultRowTypeValue) == 1) {
					containStandardDPUKScanTypes = 1;
				}
				if(matchesDPUKScanType(jsonResultRowTypeValue) == 0) {
					containNonStandardDPUKScanTypes = 1;
				}
			}
			
				var xsiType_mgmt_div = document.getElementById("inner__content");

				var mainDiv = document.createElement("div");
				mainDiv.setAttribute("class","main__control--wrapper");
				var mainUl = document.createElement("ul");
				mainUl.id = "ul-"+xsiType;
				var liLeadRow = document.createElement("li");
				liLeadRow.setAttribute("class","lead__row");
				var divFirstCell = document.createElement("div");
				divFirstCell.setAttribute("class","first__cell");
				var spanFilterElement = document.createElement("span");
				spanFilterElement.id = getMRSpanId(xsiType);
				spanFilterElement.setAttribute("class","filter__element");
				if(containStandardDPUKScanTypes ==1){
					addCheckBoxForDataType(xsiType,spanFilterElement);
				}else{
					var iExclamation = document.createElement("i");
					iExclamation.id = getMRIExclamationId(xsiType);
					iExclamation.setAttribute("class","fa fa-exclamation-triangle");
					spanFilterElement.appendChild(iExclamation);
				}
				
				divFirstCell.appendChild(spanFilterElement);
				liLeadRow.appendChild(divFirstCell);
				var spanMainTitle = document.createElement("span");
				spanMainTitle.setAttribute("class","main__title");

				var textStr = experimentName;
				spanMainTitle.innerHTML=textStr;
				spanMainTitle.setAttribute("class","main__title");
				liLeadRow.appendChild(spanMainTitle);

				if (containNonStandardDPUKScanTypes == 1) {
				    var spanInfo=document.createElement("span");
					spanInfo.setAttribute("class","alert__text pink__text");
					spanInfo.innerHTML = "UNRECOGNIZED SCAN TYPES FOUND";
					liLeadRow.appendChild(spanInfo);

					var spanTooltip = document.createElement("span");
					spanTooltip.setAttribute("class","alert__text");
					addScanTypeTooltip(spanTooltip);
					liLeadRow.appendChild(spanTooltip);
			    }
				var spanCountNo = document.createElement("span");
				spanCountNo.setAttribute("class","count__no");
				spanCountNo.innerHTML=sessionCount;
				liLeadRow.appendChild(spanCountNo);
				spanCountNo = document.createElement("span");
				spanCountNo.setAttribute("class","count");
				spanCountNo.innerHTML="COUNT:";
				liLeadRow.appendChild(spanCountNo);
				mainUl.appendChild(liLeadRow);


				var xsiType_mgmt_ul = mainUl;

			var listEntryGrayRow=document.createElement("li");
			listEntryGrayRow.setAttribute("class","gray__rows");
			var ulEntry=document.createElement("ul");
			var listEntryTableTitleRow=document.createElement("li");
			listEntryTableTitleRow.setAttribute("class","table__title--row");
			var col1span=document.createElement("span");
			col1span.setAttribute("class","first__title column__title third");
			col1span.innerHTML="SCAN TYPES";
			listEntryTableTitleRow.appendChild(col1span);
			var col2span=document.createElement("span");
			col2span.setAttribute("class","column__title third");
			col2span.innerHTML="DESCRIPTION";
			listEntryTableTitleRow.appendChild(col2span);
			var col3span=document.createElement("span");
			col3span.setAttribute("class","column__title third");
			col3span.innerHTML="DPUK SCAN TYPE";
			listEntryTableTitleRow.appendChild(col3span);

			ulEntry.appendChild(listEntryTableTitleRow);
			var rowArray = parsedResponse.ResultSet.Result;
			for	(var index = 0; index < rowArray.length; index++) {
				var jsonResultRowTypeValue = rowArray[index]["type"];
				var jsonResultRowSeriesDescriptionValue = rowArray[index]["series_description"];
				var doesNotMatch=matchesDPUKScanType(jsonResultRowTypeValue);
				var lEntryStdRow=document.createElement("li");
				lEntryStdRow.setAttribute("class","standard__row");
				var divEntryFirstCell = document.createElement("div");
				divEntryFirstCell.id = getDivIdForScan(xsiType,index);
				divEntryFirstCell.setAttribute("class","first__cell");
				if (doesNotMatch==0) {
					// addMRHref(xsiType,index,divEntryFirstCell);
					addCheckBoxForMRScan(xsiType,index,jsonResultRowTypeValue,divEntryFirstCell,true); // add disabled checkbox
				}else {
					addCheckBoxForMRScan(xsiType,index,jsonResultRowTypeValue,divEntryFirstCell);
				}
			    lEntryStdRow.appendChild(divEntryFirstCell);

				var divEntryStdCellThird = document.createElement("div");
				divEntryStdCellThird.setAttribute("class","standard__cell third");
				var span = document.createElement("span");
				if (doesNotMatch == 0) {
					span.setAttribute("class","pink__text");
				}
			    span.innerHTML = jsonResultRowTypeValue;
				divEntryStdCellThird.appendChild(span);
			    lEntryStdRow.appendChild(divEntryStdCellThird);

				divEntryStdCellThird = document.createElement("div");
				divEntryStdCellThird.setAttribute("class","standard__cell third");
				span = document.createElement("span");
				if (doesNotMatch == 0) {
					span.setAttribute("class","pink__text");
				}
			    span.innerHTML = rowArray[index]["series_description"];
				divEntryStdCellThird.appendChild(span);
			    lEntryStdRow.appendChild(divEntryStdCellThird);

				divEntryStdCellThird = document.createElement("div");
				divEntryStdCellThird.setAttribute("class","standard__cell third no-border");
				span = document.createElement("span");

				if (doesNotMatch == 0) {
					span.setAttribute("class","pink__text");
				}
				if (doesNotMatch == 0) {
					var idStr = xsiType+"_Opt["+index+"][1]";
					var selectEntry  = document.createElement("select");
					selectEntry.setAttribute("class","select__list");
					selectEntry.name = idStr;
					selectEntry.id = idStr;
					populateMRSelectWithDPUKScanType(selectEntry,xsiType,index,jsonResultRowTypeValue);
					span.appendChild(selectEntry);
				}else {
					span.innerHTML = jsonResultRowTypeValue;
			    }
				   divEntryStdCellThird.appendChild(span);


			    lEntryStdRow.appendChild(divEntryStdCellThird);

				ulEntry.appendChild(lEntryStdRow);
		    }
			listEntryGrayRow.appendChild(ulEntry);
			xsiType_mgmt_ul.appendChild(listEntryGrayRow);

			xnatmrSessionDataTotalRows=rowArray.length;
			mainDiv.appendChild(mainUl);
			xsiType_mgmt_div.appendChild(mainDiv);
		};


};

/*

Methods to toggle the display contents when Select box to map the scan-type is changed.

*/

function	populateMRSelectWithDPUKScanType(selectEntry,xsiType,scanRow,scanType) {
			for	(var iindex = 0; iindex < dpukMRScanTypes.length; iindex++) {
				var option = document.createElement("option");
				if (iindex==0) {
					option.selected=true;
				}
				option.value=dpukMRScanTypes[iindex];
				option.innerHTML = dpukMRScanTypes[iindex];
				selectEntry.appendChild(option);
			}
			selectEntry.onchange = function(){updateMRRow(this,xsiType,scanRow,scanType);};
		};


function updateMRRow(selectElement,xsiType,scanRow,scanType) {
			//If selected Index is not 0, update the row with a checkbox
			//Update the section check box to checked
			var divId = getDivIdForScan(xsiType,scanRow);
			if (selectElement.selectedIndex == 0) {
                var inputID = xsiType+"_Opt["+scanRow+"][0]";
                var thisInput = document.getElementById(inputID);
                thisInput.disabled = "disabled";
                thisInput.checked = false;
				/*
				removeMRCheckBox(xsiType,scanRow);
				addMRHrefElement(xsiType,scanRow,divId);
				*/
			}else {
			    // toggle scan row selector
                var inputID = xsiType+"_Opt["+scanRow+"][0]";
                var thisInput = document.getElementById(inputID);
                thisInput.disabled = false;
                thisInput.checked = "checked";

                // toggle MR session selector
				var spanId =getMRSpanId(xsiType);
				removeChild(spanId,getMRIExclamationId(xsiType));
				addCheckBoxForDataTypeBySpanId(spanId,xsiType);

                /*
				var divE = document.getElementById(divId);
				removeChild(divId,getIdForMRScanRowHref(xsiType,scanRow));
				addCheckBoxForMRScan(xsiType,scanRow,scanType, divE);
				*/
			}
};

function addCheckBoxForDataTypeBySpanId(spanId,xsiType) {
	var spanE = document.getElementById(spanId);
    addCheckBoxForDataType(xsiType, spanE);
};


function addCheckBoxForDataType(xsiType,span) {
		var idStr = xsiType+ "_experimentSelectionOpt";
		var inputElement = document.getElementById(idStr);
		if (inputElement == null) {
			var inputElement = document.createElement("input");
			inputElement.type="checkbox";
			inputElement.checked="true";
			inputElement.name=idStr;
			inputElement.id=idStr;
			inputElement.value=xsiType;
			var label=document.createElement("label");
			label.setAttribute("for",idStr);
			span.appendChild(inputElement);
			span.appendChild(label);
		}
};

function addCheckBoxForMRScan(xsiType,index,rowValue,divE,disabled) {
		var spanEID  = getScanRowSpanId(xsiType,index);
		var spanE  = document.getElementById(spanEID);

	    if (spanE == null) {
			spanE = document.createElement("span");
  		    spanE.id=spanEID;
		    spanE.setAttribute("class","filter__element");
		    var inputEntry  = document.createElement("input");
		    inputEntry.type="checkbox";
		    if (disabled) {
		        inputEntry.disabled="disabled";
		    } else {
    		    inputEntry.checked="true";
		    }
		    var idStr = getScanRowCheckBoxId(xsiType,index);
		    inputEntry.name = idStr;
		    inputEntry.id = idStr;
		    inputEntry.value = rowValue;
		    spanE.appendChild(inputEntry);
		    var labelEntry  = document.createElement("label");
		    labelEntry.setAttribute("for",idStr);
		    spanE.appendChild(labelEntry);
		    divE.appendChild(spanE);
		}
};

function removeMRCheckBox(xsiType,j) {
 	var parentId = getDivIdForScan(xsiType,j);
 	var childId = getScanRowSpanId(xsiType,j);
    removeChild(parentId,childId);
};


function	addMRHrefElement(xsiType,j,divId) {
	var divE = document.getElementById(divId);
	addMRHref(xsiType,j,divE);
};

function addScanTypeTooltip (parentElement){
    var hrefElement = document.createElement("a");
    hrefElement.setAttribute("data-toggle","tooltip");
    hrefElement.setAttribute("title","What does this mean?");
    var iElement = document.createElement("i");
    iElement.setAttribute("class","fa  fa-question-circle");
    hrefElement.appendChild(iElement);
    hrefElement.setAttribute("onclick","javascript:showScanHelp()");
    parentElement.appendChild(hrefElement);
}

function addMRHref(xsiType,index,divEntryFirstCell) {
		var hrefElement = document.createElement("a");
		hrefElement.setAttribute("data-toggle","tooltip");
		hrefElement.setAttribute("title","Ignore");
		hrefElement.id=getIdForMRScanRowHref(xsiType, index);
		var iElement = document.createElement("i");
		iElement.setAttribute("class","fa  fa-question-circle");
		hrefElement.appendChild(iElement);
		hrefElement.setAttribute("onclick","javascript:showScanHelp()");
		//hrefElement.setAttribute("href","#");
		divEntryFirstCell.appendChild(hrefElement);
};

function showScanHelp() {
	xModalMessage('Scan Type Mismatch', 'The scan-types do not match the standardized scan-types in DPUK Central. Please select an appropriate match.');
};


function getScanRowSpanId(xsiType,index) {
	var id = "span_filter__element_" +xsiType +"_" + index;
	return id;
};

function getMRSpanId(xsiType) {
  var id = "lead_row_span_" + xsiType;
  return id;
};


function getIdForMRScanRowHref(xsiType, index){
  var id = "href_"+xsiType + "_" + index;
  return id;
};


function getScanRowCheckBoxId(xsiType,index) {
	var idStr = xsiType+"_Opt["+index+"][0]";
	return idStr;
};

function getDivIdForScan(xsiType,scanRow){
 var divId = "div_" + xsiType + "_" + scanRow;
 return divId;
};

function getMRIExclamationId(xsiType) {
   var id = "lead_row_exclamation_" + xsiType ;
   return id;
};




