//Copyright  2015 Radiologics, Inc
 

package com.radiologics.datafreeze.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

/**
 * @author Mohana Ramaratnam
 *
 * This class contains methods to query an XnatSubject to support queries 
 * like 
 *  1. Get subject counts per group
 *  2. Get subject counts per visit within each group 
 */

public class DataFreezeSubjectHelper extends DataFreezeHelper {

	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreezeSubjectHelper.class);
	
	public DataFreezeSubjectHelper(UserI _user) {
		super(_user);
	}
	
	/**
	 * Returns the total number of subjects enrolled in a project
	 * @param projectId: String
	 * @return Count of the subjects
	 */
	public int getTotalNumberOfSubjects(String projectId) {
		int totalSubjects = 0;
		final  String COLNAME = "allsubjects";
        String query ="select count(*) as " + COLNAME + " from  xnat_subjectdata where project='"+ projectId + "';";
        XFTTable resultsTable = doQuery(query);
        if (resultsTable != null && resultsTable.getNumRows() > 0) {
        	ArrayList resultRow = resultsTable.convertColumnToArrayList(COLNAME);
        	if (resultRow != null && resultRow.size() > 0) {
        		totalSubjects = Integer.parseInt(resultRow.get(0).toString());
        	}
        }
	    return totalSubjects;
	}

	/**
	 * Returns the group wise counts of subjects for a given project
	 * @param projectId: String
	 * @return XFTTable with columns subjectcount and researchgroup
	 */
	public XFTTable getSubjectGroupCounts(String projectId) {
		XFTTable resultsTable=null;
        String query ="select count(*) as subjectcount, COALESCE(_group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as researchgroup from xnat_subjectdata where project='"+ projectId + "' GROUP BY _group order by _group;";
        resultsTable = doQuery(query);
		return resultsTable;
	}

	
	

	/**
	 * Convenience method to get subject totals across groups
	 * @param projectId
	 * @param groups
	 * @param checkForNull
	 * @return
	 */
	public int getTotalSubjectsAcrossGroups(String projectId, List<String> groups, boolean checkForNull) {
		int count = 0;
		String groupsAsCSVForSQL = toCSVForSQL(groups);
		if (checkForNull && hasNullValue(groups)) {
            String query =null;
            if (!groupsAsCSVForSQL.equals("")) 
                query = "select count(*) as total from xnat_subjectData where project='"+projectId + "' and (_group in ("+ groupsAsCSVForSQL + ") or _group is NULL)";
            else
            	query = "select count(*) as total from xnat_subjectData where project='"+projectId + "' and _group is NULL";
			XFTTable resultsTable = doQuery(query);
            if (resultsTable != null ) {
            	ArrayList<Hashtable> resultsAsHash = resultsTable.toArrayListOfHashtables();
            	if (resultsAsHash != null && resultsAsHash.size() >0 ) {
            		Hashtable row = resultsAsHash.get(0);
            		count = ((Long)row.get("total")).intValue();
            	}
            }
            return count;
		}else {
			return getTotalSubjectsAcrossGroups(projectId,groupsAsCSVForSQL);
		}
	}

	/**
	 * Convenience method which returns a Hashtable of group, subject counts for that group in a project
	 * @param projectId
	 * @param groups
	 * @param checkForNull
	 * @return
	 */
	public Hashtable getTotalSubjectsByGroups(String projectId, List<String> groups, boolean checkForNull) {
        Hashtable result = new Hashtable();
		String groupsAsCSVForSQL = toCSVForSQL(groups);
		XFTTable t = null;
		String query = null;
		if (checkForNull && hasNullValue(groups)) {
			if (!groupsAsCSVForSQL.equals(""))
             query = "select  COALESCE(_group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as group, count(*) as count from xnat_subjectData where project='" + projectId + "' and (_group in ("+ groupsAsCSVForSQL + ") or _group is NULL) GROUP BY _group";
			else
	             query = "select  COALESCE(_group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as group, count(*) as count from xnat_subjectData where project='" + projectId + "' and  _group is NULL GROUP BY _group";
			t = doQuery(query);
		}else {
            query = "select  _group as group, count(*) as count from xnat_subjectData where project='" + projectId + "' and _group in ("+ groupsAsCSVForSQL + ")  GROUP BY _group";
			t = doQuery(query);
		}
	      if (t!=null) {
	    	  t.resetRowCursor();
		        while (t.hasMoreRows())
		        {
		            Object[] row = t.nextRow();
		            result.put(row[0], row[1]);
		        }
	      }

	        return result;
	}

	

	/**
	 * Convenience method to construct the count of subjects across groups
	 * @param projectId
	 * @param groupsAsCSVForSQL
	 * @return
	 */
	private int getTotalSubjectsAcrossGroups(String projectId, String groupsAsCSVForSQL) {
		int count=0;
        String query = "select count(*) as total from xnat_subjectData where project='"+projectId + "' and _group in ("+ groupsAsCSVForSQL + ")";
        XFTTable resultsTable = doQuery(query);
        if (resultsTable != null ) {
        	ArrayList<Hashtable> resultsAsHash = resultsTable.toArrayListOfHashtables();
        	if (resultsAsHash != null && resultsAsHash.size() >0 ) {
        		Hashtable row = resultsAsHash.get(0);
        		count = ((Long)row.get("total")).intValue();
        	}
        }
		return count;
	}

	
	
	/**
	 * Method which returns the subject visit counts on a per group basis for a given project.
	 * @param projectId
	 * @param groupIds
	 * @param checkForNull
	 * @return
	 */
	public XFTTable getSubjectVisitCountsFilteredByGroups(String projectId, List<String> groupIds, boolean checkForNull) {
		XFTTable resultsTable=null;
			String groupFilterAsSQLStmt=toCSVForSQL(groupIds);
			if (checkForNull && hasNullValue(groupIds)) {
				String query = null;
				if (!groupFilterAsSQLStmt.equals("")){
					query = "select count(*) as subjectvisitcount, innerQuery.visitid, innerQuery.subjectgroup from (select s.id, COALESCE(s._group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as visitid from xnat_subjectdata s";
					query += " LEFT JOIN xnat_subjectassessordata sa ON  s.id=sa.subject_id";
					query += " LEFT JOIN xnat_experimentdata e ON sa.id=e.id where"; 
					query += "  e.project='"+projectId+ "' and ( s._group in (" + groupFilterAsSQLStmt +") or s._group is NULL) group by s._group,e.visit_id,s.id)";
					query += " as innerQuery group by visitid, subjectgroup";
				}else {
					//query = "select count(s.*) as subjectvisitcount,COALESCE(s._group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as visitid from xnat_subjectdata s, xnat_subjectassessordata sa, xnat_experimentdata e where s.id=sa.subject_id and sa.id=e.id and s.project=e.project and s.project='"+projectId+ "' and s._group is NULL group by s._group,e.visit_id order by s._group;";
					query = "select count(*) as subjectvisitcount, innerQuery.visitid, innerQuery.subjectgroup from (select s.id, COALESCE(s._group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as visitid from xnat_subjectdata s";
					query += " LEFT JOIN xnat_subjectassessordata sa ON  s.id=sa.subject_id";
					query += " LEFT JOIN xnat_experimentdata e ON sa.id=e.id where"; 
					query += "  e.project='"+projectId+ "' and  s._group is NULL group by s._group,e.visit_id,s.id)";
					query += " as innerQuery group by visitid, subjectgroup";
				}
				resultsTable = doQuery(query);
			}else {
			//	String query = "select count(s.*) as subjectvisitcount,s._group as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as visitid from xnat_subjectdata s, xnat_subjectassessordata sa, xnat_experimentdata e where s.id=sa.subject_id and sa.id=e.id and s.project=e.project and s.project='"+projectId+ "' and  s._group in (" + groupFilterAsSQLStmt +") group by s._group,e.visit_id order by s._group;";
				String query = "select count(*) as subjectvisitcount, innerQuery.visitid, innerQuery.subjectgroup from (select s.id, COALESCE(s._group,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as visitid from xnat_subjectdata s";
				query += " LEFT JOIN xnat_subjectassessordata sa ON  s.id=sa.subject_id";
				query += " LEFT JOIN xnat_experimentdata e ON sa.id=e.id where"; 
				query += "  e.project='"+projectId+ "' and s._group in (" + groupFilterAsSQLStmt +")   group by s._group,e.visit_id,s.id)";
				query += " as innerQuery group by visitid, subjectgroup";

				resultsTable = doQuery(query);
			}
		return resultsTable;
	}
	
	/**
	 * Method which returns the subject counts across a given group and visit for a given project.
	 * @param projectId
	 * @param groupIds
	 * @param checkForNull
	 * @return
	 */
	public XFTTable getSubjectCountsAcrossGroupsAndVisits(String projectId, List<String> groupIds, boolean checkForNull) {
		XFTTable resultsTable=null;
			String groupFilterAsSQLStmt=toCSVForSQL(groupIds);
			if (checkForNull && hasNullValue(groupIds)) {
				String query = null;
				if (!groupFilterAsSQLStmt.equals("")){
					query = "select count(distinct id) as subjectvisitcount from (select distinct s.id, COALESCE(s._group,'"+ DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as visitid from xnat_subjectdata s";
					query += " LEFT JOIN xnat_subjectassessordata sa ON  s.id=sa.subject_id"; 
					query += " LEFT JOIN xnat_experimentdata e ON sa.id=e.id"; 
					query += " where e.project='"+projectId+"' and ( s._group in ("+groupFilterAsSQLStmt+") or s._group is NULL) group by s._group,e.visit_id,s.id) as innertable";					
				}else {
					query = "select count(distinct id) as subjectvisitcount from (select distinct s.id, COALESCE(s._group,'"+ DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as visitid from xnat_subjectdata s";
					query += " LEFT JOIN xnat_subjectassessordata sa ON  s.id=sa.subject_id"; 
					query += " LEFT JOIN xnat_experimentdata e ON sa.id=e.id"; 
					query += " where e.project='"+projectId+"' and  s._group is NULL group by s._group,e.visit_id,s.id) as innertable";					
				}
				resultsTable = doQuery(query);
			}else {
				String query = "select count(distinct id) as subjectvisitcount from (select distinct s.id, COALESCE(s._group,'"+ DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION +"') as subjectgroup, COALESCE(e.visit_id,'"+DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION+"') as visitid from xnat_subjectdata s";
				query += " LEFT JOIN xnat_subjectassessordata sa ON  s.id=sa.subject_id"; 
				query += " LEFT JOIN xnat_experimentdata e ON sa.id=e.id"; 
				query += " where e.project='"+projectId+"' and  s._group in ("+groupFilterAsSQLStmt+") group by s._group,e.visit_id,s.id) as innertable";					

				resultsTable = doQuery(query);
			}
		return resultsTable;
	}

	
	

}
