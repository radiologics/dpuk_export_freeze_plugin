/**
Copyright  2015 Radiologics, Inc
/**
 * 
 */
package com.radiologics.datafreeze.utils;

import java.util.ArrayList;
import java.util.List;

import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;

/**
 * @author Mohana Ramaratnam
 *
 * This class contains methods to query an XnatProject to support queries 
 * like 
 *  1. Get me all the groups within this project
 *  2. Get me all the subjects who belong to a collection of groups
 *  3. Get me all the subjects who belong to a collection of groups and visits
 */
/**
 * @author mohan_000
 *
 */
public class DataFreezeProjectHelper extends DataFreezeHelper {

	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreezeProjectHelper.class);
	
	public DataFreezeProjectHelper(UserI _user ) {
		super(_user);
	}
	
	/**
	 * Gives the experiment counts based for a project filtered by visits and groups
	 * @param projectId
	 * @param visitId
	 * @param groupId
	 * @return
	 */
	public XFTTable getExperimentCountsByVisitAndGroup(String projectId, List visitIds, List groupIds, boolean checkForNull) {
		String groupsAsCSVForSQL = toCSVForSQL(groupIds);
		String visitsAsCSVForSQL = toCSVForSQL(visitIds);
		String query = "select count(e.id) as totalexperiments,xes.element_name, xes.plural from xnat_experimentdata e ";
		query += "LEFT JOIN xdat_meta_element xme ON e.extension = xme.xdat_meta_element_id ";
		query += "LEFT JOIN xdat_element_security xes ON xes.element_name=xme.element_name ";
		query += "LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id ";
		query += "LEFT JOIN xnat_subjectdata s ON s.id=sa.subject_id where e.project='"+ projectId +"' " ;

		if (checkForNull) {
			if (hasNullValue(groupIds)) {
				if (!groupsAsCSVForSQL.equals("")) { 
					query += "  and (s._group in (" + groupsAsCSVForSQL + ") or s._group is NULL) ";
				}else {
					query += "  and  s._group is NULL ";
				}
			}else {
				query += "  and s._group in (" + groupsAsCSVForSQL + ") ";
			}
			if (hasNullValue(visitIds)) {
				if (!visitsAsCSVForSQL.equals("")) { 
					query += "  and (e.visit_id in (" + visitsAsCSVForSQL + ") or e.visit_id is NULL) ";
				}else {
					query += "  and  e.visit_id is NULL ";
				}
			}else {
				query += "  and e.visit_id in (" + visitsAsCSVForSQL + ") ";
			}
		}else {
			query += "  and s._group in (" + groupsAsCSVForSQL +")" ;
			query += "  and e.visit_id in (" + visitsAsCSVForSQL + ")";
		}
		query += " group by  xes.plural, xes.element_name order by xes.plural "; 
		//System.out.println("Experiment Counts by Visits and Groups ");
		XFTTable results = doQuery(query);
		return results;
	}

	/**
	 * Gives the experiment counts based for a project filtered by scantypes, visits and groups
	 * @param projectId
	 * @param scanTypes
	 * @param visitId
	 * @param groupId
	 * @return
	 */
	public XFTTable getExperimentCountsByScanTypesVisitAndGroup(String projectId,String xsiType, List<String> scanTypes, List<String> visitIds, List<String> groupIds, boolean checkForNull) {
		String groupsAsCSVForSQL = toCSVForSQL(groupIds);
		String visitsAsCSVForSQL = toCSVForSQL(visitIds);
		String scanTypesAsCSVForSQL = toCSVForSQL(scanTypes);
		String query = "select count(*) as totalexperiments from (select e.id as totalexperiments from xnat_experimentdata e ";
   		query += " LEFT JOIN xdat_meta_element xme ON e.extension = xme.xdat_meta_element_id ";
		query += " LEFT JOIN xdat_element_security xes ON xes.element_name=xme.element_name ";
		query += " LEFT JOIN xnat_imagesessiondata im ON im.id=e.id ";
		query += " LEFT JOIN xnat_imagescandata isd ON isd.image_session_id=e.id ";
		query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id ";
		query += " LEFT JOIN xnat_subjectdata s ON s.id=sa.subject_id where e.project='"+ projectId +"' " ;

		if (checkForNull) {
			if (hasNullValue(groupIds)) {
				if (!groupsAsCSVForSQL.equals("")) { 
					query += "  and (s._group in (" + groupsAsCSVForSQL + ") or s._group is NULL) ";
				}else {
					query += "  and  s._group is NULL ";
				}
			}else {
				query += "  and s._group in (" + groupsAsCSVForSQL + ") ";
			}
			if (hasNullValue(visitIds)) {
				if (!visitsAsCSVForSQL.equals("")) { 
					query += "  and (e.visit_id in (" + visitsAsCSVForSQL + ") or e.visit_id is NULL) ";
				}else {
					query += "  and  e.visit_id is NULL ";
				}
			}else {
				query += "  and e.visit_id in (" + visitsAsCSVForSQL + ") ";
			}
			if (hasNullValue(scanTypes)) {
				if (!scanTypesAsCSVForSQL.equals("")) { 
					query += "  and (isd.type in (" + scanTypesAsCSVForSQL + ") or isd.type is NULL) ";
				}else {
					query += "  and  isd.type is NULL ";
				}
			}else {
				query += "  and isd.type in (" + scanTypesAsCSVForSQL + ") ";
			}
		}else {
			query += "  and s._group in (" + groupsAsCSVForSQL +")" ;
			query += "  and e.visit_id in (" + visitsAsCSVForSQL + ")";
			query += "  and isd.type in (" + scanTypesAsCSVForSQL + ")";
		}
		query += " and xes.element_name='"+xsiType+"' group by e.id ) as resultsAlias ";
		
		logger.info("Query");
		logger.info(query);
		XFTTable results = doQuery(query);
		return results;
	}

	/**
	 * Gives the experiment counts based for a project filtered by scantypes, visits and groups
	 * @param projectId
	 * @param scanTypes
	 * @param visitId
	 * @param groupId
	 * @return
	 */
	public XFTTable getPetSessionCountsByScanTypesVisitAndGroupApplyTracerFilter(String projectId,String tracer, List<String> scanTypes, List<String> visitIds, List<String> groupIds, boolean checkForNull) {
		String groupsAsCSVForSQL = toCSVForSQL(groupIds);
		String visitsAsCSVForSQL = toCSVForSQL(visitIds);
		String scanTypesAsCSVForSQL = toCSVForSQL(scanTypes);
		String query = "select count(e.id) as totalexperiments from xnat_petSessiondata p ";
		query += " LEFT JOIN xnat_imagesessiondata im ON im.id=p.id ";
		query += "  LEFT JOIN xnat_experimentdata e ON e.id=p.id ";
		query += " LEFT JOIN xnat_imagescandata isd ON isd.image_session_id=p.id ";
		query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=p.id ";
		query += " LEFT JOIN xnat_subjectdata s ON s.id=sa.subject_id where e.project='"+ projectId +"' " ;

		if (checkForNull) {
			if (hasNullValue(groupIds)) {
				if (!groupsAsCSVForSQL.equals("")) { 
					query += "  and (s._group in (" + groupsAsCSVForSQL + ") or s._group is NULL) ";
				}else {
					query += "  and  s._group is NULL ";
				}
			}else {
				query += "  and s._group in (" + groupsAsCSVForSQL + ") ";
			}
			if (hasNullValue(visitIds)) {
				if (!visitsAsCSVForSQL.equals("")) { 
					query += "  and (e.visit_id in (" + visitsAsCSVForSQL + ") or e.visit_id is NULL) ";
				}else {
					query += "  and  e.visit_id is NULL ";
				}
			}else {
				query += "  and e.visit_id in (" + visitsAsCSVForSQL + ") ";
			}
			if (hasNullValue(scanTypes)) {
				if (!scanTypesAsCSVForSQL.equals("")) { 
					query += "  and (isd.type in (" + scanTypesAsCSVForSQL + ") or isd.type is NULL) ";
				}else {
					query += "  and  isd.type is NULL ";
				}
			}else {
				query += "  and isd.type in (" + scanTypesAsCSVForSQL + ") ";
			}
		}else {
			query += "  and s._group in (" + groupsAsCSVForSQL +")" ;
			query += "  and e.visit_id in (" + visitsAsCSVForSQL + ")";
			query += "  and isd.type in (" + scanTypesAsCSVForSQL + ")";
		}
		query += " and p.tracer_name='"+tracer+"' ";
		
		XFTTable results = doQuery(query);
		return results;
	}

	/**
	 * Gets the total experiments across experiment types for a given project, collection of visits and groups
	 * @param projectId
	 * @param visitIds
	 * @param groupIds
	 * @param checkForNull
	 * @return
	 */
	
	public int getTotalExperimentsAcrossVisitAndGroup(String projectId, ArrayList visitIds, ArrayList groupIds, boolean checkForNull) {
		int count = 0;
		XFTTable results = getExperimentCountsByVisitAndGroup(projectId,visitIds,groupIds,checkForNull);
		count = getColumnSum(results, "totalexperiments");
		return count;
	}
	
}
