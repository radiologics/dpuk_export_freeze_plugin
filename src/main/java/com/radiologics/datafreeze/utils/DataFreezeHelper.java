//Copyright 2015 Radiologics, Inc
package com.radiologics.datafreeze.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

/**
 * @author Mohana Ramaratnam
 *
 */
public abstract class DataFreezeHelper {
	
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreezeHelper.class);

	UserI user = null;
	
	
	DataFreezeHelper(UserI _user) {
		user=_user;
	}
	
	/**
	 * Convenience method which executes a query
	 * TODO add SQL Injection check
	 * @param query: query to execute
	 * @return XFTTable : result of the query
	 */
	protected XFTTable doQuery(String query)  {
		logger.debug(query);
		XFTTable resultsTable = null;
		try {
           resultsTable = XFTTable.Execute(query,user.getDBName(),user.getUsername());
		} catch (SQLException e) {
            logger.error("",e);
        } catch (DBPoolException e) {
            logger.error("",e);
        }
		return resultsTable;
	}
	
	/**
	 * Convenience method to display the groups as a comma separated string
	 * @param groupIds
	 * @return CSV String
	 */
	public String toCSV(List<String> groupIds) {
		return DataFreezeUtils.toCSV(groupIds);
	}

	/**
	 * Convenience method to display the groups as a comma separated string
	 * @param groupIds
	 * @return CSV String
	 */
	public String toCSV(List<String> groupIds, boolean selectDistinct) {
		if (selectDistinct) {
			String rtnStr = "";
			Hashtable<String,String> selectedHash = new Hashtable<String,String>();
			ArrayList<String> uniqueGroups = new ArrayList<String>();
			for (String group:groupIds) {
				if (!selectedHash.containsKey(group)) {
					selectedHash.put(group, "1");
				}
			}
			Enumeration<String> keys = selectedHash.keys();
			while (keys.hasMoreElements()) 
			uniqueGroups.add(keys.nextElement());
			return toCSV(uniqueGroups);
		}else
			return DataFreezeUtils.toCSV(groupIds);
	}

	
	/**
	 * Convenience method to check if value supplied for a group is [none] (a mapping for NULL value)
	 * @param groupIds
	 * @return
	 */
	public boolean hasNullValue(List<String> groupIds) {
		return DataFreezeUtils.hasNullValue(groupIds);
	}

	/**
	 * Convenience method to construct comma-separated and single-quoted string of groupIds
	 * @param groupIds
	 * @return
	 */
	public String toCSVForSQL(List<String> groupIds) {
		return DataFreezeUtils.toCSVForSQL(groupIds);

	}

	/**
	 * Sums up a column of a table 
	 * @param table
	 * @param columnName
	 * @return
	 */
	
	public int getColumnSum(XFTTable table, String columnName) {
		return DataFreezeUtils.getColumnSum(table, columnName);
	}
	
}
