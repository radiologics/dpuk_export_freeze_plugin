/**
 *Copyright (c) 2015 Radiologics, Inc
 */


package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;

import com.radiologics.datafreeze.utils.DataFreezeSubjectHelper;

/**
 * @author Mohana Ramaratnam
 *
 */
public class DataFreeze_Step2 extends SecureScreen {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreeze_Step2.class);

	public void doBuildTemplate(RunData data, Context context) {
		   UserI user = TurbineUtils.getUser(data);
		   String projectId = data.getParameters().get("project");
		   List<String> groupIds = new ArrayList<String>();
		   //Hack here to support Back Button
		   try {
			   groupIds = (ArrayList<String>)context.get("groupIds");
			   if (groupIds == null) throw new NullPointerException("GroupIds is null");
		   }catch(Exception e) {
			   groupIds = new ArrayList<String>();
			   String groupsCSV = (String)TurbineUtils.GetPassedParameter("groupids", data);
			   String[] groups = groupsCSV.split(",");
			   groupIds.addAll(Arrays.asList(groups));
		   }
		   DataFreezeSubjectHelper dataFreezeSubjectHelper = new DataFreezeSubjectHelper(user);
		   XFTTable results = dataFreezeSubjectHelper.getSubjectVisitCountsFilteredByGroups(projectId, groupIds,true);
		   context.put("subjectVisitCountsTable", results);
		   
		   Hashtable subjectCountsByGroups = dataFreezeSubjectHelper.getTotalSubjectsByGroups(projectId, groupIds, true);
		   context.put("subjectCountsByGroups", subjectCountsByGroups);
		   context.put("projectId", projectId);
		   context.put("filterCriterion", dataFreezeSubjectHelper.toCSV(groupIds));
	       context.put("totalSubjects", dataFreezeSubjectHelper.getTotalSubjectsAcrossGroups(projectId, groupIds,true));
	}
	
	
	

}
