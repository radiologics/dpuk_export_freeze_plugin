package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;

public class DataFreeze_Save extends SecureScreen {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreeze_Save.class);

	public void doBuildTemplate(RunData data, Context context) {
    	String userSelectionAsJSONStr = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("userSelection",data));
    	userSelectionAsJSONStr = StringEscapeUtils.unescapeHtml(userSelectionAsJSONStr);
		context.put("userSelection", userSelectionAsJSONStr);
	}
}
