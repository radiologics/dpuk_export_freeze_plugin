//Copyright  2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.turbine.modules.screens;

import java.io.IOException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Mohana Ramaratnam
 *
 */
public class DataFreeze_Step4 extends SecureScreen {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreeze_Step4.class);

	public void doBuildTemplate(RunData data, Context context) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			JsonNode userSelectionAsPOJO = objectMapper.readValue((String)context.get("userSelection"), JsonNode.class);
			context.put("userSelectionAsPOJO", userSelectionAsPOJO);
			context.put("userSelection",StringEscapeUtils.escapeHtml(userSelectionAsPOJO.toString()));
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
