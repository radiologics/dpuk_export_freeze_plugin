package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;

import com.radiologics.datafreeze.utils.DataFreezeSubjectHelper;



/**
 * @author Mohana Ramaratnam
 *
 * Screen class to prepare the Data Freeze Step1 page
 */

public class DataFreeze_Step1 extends SecureScreen {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreeze_Step1.class);

	public void doBuildTemplate(RunData data, Context context) {
	   String projectId = data.getParameters().get("search_value");
	   UserI user = TurbineUtils.getUser(data);
	   DataFreezeSubjectHelper dataFreezeSubjectHelper = new DataFreezeSubjectHelper(user);
	   XFTTable results = dataFreezeSubjectHelper.getSubjectGroupCounts(projectId);
	   context.put("subjectGroupCountsTable", results);
	   context.put("projectId", projectId);
	   context.put("totalSubjects", dataFreezeSubjectHelper.getTotalNumberOfSubjects(projectId));
	}
	
}
