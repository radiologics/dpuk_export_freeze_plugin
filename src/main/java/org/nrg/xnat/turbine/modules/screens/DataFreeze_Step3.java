//Copyright  2015 Radiologics, Inc
package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

import com.radiologics.datafreeze.utils.DataFreezeProjectHelper;
import com.radiologics.datafreeze.utils.DataFreezeSubjectHelper;

/**
 * @author Mohana Ramaratnam
 *
 */
public class DataFreeze_Step3 extends SecureScreen {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreeze_Step3.class);

	public void doBuildTemplate(RunData data, Context context) {
		   UserI user = TurbineUtils.getUser(data);
		   String projectId = (String)context.get("project");
		   List<String> groupIds = new ArrayList<String>();
		   //Hack here to support Back Button
		   try {
			   groupIds = (ArrayList<String>)context.get("groupIds");
			   if (groupIds == null) throw new NullPointerException("GroupIds is null");
		   }catch(Exception e) {
			   groupIds = new ArrayList<String>();
			   String groupsCSV = (String)TurbineUtils.GetPassedParameter("groupids", data);
			   String[] groups = groupsCSV.split(",");
			   groupIds.addAll(Arrays.asList(groups));
		   }
		   List<String> visitIds = new ArrayList<String>();
		   //Hack here to support Back Button
		   try {
			   visitIds = (ArrayList<String>)context.get("visitIds");
			   if (visitIds == null) throw new NullPointerException("VisitIds is null");
		   }catch(Exception e) {
			   visitIds = new ArrayList<String>();
			   String visitsAsCSV = (String)TurbineUtils.GetPassedParameter("visitids", data);
			   String[] visits = visitsAsCSV.split(",");
			   visitIds.addAll(Arrays.asList(visits));
		   }

		   DataFreezeProjectHelper dataFreezeProjectHelper = new DataFreezeProjectHelper(user);
		   XFTTable results = dataFreezeProjectHelper.getExperimentCountsByVisitAndGroup(projectId, visitIds, groupIds, true);
	       context.put("totalFilteredExperiments",dataFreezeProjectHelper.getColumnSum(results, "totalexperiments"));
		   context.put("experimentDetails", results);
		   DataFreezeSubjectHelper dataFreezeSubjectHelper = new DataFreezeSubjectHelper(user);
		   XFTTable subjectVisitCounts = dataFreezeSubjectHelper.getSubjectCountsAcrossGroupsAndVisits(projectId, groupIds, true);
		   //Hack here  - getCellValue throws ArrayOutOfBounds if there is one row and one column in the table
		   context.put("totalSubjects",((Object[])subjectVisitCounts.rows().get(0))[0]);
	       String visitsAsStr = dataFreezeProjectHelper.toCSV(visitIds);
		   context.put("visits", visitsAsStr);
	       int totalVisits = visitsAsStr.split(DataFreezeUtils.SEPARATOR).length;
	       context.put("numberOfVisits",totalVisits);
	       boolean selectUnique = true;
	       context.put("groups", dataFreezeProjectHelper.toCSV(groupIds,selectUnique));
	}

}
