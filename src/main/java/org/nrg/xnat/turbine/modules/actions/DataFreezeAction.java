//Copyright  2015 Radiologics, Inc

package org.nrg.xnat.turbine.modules.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.radiologics.datafreeze.utils.DataFreezeProjectHelper;

/**
 * @author Mohana Ramaratnam
 *
 */
public class DataFreezeAction extends SecureAction {
    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreezeAction.class);
   
    public void doPerform(RunData data, Context context) throws Exception {
	    data.setScreenTemplate("DataFreeze_Step1.vm");    	
    }
	
    public void doStep1(RunData data, Context context) throws Exception {
        final String projectId = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("project",data));
        //Extract the groups selected and insert in context;
        ArrayList<String> formValues = new ArrayList<String>();
        Integer totalGroupsInProject = org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedInteger("totalgroups",data);
        for (int i=1; i<= totalGroupsInProject.intValue(); i++) {
	        String formfieldname = "option" + i;
	        if (TurbineUtils.HasPassedParameter(formfieldname, data)) {
			   formValues.add(((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter(formfieldname,data)));
	        }
        }
        context.put("groupIds", formValues);
        context.put("project", projectId);
        //data.setScreenTemplate("DataFreeze_Step2.vm");
    }

    
    public void doStep2(RunData data, Context context) throws Exception {
        final String projectId = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("project",data));
        //Extract the groups selected and insert in context;
        ArrayList<String> formGroupValues = new ArrayList<String>();
        ArrayList<String> formVisitValues = new ArrayList<String>();
        Integer totalgroupsandvisits = org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedInteger("totalgroupsandvisits",data);
        for (int i=1; i<= totalgroupsandvisits.intValue(); i++) {
	        String formfieldname = "option" + i;
	        if (TurbineUtils.HasPassedParameter(formfieldname, data)) {
	           String formGroupFieldName = "group" + i;
	           String formVisitFieldName = "visit" + i;
			   formGroupValues.add(((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter(formGroupFieldName,data)));
			   formVisitValues.add(((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter(formVisitFieldName,data)));
	        }
        }
        context.put("groupIds", formGroupValues);
        context.put("visitIds", formVisitValues);
        context.put("project", projectId);
	    //data.setScreenTemplate("DataFreeze_Step3.vm");
    }

    public void doStep3(RunData data, Context context) throws Exception {
        String result_json = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("json_result",data));
        //Update the session counts as per the scan selections
        result_json = StringEscapeUtils.unescapeHtml(result_json);
        ObjectMapper objectMapper = new ObjectMapper();
		try {
			UserI _user = TurbineUtils.getUser(data);
			JsonNode userSelectionAsPOJO = objectMapper.readValue(result_json, JsonNode.class);
			String projectId = userSelectionAsPOJO.get("sproject").asText();
			JsonNode groups = userSelectionAsPOJO.get("groups");
			JsonNode visits = userSelectionAsPOJO.get("visits");
			List<String> groupsAsList = convertToList(groups);
			List<String> visitsAsList = convertToList(visits);
			for (JsonNode dataType:userSelectionAsPOJO.get("sessionfilters")) {
				String xsiType = dataType.get("datatype").asText();
				JsonNode scanTypeNode = dataType.get("scantypes");
				if (scanTypeNode != null) {
					List<String> scanTypesAsList = convertToList(scanTypeNode);
					if (scanTypesAsList.size() >0) {
						//Find the count of sessions in a group and visit with given scantypes
						DataFreezeProjectHelper projectHelper = new DataFreezeProjectHelper(_user); 
						XFTTable result = null;
						if (xsiType.equals(XnatPetsessiondata.SCHEMA_ELEMENT_NAME)) {
							JsonNode tracerNode = dataType.get("tracers");
							String tracer = tracerNode.get(0).asText();
							result = projectHelper.getPetSessionCountsByScanTypesVisitAndGroupApplyTracerFilter(projectId,tracer,scanTypesAsList,visitsAsList,groupsAsList,true);
						}else {
							result = projectHelper.getExperimentCountsByScanTypesVisitAndGroup(projectId,xsiType,scanTypesAsList,visitsAsList,groupsAsList,true);
						}
						if (result != null) {
							//Object experimentCount = result.getCellValue("totalexperiments");
							int totalExperiments=DataFreezeUtils.getColumnSum(result,"totalexperiments");
							((ObjectNode)dataType).put("experimentcount", ""+totalExperiments);
						}
					}
				}
			}
		 result_json = userSelectionAsPOJO.toString();

		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
        context.put("userSelection",result_json);
		//data.setScreenTemplate("DataFreeze_Step4.vm");
    }

    
    public void doSharedata(RunData data, Context context) throws Exception {
    	String destination =  ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("destination",data));
    	data.setRedirectURI(destination);
    }    
    

    
    private List<String> convertToList(JsonNode node) {
    	List<String> list = new ArrayList<String>();
    	if (node != null && node.isArray()) {
    		for (int i=0; i< node.size() ;i++) {
    			JsonNode nodeElement = node.get(i);
    			list.add(nodeElement.asText());
    		}
    	}
    	return list;
    }
    
    private void redirectMe(RunData data) {
        String url = data.getRequest().getRequestURL().toString();
        int pos = url.indexOf("/action");
        if (pos != -1){
        	url = url.substring(0, pos);
        	data.setStatusCode(302);
        	data.setRedirectURI(url);
        }
    }

	
}
	
