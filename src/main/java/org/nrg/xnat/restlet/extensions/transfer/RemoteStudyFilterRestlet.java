/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */

package org.nrg.xnat.restlet.extensions.transfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.transfer.domain.RemoteStudyFilter;
import org.nrg.xnat.services.transfer.remote.RemoteRESTService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;
import org.springframework.http.ResponseEntity;

@XnatRestlet({ "/remote/study/filter/false" })
public class RemoteStudyFilterRestlet extends SecureResource {

	private final RemoteRESTService remoteRESTService = XDAT.getContextService().getBean(RemoteRESTService.class);

	public RemoteStudyFilterRestlet(Context context, Request request, Response response) throws ResourceException {
		super(context, request, response);
	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {

		try {
			if (!((RequestUtil.hasContent(this.getRequest().getEntity())
					&& RequestUtil.compareMediaType(this.getRequest().getEntity(), MediaType.APPLICATION_JSON)))) {
				throw new Exception("POST data must be valid json format");
			}

			String jsonbody = this.getRequest().getEntity().getText();

			RemoteStudyFilter studyFilter=null;
			try {
				studyFilter = (RemoteStudyFilter) new ObjectMapper().readValue(jsonbody, RemoteStudyFilter.class);
			} catch (Exception ex) {
				throw new Exception("POST data must be valid json format");
			}
			

			ResponseEntity<String>responseEntity=remoteRESTService.getStudies(studyFilter);

			//Response response = new Response(this.getRequest());
			//response.setEntity(responseEntity.getBody().toString(), MediaType.APPLICATION_JSON);
			//this.setResponse(response);
			getResponse().setEntity(responseEntity.getBody().toString(), MediaType.APPLICATION_JSON);
			getResponse().setStatus(Status.SUCCESS_OK);
		} catch (Exception exception) {
			logger.error("",exception);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, exception, exception.getMessage());
		}
	}

}
