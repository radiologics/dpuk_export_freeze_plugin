/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */

package org.nrg.xnat.restlet.extensions.transfer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.transfer.TransferService;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Rules;
import org.nrg.xnat.services.transfer.domain.Transfer;
import org.nrg.xnat.services.transfer.remote.RemoteTransferRequest;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@XnatRestlet({ "/exportremote/project/{SOURCEPROJECT}/project/{DESTPROJECT}" })
public class RemoteExportRestlet extends SecureResource {

	TransferService transferService;

	public RemoteExportRestlet(Context context, Request request, Response response) throws ResourceException {
		super(context, request, response);

	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {

		try {
			if (!((RequestUtil.hasContent(this.getRequest().getEntity())
					&& RequestUtil.compareMediaType(this.getRequest().getEntity(), MediaType.APPLICATION_JSON)))) {
				throw new Exception("POST data must be valid json format");
			}

			String jsonbody = this.getRequest().getEntity().getText();

			Payload payload;
			try {
				ObjectMapper objectMapper = new ObjectMapper();
            	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				payload = (Payload) new ObjectMapper().readValue(jsonbody, Payload.class);
			} catch (Exception ex) {
				throw new Exception("POST data must be valid json format");
			}

			Map<String, List<String>> params = this.getFullBodyFormAsMap();

			List<String> rules = (params.get("rule") != null) ? params.get("rule") : new ArrayList<String>();

			String dproject = (String) getRequest().getAttributes().get("DESTPROJECT");
			String sproject = (String) getRequest().getAttributes().get("SOURCEPROJECT");

			XnatProjectdata sProj = XnatProjectdata.getProjectByIDorAlias(sproject, getUser(), true);
			XnatProjectdata dProj = XnatProjectdata.getProjectByIDorAlias(dproject, getUser(), true);

			if (sProj == null) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Source project Not Found.");
				return;
			}
			if (dproject == null) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Destination project Not Found.");
				return;
			}
			if (!sProj.canRead(getUser())) {
				this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient privileges for source project.");
				return;
			}
			//should do this with remote project
			//if (!dProj.canEdit(user)) {
			//	this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,
			//			"Specified user account has insufficient privileges for destination project.");
			//	return;
			//}

			payload.setSproject(sproject);
			payload.setDproject(dproject);

			//String decodedpayload = new ObjectMapper().writeValueAsString(payload);

			//logger.error(decodedpayload);

			// rules/mapping not implemented yet. does nothing
			Rules rulesSet = new Rules(rules);
			Transfer transfer = new Transfer(payload, rulesSet);
			XDAT.sendJmsRequest(new RemoteTransferRequest(getUser(), transfer));
			getResponse().setStatus(Status.SUCCESS_OK);
		} catch (Exception exception) {
			logger.error("",exception);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, exception, exception.getMessage());
		}
	}

}
