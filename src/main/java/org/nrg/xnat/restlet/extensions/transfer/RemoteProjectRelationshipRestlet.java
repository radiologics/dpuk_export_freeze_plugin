/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */

package org.nrg.xnat.restlet.extensions.transfer;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.transfer.domain.RemoteProjectRelationship;
import org.nrg.xnat.services.transfer.remote.RemoteRESTService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


@XnatRestlet({ "/remote/projectrelations" })
public class RemoteProjectRelationshipRestlet extends SecureResource {

	private final RemoteRESTService remoteRESTService = XDAT.getContextService().getBean(RemoteRESTService.class);
    
	static Logger logger = Logger.getLogger(RemoteProjectRelationshipRestlet.class);
    
	public RemoteProjectRelationshipRestlet(Context context, Request request, Response response) throws ResourceException {
		super(context, request, response);
	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {

		try {
			if (!((RequestUtil.hasContent(this.getRequest().getEntity())
					&& RequestUtil.compareMediaType(this.getRequest().getEntity(), MediaType.APPLICATION_JSON)))) {
				throw new Exception("POST data must be valid json format");
			}

			String jsonbody = this.getRequest().getEntity().getText();

			RemoteProjectRelationship projectRelationship=null;
			try {
				ObjectMapper objectMapper = new ObjectMapper();
            	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				projectRelationship = (RemoteProjectRelationship) objectMapper.readValue(jsonbody, RemoteProjectRelationship.class);
				
			} catch (Exception ex) {
				throw new Exception("POST data must be valid json format");
			}
			
			
			ResponseEntity<String>response=remoteRESTService.importProjectRelationship(projectRelationship);

			getResponse().setStatus(Status.SUCCESS_OK);
		} catch (Exception exception) {
			logger.error("",exception);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, exception, exception.getMessage());
		}
	}

}
