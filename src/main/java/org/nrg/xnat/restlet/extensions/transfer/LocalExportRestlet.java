/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */

package org.nrg.xnat.restlet.extensions.transfer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.nrg.config.exceptions.ConfigServiceException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.transfer.TransferService;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Rules;
import org.nrg.xnat.services.transfer.domain.Transfer;
import org.nrg.xnat.services.transfer.local.SimpleTransferRequest;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@XnatRestlet({ "/exportlocal/project/{SOURCEPROJECT}/project/{DESTPROJECT}" })
public class LocalExportRestlet extends SecureResource {

	TransferService transferService;

	public LocalExportRestlet(Context context, Request request, Response response) throws ResourceException {
		super(context, request, response);

	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {

		try {
			if (!((RequestUtil.hasContent(this.getRequest().getEntity())
					&& RequestUtil.compareMediaType(this.getRequest().getEntity(), MediaType.APPLICATION_JSON)))) {
				throw new Exception("POST data must be valid json format");
			}
			//these configurations must exists.
			if (StringUtils.isEmpty(XDAT.getSiteConfigurationProperty("datafreeze-remote-script", ""))) {
				throw new Exception("Missing anonymization script");
			}
			if (StringUtils.isEmpty(XDAT.getSiteConfigurationProperty("datafreeze-remote-config", ""))) {
				throw new Exception("Missing field config script");
			}

			
			
			String jsonbody = this.getRequest().getEntity().getText();
			//just testing
			logger.debug(jsonbody);
			
			Payload payload = this.loadPayload(jsonbody);

			Map<String, List<String>> params = this.getFullBodyFormAsMap();

			// not used
			List<String> rules = (params.get("rule") != null) ? params.get("rule") : new ArrayList<String>();

			String dproject = (String) getRequest().getAttributes().get("DESTPROJECT");
			String sproject = (String) getRequest().getAttributes().get("SOURCEPROJECT");

			XnatProjectdata sProj = XnatProjectdata.getProjectByIDorAlias(sproject, getUser(), true);
			XnatProjectdata dProj = XnatProjectdata.getProjectByIDorAlias(dproject, getUser(), true);

			if (sProj == null) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Source project Not Found.");
				return;
			}
			if (dProj == null) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Destination project Not Found.");
				return;
			}
			if (!sProj.canRead(getUser())) {
				this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,
						"Specified user account has insufficient privileges for source project.");
				return;
			}
			if (!dProj.canEdit(getUser())) {
				this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,
						"Specified user account has insufficient privileges for destination project.");
				return;
			}

			if(payload.getCreatedby()==null || payload.getCreatedon()==null){
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Missing CreatedBy and CreatedOn in payload");
				return;
			}
			
			
			List<XnatSubjectdata> subjects = dProj.getParticipants_participant();
			if (subjects != null && subjects.size() > 0) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, "Destination project is not empty.");
				return;
			}

			payload.setSproject(sProj.getId());
			payload.setDproject(dProj.getId());

			// rules/mapping not implemented yet. does nothing
			Rules rulesSet = new Rules(rules);
			Transfer transfer = new Transfer(payload, rulesSet);
			XDAT.sendJmsRequest(new SimpleTransferRequest(getUser(), transfer));

			getResponse().setStatus(Status.SUCCESS_OK);
		} catch (Exception exception) {
			logger.error("Exception: " + exception.getMessage());
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, exception, exception.getMessage());
		}
	}

	// convert mapping (json mapping) to map.

	Payload loadPayload(String jsonbody) throws Exception, ConfigServiceException {
		Payload payload = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			payload = (Payload) objectMapper.readValue(jsonbody, Payload.class);
			logger.debug(jsonbody);
		} catch (Exception ex) {
			logger.debug(jsonbody);
			logger.error(ex.getMessage());
			throw new Exception("POST data must be valid json format");
		}

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String defaultPayloadJson = XDAT.getSiteConfigurationProperty("datafreeze-remote-config", "");
			Payload defaultpayload = (Payload) objectMapper.readValue(defaultPayloadJson, Payload.class);
			// merge default fields...
			payload.merge(defaultpayload);
			logger.debug(jsonbody);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(jsonbody);
			logger.error(ex.getMessage());
			throw new Exception("POST data must be valid json format");
		}
		return payload;
	}

}
