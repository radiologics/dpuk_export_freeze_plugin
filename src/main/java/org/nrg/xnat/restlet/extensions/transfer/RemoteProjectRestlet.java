/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */

package org.nrg.xnat.restlet.extensions.transfer;

import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.transfer.domain.RemoteProject;
import org.nrg.xnat.services.transfer.remote.RemoteRESTService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;
import org.springframework.http.ResponseEntity;

@XnatRestlet({ "/remote/projects/{PROJECT}" })
public class RemoteProjectRestlet extends SecureResource {

	private final RemoteRESTService remoteRESTService = XDAT.getContextService().getBean(RemoteRESTService.class);
    
	static Logger logger = Logger.getLogger(RemoteProjectRestlet.class);
    
	public RemoteProjectRestlet(Context context, Request request, Response response) throws ResourceException {
		super(context, request, response);
	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {

		try {
			if (!((RequestUtil.hasContent(this.getRequest().getEntity())
					&& RequestUtil.compareMediaType(this.getRequest().getEntity(), MediaType.APPLICATION_JSON)))) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"POST data must be valid json format");
				return;
			}

			String jsonbody = this.getRequest().getEntity().getText();

			RemoteProject rproject=null;
			try {
				rproject = (RemoteProject) new ObjectMapper().readValue(jsonbody, RemoteProject.class);
				
			} catch (Exception ex) {
				getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,ex,"POST data must be valid json format");
				return;
			}
			
			
			String dproject = (String) getRequest().getAttributes().get("PROJECT");
			rproject.setId(dproject);
			

			if (dproject == null) {
				this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "Destination project Not Found.");
				return;
			}
			
			ResponseEntity<String>response=remoteRESTService.importProject(rproject);

			getResponse().setStatus(Status.SUCCESS_OK);
		} catch (Exception exception) {
			logger.error("",exception);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, exception, exception.getMessage());
		}
	}

}
