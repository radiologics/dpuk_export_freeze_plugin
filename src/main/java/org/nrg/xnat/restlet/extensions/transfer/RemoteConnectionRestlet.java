/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */

package org.nrg.xnat.restlet.extensions.transfer;

import org.apache.log4j.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.util.RequestUtil;
import org.nrg.xnat.services.transfer.domain.RemoteConnection;
import org.nrg.xnat.services.transfer.remote.RemoteRESTService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;
import org.springframework.http.HttpStatus;

@XnatRestlet({ "/remote/connection" })
public class RemoteConnectionRestlet extends SecureResource {

	private final RemoteRESTService remoteRESTService = XDAT.getContextService().getBean(RemoteRESTService.class);
    
	static Logger logger = Logger.getLogger(RemoteProjectRestlet.class);
    
	public RemoteConnectionRestlet(Context context, Request request, Response response) throws ResourceException {
		super(context, request, response);
	}

	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {

		try {
			if (!((RequestUtil.hasContent(this.getRequest().getEntity())
					&& RequestUtil.compareMediaType(this.getRequest().getEntity(), MediaType.APPLICATION_JSON)))) {
				throw new Exception("POST data must be valid json format");
			}

			String jsonbody = this.getRequest().getEntity().getText();

			RemoteConnection connection=null;
			try {
				connection = (RemoteConnection) new ObjectMapper().readValue(jsonbody, RemoteConnection.class);
				
			} catch (Exception ex) {
				throw new Exception("POST data must be valid json format");
			}
			
			RemoteConnection response=remoteRESTService.getConnection(connection);
			
			String jsonConnection = new ObjectMapper().writeValueAsString(response);
			getResponse().setEntity(jsonConnection, MediaType.APPLICATION_JSON);
			getResponse().setStatus(Status.SUCCESS_OK);
		}catch(org.springframework.web.client.HttpClientErrorException httpex){
			logger.error("",httpex);
			if(HttpStatus.UNAUTHORIZED==httpex.getStatusCode()){
				getResponse().setStatus(Status.CLIENT_ERROR_UNAUTHORIZED, httpex, httpex.getMessage());
			}else if(HttpStatus.NOT_FOUND==httpex.getStatusCode()){
				getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND, httpex, httpex.getMessage());
			}else{
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, httpex, httpex.getMessage());	
			}
		}
		catch (Exception exception) {
			logger.error("",exception);
			getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, exception, exception.getMessage());
		}
	}

}
