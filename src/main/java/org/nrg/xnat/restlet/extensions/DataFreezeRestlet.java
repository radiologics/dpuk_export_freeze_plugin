//Copyright (c) 2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.restlet.extensions;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.DBPoolException;
import org.nrg.xnat.datafreeze.handlers.DataFreezeExperimentHandlerI;
import org.nrg.xnat.datafreeze.handlers.ExperimentHandler;
import org.nrg.xnat.datafreeze.handlers.GenericDataFreezeHandler;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mohana Ramaratnam
 *
 */

@XnatRestlet(value={"/projects/{PROJECT_ID}/datafreeze"})
public class DataFreezeRestlet extends SecureResource {
	    public static final String PARAM_PROJECT_ID = "PROJECT_ID";
	    private final String _projectId;

	    private static final Logger _log = LoggerFactory.getLogger(DataFreezeRestlet.class);


	    public DataFreezeRestlet(Context context, Request request, Response response) throws ResourceException {
	        super(context, request, response);

	        _projectId = (String) getRequest().getAttributes().get(PARAM_PROJECT_ID);

	        if (StringUtils.isBlank(_projectId)) {
	            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "No project specified");
	        }

	        getVariants().add(new Variant(MediaType.ALL));
	    }
	    
    
	    @Override
	    public Representation getRepresentation(Variant variant) {  
	        if (_log.isDebugEnabled()) {
	            _log.debug("Returning experiment details");
	        }
        	String groupsAsCSV = getQueryVariable("groups");
        	String visitsAsCSV = getQueryVariable("visits");
        	String xsiType = getQueryVariable("xsitype");
	        XFTTable table;
	        DataFreezeExperimentHandlerI handler=null;
	        try {
		        List<DataFreezeExperimentHandlerI> handlers = ExperimentHandler.getHandlers();
		        List<DataFreezeExperimentHandlerI> synHandlerslist = Collections.synchronizedList(handlers);
	        	for(DataFreezeExperimentHandlerI experimentSpecificHandler:synHandlerslist){
					if(experimentSpecificHandler.canHandle(xsiType)){
						handler=experimentSpecificHandler;
						break;
					}
				}
			} catch (InstantiationException e1) {
				logger.error("",e1);
			} catch (IllegalAccessException e1) {
				logger.error("",e1);
			}
	        Hashtable<String,Object> params=new Hashtable<String,Object>();
	        params.put("groups", groupsAsCSV);
	        params.put("visits", visitsAsCSV);
	        params.put("xsiType", xsiType);
	        params.put("project", _projectId);
	        try {
		        if(handler!=null){
		        	table=handler.build(getUser(),params);
		        }else{
		        	//unable to get experiment specific filter...using generic one
		        	DataFreezeExperimentHandlerI genericHandler = new GenericDataFreezeHandler();
		        	table=genericHandler.build(getUser(),params);
		        }
		            
	        } catch (SQLException e) {
	            logger.error("Error occurred executing database query", e);
	            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
	            return null;
	        } catch (DBPoolException e) {
	            logger.error("Error occurred connecting to database", e);
	            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
	            return null;
	        } catch (Exception e) {
	            logger.error("Unknown error occurred",e);
	            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
	            return null;
	        }
	        
	        if(table==null){
	        	return null;
	        }

	        MediaType mt = overrideVariant(variant);
	        if(table!=null) {
	            params.put("totalRecords", table.size());
	        }
	        return this.representTable(table, mt, params);
	    }
}
