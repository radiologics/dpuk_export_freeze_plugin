/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import java.util.List;

import javax.mail.MessagingException;

import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xnat.services.transfer.TransferService;
import org.nrg.xnat.services.transfer.domain.ManifestEntry;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// TODO: Auto-generated Javadoc
/**
 * The Class SimpleTransferRequestHandlerImpl.
 */
@Service
public class SimpleTransferRequestHandlerImpl implements SimpleTransferRequestHandler {

	/** The transfer service. */
	@Autowired
	TransferService transferService;

	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.local.SimpleTransferRequestHandler#handle(org.nrg.xnat.services.transfer.local.SimpleTransferRequest)
	 */
	@SuppressWarnings("unused")
	public void handle(SimpleTransferRequest request)   {
		try {
			logger.info("handle(SimpleTransferRequest request)");
			transferService.transfer(request.getUser(), request.getTransfer());
			emailSuccess(request);

		} catch (Exception e) {
			final String message = String.format("An error occurred processing a request from user %s to perform some operation ",request.getUser().getUsername());
			logger.error("", e);
			emailFailedUser(request);
			emailFailedAdmin(request);

		}
		
	}

	
	/**
	 * Delete on failure.
	 *
	 * @param request the request
	 */
	void deleteOnFailure(final SimpleTransferRequest request)   {
		
			try {
				logger.debug("deleteOnFailure");
				Payload payload=request.getTransfer().getPayload();
				XDATUser user= new XDATUser(request.getUser().getUsername());
				if(payload.getDeleteOnFailure()!=null &&request.getTransfer().getPayload().getDeleteOnFailure()==true){
					XnatProjectdata proj= XnatProjectdata.getProjectByIDorAlias(payload.getDproject(),new XDATUser(request.getUser().getUsername()), false);
					proj.delete(true, user, null);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("cannot delete destination project"+e.getMessage());

				e.printStackTrace();
			}
		
	}


	/**
	 * Format success email to requesting user.
	 *
	 * @param request the request
	 */
	void emailSuccess(final SimpleTransferRequest request) {
		
	
		try {
			String subject="Data Freeze created on "+XDAT.getSiteConfigPreferences().getSiteId()+" node";
			StringBuilder sb = new StringBuilder();
			
			Payload payload=request.getTransfer().getPayload();

			
			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>The following data freeze was created from project "+payload.getSproject()+" on "+TurbineUtils.GetFullServerPath()+" to "+TurbineUtils.GetFullServerPath()+"/data/projects/"+payload.getDproject()+" requested by "+request.getUser().getUsername()+". This data freeze is now ready to share.</p>");
			
			
			sb.append("<table>");
			sb.append("<th> Source Project </th>");
			sb.append("<th> Target Project </th>");
			sb.append("<th> Label </th>");
			sb.append("<th> Message </th>");
			sb.append("<th> Status </th>");
			List<ManifestEntry> entries = request.getTransfer().getPayload().getManifest().getEntries();
			
			for (ManifestEntry payloadManifestEntry : entries) {
				 sb.append("<tr>");
				 sb.append("<td> " + payload.getSproject() + " </td>");
				 sb.append("<td> " + payload.getDproject() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getEntry() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getMessage() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getStatus() + " </td>");
				 sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			XDAT.getMailService().sendHtmlMessage(XDAT.getSiteConfigPreferences().getAdminEmail(), request.getUser().getEmail(), subject,
					sb.toString());
		} catch (MessagingException me) {
			logger.error("Failed to send email.", me);
		} catch (Exception e) {
			logger.error("Failed to send email.", e);
		}

	}
	
	/**
	 * Email failed user.
	 *
	 * @param request the request
	 */
	void emailFailedUser(final SimpleTransferRequest request) {

		try {
			String subject="Data Freeze creation failed on "+XDAT.getSiteConfigPreferences().getSiteId()+" node";
			StringBuilder sb = new StringBuilder();
		
			Payload payload=request.getTransfer().getPayload();

			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>The data freeze "+payload.getDproject()+" failed for project "+payload.getSproject()+" on "+TurbineUtils.GetFullServerPath()+" to "+TurbineUtils.GetFullServerPath()+"/data/projects/"+payload.getDproject()+" requested by "+request.getUser().getUsername()+". Please contact your administrator.</p>");
			
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			XDAT.getMailService().sendHtmlMessage(XDAT.getSiteConfigPreferences().getAdminEmail(), request.getUser().getEmail(), subject,sb.toString());
		} catch (MessagingException me) {
			logger.error("Failed to send email.", me);
		} catch (Exception e) {
			logger.error("Failed to send email.", e);
		}
		}
	
	/**
	 * Email failed admin.
	 *
	 * @param request the request
	 */
	void emailFailedAdmin(final SimpleTransferRequest request) {

		
		
		try {
			String subject="Data Freeze creation failed on "+XDAT.getSiteConfigPreferences().getSiteId()+" node";
			StringBuilder sb = new StringBuilder();
			
			Payload payload=request.getTransfer().getPayload();

			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>The data freeze "+payload.getDproject()+" failed for project "+payload.getSproject()+" on "+TurbineUtils.GetFullServerPath()+" to "+TurbineUtils.GetFullServerPath()+"/data/projects/"+payload.getDproject()+" requested by "+request.getUser().getUsername()+". Please review the following transcript of results.</p>");
			sb.append("For details see: "+TurbineUtils.GetFullServerPath()+"/data/projects/"+payload.getSproject()+"/resources/datafreeze/files/"+payload.getJsonFilename()+"</p>");

			
			sb.append("<table>");
			sb.append("<th> Source Project </th>");
			sb.append("<th> Target Project </th>");
			sb.append("<th> Label </th>");
			sb.append("<th> Message </th>");
			sb.append("<th> Status </th>");
			
			List<ManifestEntry> entries = request.getTransfer().getPayload().getManifest().getEntries();
			
			for (ManifestEntry payloadManifestEntry : entries) {
				 sb.append("<tr>");
				 sb.append("<td> " + payload.getSproject() + " </td>");
				 sb.append("<td> " + payload.getDproject() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getEntry() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getMessage() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getStatus() + " </td>");
				 sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			XDAT.getMailService().sendHtmlMessage(XDAT.getSiteConfigPreferences().getAdminEmail(), XDAT.getSiteConfigPreferences().getAdminEmail(), subject,sb.toString());
		} catch (MessagingException me) {
			logger.error("Failed to send email.", me);
		} catch (Exception e) {
			logger.error("Failed to send email.", e);
		}
		}
	
	
	
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(SimpleTransferRequestHandler.class);
}
