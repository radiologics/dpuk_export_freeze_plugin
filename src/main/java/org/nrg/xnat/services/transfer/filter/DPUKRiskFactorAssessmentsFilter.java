/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.SessionFilter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


public class DPUKRiskFactorAssessmentsFilter implements Filter {

	
	public static Logger logger = Logger.getLogger(DPUKRiskFactorAssessmentsFilter.class);

	final String FILTERTYPE="DPUKRiskFactorAssessments"; 

	@Override
	public List<String> filter(SessionFilter filter,Payload payload) {
		List<String> results = new ArrayList<String>();
		try {

			if (payload.getGroups().size() == 0) {
				throw new Exception("Missing groups");
			}
			if (payload.getVisits().size() == 0) {
				throw new Exception("Missing visit");
			}

			String query = "select distinct(dpuk.id) as experiment_id from dpuk_dpukriskfactorassessmentdata  dpuk  ";
			query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=dpuk.id  ";
			query += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id  "; 
			query += " LEFT JOIN xnat_experimentdata e ON e.id=dpuk.id  ";

			query += " where e.project = :project ";
			
			if(payload.getGroups().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)){
				query += " and (s._group in (:groups) or s._group is null)";
			}else{
				query += " and s._group in  (:groups) ";
			}
			if(payload.getVisits().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)){
				query += " and (e.visit_id in  (:visits) or e.visit_id is null)";
			}else{
				query += " and e.visit_id in  (:visits) ";
			}
			
			query += " ORDER BY dpuk.id DESC ";
			logger.error(query);
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("groups", payload.getGroups());
			parameters.addValue("visits", payload.getVisits());
			parameters.addValue("project", payload.getSproject());

			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			results = jdbcTemplate.queryForList(query, parameters, String.class);

			logger.debug(query);
			for (String result : results) {
				logger.debug("DPUKRiskAssessmentFilter whitelist "+result);
			}
			return results;
		} catch (Exception e) {
			logger.error("failed to find sessions", e);
			return new ArrayList<String>();// empty
		}

	}

	
	
}
