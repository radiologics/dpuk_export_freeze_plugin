package org.nrg.xnat.services.transfer.remote;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.nrg.xnat.services.transfer.domain.RemoteConnection;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractRemoteRESTService.
 */
public abstract class AbstractRemoteRESTService {
	
	
	//@Autowired
	//SimpleClientHttpRequestFactory requestFactory;

	/** The logger. */
	public static Logger logger = Logger.getLogger(AbstractRemoteRESTService.class);

	/**
	 * Gets the resttemplate.
	 *
	 * @return the resttemplate
	 */
	public RestTemplate getResttemplate(){
		SimpleClientHttpRequestFactory requestFactory =new SimpleClientHttpRequestFactory();
		//requestFactory.setBufferRequestBody(false);
		return new RestTemplate(requestFactory);
	}

	/**
	 * Sets the jsessionid.
	 *
	 * @param connection the connection
	 * @return the string
	 */
	String setJsessionid(RemoteConnection connection){
		HttpEntity<String> request = new HttpEntity<String>(this.getAuthHeaders(connection));
		ResponseEntity<String> response = getResttemplate().exchange(connection.getUrl()+"/data/JSESSIONID", HttpMethod.POST, request, String.class);
		logger.info(response);
		logger.info(response.getBody());
		connection.setJsessionid(response.getBody());
		return response.getBody();
	}
	
	
	
	/**
	 * Gets the auth headers.
	 *
	 * @param connection the connection
	 * @return the auth headers
	 */
	HttpHeaders getAuthHeaders(RemoteConnection connection){
		HttpHeaders headers = new HttpHeaders();
		if(connection.getJsessionid()==null){
			headers.add("Authorization", "Basic " + getBase64Credentials(connection));
			//connection.setJsessionid(this.getSessionId(connection);
			logger.info("adding "+" Basic " + getBase64Credentials(connection));
		}else{
			headers.add("Cookie", "JSESSIONID=" + connection.getJsessionid());
			logger.info("adding "+"JSESSIONID=" + connection.getJsessionid());
		}
		return headers;
	}
	
	/**
	 * Gets the base64 credentials.
	 *
	 * @param conn the conn
	 * @return the base64 credentials
	 */
	String getBase64Credentials(RemoteConnection conn) {
		String plainCreds;
		//if(conn.getAlias()!=null){
		//	plainCreds = conn.getAlias().getAlias()+":"+conn.getAlias().getToken();
		//}else{
			plainCreds = conn.getUsername()+":"+conn.getPassword();
		//}
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		return base64Creds;
	}
}
