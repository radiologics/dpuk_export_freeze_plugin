/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import org.nrg.xdat.om.XnatImagesessiondata;


public interface SimpleExportAnonymizerService {
		  void anonymize(final XnatImagesessiondata session, final String origproject) throws Exception;     
}
