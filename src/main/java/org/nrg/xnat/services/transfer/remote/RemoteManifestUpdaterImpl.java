/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.remote;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xdat.model.XnatResourcecatalogI;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectResourceModifierBuilder;
import org.nrg.xnat.helpers.resource.direct.ResourceModifierA.UpdateMeta;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.services.transfer.domain.ManifestEntry;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Payload.PayloadType;
import org.nrg.xnat.services.transfer.domain.RemoteConnection;
import org.nrg.xnat.utils.CatalogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

// TODO: Auto-generated Javadoc
/**
 * The Class RemoteManifestUpdaterImpl.
 */
@Service
public class RemoteManifestUpdaterImpl implements RemoteManifestUpdater {
	
	
	
	/** The remote rest service. */
	@Autowired
	RemoteRESTService remoteRESTService;
	
	
	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.remote.RemoteManifestUpdater#update(org.nrg.xft.security.UserI, org.nrg.xnat.services.transfer.domain.Payload.PayloadType, java.lang.String, org.nrg.xnat.services.transfer.domain.ManifestEntry, org.nrg.xnat.services.transfer.domain.Payload)
	 */
	public void update(UserI user,PayloadType type,String status,ManifestEntry entry,Payload payload) {
		try {
			payload.addManifestEntry(type, status,entry);
			Payload freezePayload=this.getPayload(user, payload.getSproject(), payload);
			freezePayload.addManifestEntry(type, status, entry);
			this.update(user, freezePayload.getSproject(),freezePayload);
			this.update(user, freezePayload.getDproject(),freezePayload);
			
		} catch (Exception e) {
			logger.error("Failed to update payload",e);
			throw new RuntimeException("Failed to update payload");

		}	
	}
	
	/*
	 * 
	 * 
	 * (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.remote.RemoteManifestUpdater#updateRemote(org.nrg.xft.security.UserI, org.nrg.xnat.services.transfer.domain.Payload)
	 */
	public void updateRemote(UserI user,Payload payload) {
		try {
			Payload freezePayload=this.getPayload(user, payload.getSproject(), payload);
			//frezezePayload is missing remote destination, add here.
			freezePayload.setRproject(payload.getDproject());
			this.update(user, freezePayload.getSproject(),freezePayload);
			this.update(user, freezePayload.getDproject(),freezePayload);
			this.updateRemoteInternal(user,payload.getConnection(),payload.getDproject(), freezePayload);	
		} catch (Exception e) {
			logger.error("Failed to update payload",e);
			
			throw new RuntimeException("Failed to update payload");

		}
	}
	

	/**
	 * Update remote internal.
	 *
	 * @param user the user
	 * @param payload the payload
	 */
	private void updateRemoteInternal(UserI user,RemoteConnection connection,String remoteproject,Payload payload) {
		try {
			String name = payload.getDproject() + "_" + payload.getCreatedon() + "_" + payload.getCreatedby() + ".json";

			
			ObjectMapper objectMapper = new ObjectMapper();
        	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String freezePayloadString = objectMapper.writeValueAsString(payload);
			File tempPayload=File.createTempFile("payload2", "json");
			FileUtils.writeStringToFile(tempPayload, freezePayloadString);
			remoteRESTService.importPayload(connection, remoteproject, tempPayload,name);
		} catch (Exception e) {
			logger.error("Failed to update remote payload",e);
			throw new RuntimeException("Failed to update remote payload");
		}
		
				
	}

	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.remote.RemoteManifestUpdater#verify(org.nrg.xft.security.UserI, org.nrg.xnat.services.transfer.domain.Payload)
	 */
	public void verify(UserI user,Payload payload) throws Exception {
		
			Payload freezePayload=this.getPayload(user, payload.getSproject(), payload);
			
			int status_subj=	Integer.compare(freezePayload.getCurrentremotesubjects(), freezePayload.getTotalsubjects());
			int status_exp=		Integer.compare(freezePayload.getCurrentremoteexperiments(), freezePayload.getTotalexperiments());
			if(status_subj!=0){
				logger.error("subjects: mismatch: "+freezePayload.getCurrentremotesubjects()+" / "+ freezePayload.getTotalsubjects());
				throw new RuntimeException("Manifest verification failed. subjects: mismatch: "+freezePayload.getCurrentremotesubjects()+" / "+ freezePayload.getTotalsubjects());
			}
			if(status_exp!=0){
				logger.error("experiments: mismatch: "+freezePayload.getCurrentremoteexperiments()+" / "+ freezePayload.getTotalexperiments());
				throw new RuntimeException("Manifest verification failed. experiments: mismatch: "+freezePayload.getCurrentremoteexperiments()+" / "+ freezePayload.getTotalexperiments());
			}			
			
		}
		
	
	
	
	/**
	 * Gets the payload.
	 *
	 * @param user the user
	 * @param project the project
	 * @param payload the payload
	 * @return the payload
	 * @throws Exception the exception
	 */
	private Payload getPayload(UserI user,String project, Payload payload) throws Exception {
		Payload freezePayload=new Payload();
		String name = payload.getDproject() + "_" + payload.getCreatedon() + "_" + payload.getCreatedby() + ".json";

		XnatProjectdata proj;
		try {
			proj = XnatProjectdata.getProjectByIDorAlias(project, new XDATUser(user.getUsername()),false);
			DirectResourceModifierBuilder builder = new DirectResourceModifierBuilder();
			builder.setProject(proj);
			List<XnatResourcecatalogI> resources = proj.getResources_resource();

			for (XnatResourcecatalogI xnatAbstractresourceI : resources) {
				final String type = xnatAbstractresourceI.getLabel();
				if ("datafreeze".equals(type)) {
					final File catalogFile = CatalogUtils.getCatalogFile(proj.getArchiveRootPath(),
							xnatAbstractresourceI);
					CatCatalogBean cat = CatalogUtils.getCatalog(catalogFile);
					for (CatEntryI match : CatalogUtils.getEntriesByRegex(cat, ".*.json")) {
						
							String parentPath = (new File(xnatAbstractresourceI.getUri())).getParent();
							File json = CatalogUtils.getFile(match, parentPath);

							String decodedpayload = new ObjectMapper().writeValueAsString(payload);
							logger.info("getPayload:" + json.getAbsolutePath());

							String jsonString=FileUtils.readFileToString(json);
							ObjectMapper objectMapper = new ObjectMapper();
				            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				            freezePayload=(Payload)objectMapper.readValue(jsonString, Payload.class);
					}
				}

			}
		} catch (Exception e){ 
			logger.error("",e);
			throw new RuntimeException("Failed to retrieve source payload");
		}
		return freezePayload;
	}
	
	/**
	 * Update.
	 *
	 * @param user the user
	 * @param project the project
	 * @param payload the payload
	 */
	private void update(UserI user,String project, Payload payload) {
		String name = payload.getDproject() + "_" + payload.getCreatedon() + "_" + payload.getCreatedby() + ".json";

		XnatProjectdata proj;
		try {
			proj = XnatProjectdata.getProjectByIDorAlias(project, new XDATUser(user.getUsername()),false);
			DirectResourceModifierBuilder builder = new DirectResourceModifierBuilder();
			builder.setProject(proj);
			List<XnatResourcecatalogI> resources = proj.getResources_resource();

			for (XnatResourcecatalogI xnatAbstractresourceI : resources) {
				final String type = xnatAbstractresourceI.getLabel();
				if ("datafreeze".equals(type)) {
					final File catalogFile = CatalogUtils.getCatalogFile(proj.getArchiveRootPath(),
							xnatAbstractresourceI);
					CatCatalogBean cat = CatalogUtils.getCatalog(catalogFile);
					for (CatEntryI match : CatalogUtils.getEntriesByRegex(cat, ".*.json")) {
						if (match.getName().equals(name)) {
							String parentPath = (new File(xnatAbstractresourceI.getUri())).getParent();
							File json = CatalogUtils.getFile(match, parentPath);

							String decodedpayload = new ObjectMapper().writeValueAsString(payload);
							logger.info("update:" + json.getAbsolutePath());
							File temp = File.createTempFile("xnat", "json");

							FileUtils.writeStringToFile(temp, decodedpayload);

							final EventMetaI i = EventUtils.DEFAULT_EVENT(user, null);
							UpdateMeta um = new UpdateMeta(i, true);
							builder.buildResourceModifier(true, new XDATUser(user.getUsername()), i).addFile(
									(List<? extends FileWriterWrapperI>) Lists.newArrayList(new StoredFile(temp, true)),
									xnatAbstractresourceI.getXnatAbstractresourceId(), null, json.getName(), buildResourceInfo(um, user),
									false);
						}
					}
				}

			}
		} catch (Exception e) {
			logger.error("",e);
			throw new RuntimeException("Failed to update payload");
		}
	}

	/**
	 * Builds the resource info.
	 *
	 * @param ci the ci
	 * @param user the user
	 * @return the xnat resource info
	 */
	public XnatResourceInfo buildResourceInfo(EventMetaI ci, UserI user) {
		Date d = EventUtils.getEventDate(ci, false);
		return XnatResourceInfo.buildResourceInfo(null, null, null, null, user, d, d, EventUtils.getEventId(ci));
	}

	/** The logger. */
	public static Logger logger = Logger.getLogger(RemoteManifestUpdaterImpl.class);

}