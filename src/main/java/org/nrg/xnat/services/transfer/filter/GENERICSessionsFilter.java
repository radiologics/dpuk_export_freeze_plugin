/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.SessionFilter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


/**
 * The Class MRSessionFilter.
 */

public class GENERICSessionsFilter implements Filter {

	/** The logger. */
	public static Logger logger = Logger.getLogger(GENERICSessionsFilter.class);

	/** The filtertype. */
	final String FILTERTYPE="GENERICSession"; 
	
	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.filter.Filter#filter(org.nrg.xnat.services.transfer.domain.Payload)
	 */
	@Override
	public List<String> filter(SessionFilter filter,Payload payload) {
		List<String> results = new ArrayList<String>();
		try {

			List<String> scantypes=new ArrayList<String>();
			//get the scantype filters.
			
			if(filter.getFiltertype()!=null){
				scantypes.addAll(filter.getScantypes());
			}
			
			
			if (payload.getGroups().size() == 0) {
				throw new Exception("Missing groups");
			}
			if (payload.getVisits().size() == 0) {
				throw new Exception("Missing visit");
			}
			// String query = "select
			// e.label,s.label,e.visit_id,s._group,e.project from
			// xnat_experimentdata e ";
			String query = "select distinct(e.id) as experiment_id from xnat_experimentdata e ";
			query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id ";
			query += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id ";
			query += " LEFT JOIN xnat_imagescandata scan ON e.id=scan.image_session_id ";
			query += " where e.project = :project ";
			query += " and scan.type in  (:scantype) ";
			query += " and s._group in  (:groups) ";
			query += " and e.visit_id in (:visits) ";
			query += " ORDER BY e.id DESC ";
			logger.error(query);
			MapSqlParameterSource parameters = new MapSqlParameterSource();

			parameters.addValue("groups", payload.getGroups());
			parameters.addValue("visits", payload.getVisits());
			parameters.addValue("project", payload.getSproject());
			parameters.addValue("scantype",scantypes);

			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			results = jdbcTemplate.queryForList(query, parameters, String.class);

			logger.error(query);

			return results;
		} catch (Exception e) {
			logger.error("failed to find sessions", e);
			return new ArrayList<String>();// empty
		}

	}
}
