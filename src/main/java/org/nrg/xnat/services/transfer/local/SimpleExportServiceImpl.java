/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.model.XnatExperimentdataShareI;
import org.nrg.xdat.model.XnatImageassessordataI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.model.XnatReconstructedimagedataI;
import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.model.XnatSubjectdataI;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatDemographicdata;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatResourceseries;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;
import org.nrg.xnat.exceptions.InvalidArchiveStructure;
import org.nrg.xnat.services.transfer.SimpleExportService;
import org.nrg.xnat.services.transfer.domain.ManifestEntry;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Payload.PayloadType;
import org.nrg.xnat.services.transfer.domain.SessionFilter;
import org.nrg.xnat.services.transfer.filter.Filter;
import org.nrg.xnat.services.transfer.filter.FilterLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * The Class SimpleExportServiceImpl.
 *
 * @author james@radiologics.com
 */

@Service
public class SimpleExportServiceImpl implements SimpleExportService {

	/** The Constant PATTERN. */
	private static final String PATTERN = "_v[0-9]{1,8}_\\d+$";

	/** The logger. */
	public static Logger logger = Logger.getLogger(SimpleExportServiceImpl.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8092462697472805786L;

	/** The pattern. */
	java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(PATTERN);

	/** The simple export anonymizer service. */
	@Autowired
	SimpleExportAnonymizerService simpleExportAnonymizerService;

	/** The simple export date calculations service. */
	@Autowired
	SimpleExportDelayCalculationsService simpleExportDateCalculationsService;

	/** The simple export age at scan calculations service. */
	@Autowired
	SimpleExportAgeAtScanCalculationsService simpleExportAgeAtScanCalculationsService;

	/** The filter loader. */
	@Autowired
	FilterLoader filterLoader;

	/** The manifest updater. */
	@Autowired
	SimpleManifestUpdater manifestUpdater;

	/**
	 * Instantiates a new simple export service impl.
	 */
	public SimpleExportServiceImpl() {
		super();
	}

	/**
	 * Simple export service to copy subjects from destination project and
	 * filter based on provided filters. Payload includes source project, dest
	 * project and additional filters.
	 *
	 * @param user
	 *            the user
	 * @param payload
	 *            the payload
	 * @param ed
	 *            the ed
	 * @throws Exception
	 *             the exception
	 */

	public void export(UserI user, Payload payload, EventDetails ed) throws Exception {
		// Manifest.
		String dproject = "";
		try {
			dproject = payload.getDproject();

			XnatProjectdata dproj = XnatProjectdata.getProjectByIDorAlias(dproject, (XDATUser) user, true);

			String sproject = payload.getSproject();
			XnatProjectdata sproj = XnatProjectdata.getProjectByIDorAlias(sproject, (XDATUser) user, true);

			// List<XnatSubjectdata> subjects=new ArrayList<XnatSubjectdata>();

			XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(payload.getSproject(), (XDATUser) user, true);

			List<String> filteredSessionsIdsByPayload = findSessionsByPayload(payload);
			payload.setExperiments(filteredSessionsIdsByPayload);
			payload.setTotalexperiments(filteredSessionsIdsByPayload.size());

			// fill in the subjects,,,
			// update payload
			// TODO fix this nonsense....
			List<String> subjectIds = findSubjectsByExperiments(user, payload);
			payload.setSubjects(subjectIds);
			payload.setTotalsubjects(subjectIds.size());

			List<XnatSubjectdata> subjects = this.getFilteredSubjects(user, subjectIds);

			payload.addManifestEntry(PayloadType.LOCALPROJECT, Payload.STAGING_IN_PROGRESS,
					new ManifestEntry(dproject, ManifestEntry.SUCCESS, "Start Preparing Data", ""));

			manifestUpdater.update(user, payload);

			for (XnatSubjectdata subj : subjects) {
				// Step 1 copy and anonymize
				XnatSubjectdata newSubj = this.copySubject(user, payload, dproj, subj, ed);
				logger.info("copied new subject," + newSubj.getLabel());
			}

			// finally verify manifest
			manifestUpdater.verify(user, payload);

			payload.addManifestEntry(PayloadType.LOCALPROJECT, Payload.STAGING_COMPLETE,
					new ManifestEntry(dproject, ManifestEntry.SUCCESS, "Finished Preparing Data", ""));
			manifestUpdater.update(user, payload);
		} catch (Exception ex) {
			payload.addManifestEntry(PayloadType.LOCALPROJECT, Payload.STAGING_FAILED,
					new ManifestEntry(dproject, ManifestEntry.FAIL, "Failed Preparing Data", ex.getMessage()));
			manifestUpdater.update(user, payload);
			throw new Exception(ex);
		}
	}

	/**
	 * Fix scan types.
	 *
	 * @param orig
	 *            the orig
	 * @param exp
	 *            the exp
	 * @param payload
	 *            the payload
	 */
	private void fixScanTypes(XnatExperimentdata orig, XnatExperimentdata exp, Payload payload) {

		SessionFilter sessionFilter = this.getSessionFilter(payload, orig.getId(), orig.getXSIType());

		List<XnatImagescandataI> scans = ((XnatImagesessiondata) exp).getScans_scan();
		for (int i = 0; i < scans.size(); i++) {
			Map<String, String> map = sessionFilter.getMappings();
			if (map != null) {
				String newType = map.get(scans.get(i).getType());
				if (newType != null) {
					payload.addLog(exp.getLabel()+":"+"fixScanTypes:mappings change: " + scans.get(i).getType() + " to " + newType);
					scans.get(i).setType(newType);
				} else {
					payload.addLog(exp.getLabel()+":"+"fixScanTypes:mappings newType is null, leaving: " + scans.get(i).getType());
				}
			} else {
				payload.addLog(exp.getLabel()+":"+"fixScanTypes:mappings: no mappings found");

			}
		}
		return;
	}

	private List<String> findSubjectsByExperiments(org.nrg.xft.security.UserI user, Payload payload) {

		List<String> subjects = new ArrayList<String>();
		try {

			if (payload.getExperiments().size() == 0) {
				throw new Exception("Missing experiments");
			}

			String query = "select distinct(s.id) as subject_id from xnat_experimentdata e ";
			query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id ";
			query += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id ";
			query += " where e.project = :project ";
			query += " and e.id in  (:experiments) ";
			query += " ORDER BY s.id DESC ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();

			parameters.addValue("experiments", payload.getExperiments());
			parameters.addValue("project", payload.getSproject());

			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			subjects = jdbcTemplate.queryForList(query, parameters, String.class);

			//payload.addLog(query);
			return subjects;
		} catch (Exception e) {
			logger.error("failed to find subjects", e);
			return new ArrayList<String>();// empty
		}

	}

	/**
	 * Find subjects by visit and study group.
	 *
	 * @param user
	 *            the user the payload
	 * @return the list
	 */
	private List<String> findSubjectsByVisitAndStudyGroup(org.nrg.xft.security.UserI user, Payload payload) {
		List<String> subjects = new ArrayList<String>();
		try {

			if (payload.getGroups().size() == 0) {
				throw new Exception("Missing groups");
			}
			if (payload.getVisits().size() == 0) {
				throw new Exception("Missing visit");
			}

			String query = "select distinct(s.id) as subject_id from xnat_experimentdata e ";
			query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id ";
			query += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id ";
			query += " where e.project = :project ";
			if (payload.getGroups().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)) {
				query += " and (s._group in (:groups) or s._group is null)";
			} else {
				query += " and s._group in  (:groups) ";
			}
			if (payload.getVisits().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)) {
				query += " and (e.visit_id in  (:visits) or e.visit_id is null)";
			} else {
				query += " and e.visit_id in  (:visits) ";
			}
			query += " ORDER BY s.id DESC ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();

			parameters.addValue("groups", payload.getGroups());
			parameters.addValue("visits", payload.getVisits());
			parameters.addValue("project", payload.getSproject());

			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			subjects = jdbcTemplate.queryForList(query, parameters, String.class);

			payload.addLog(query);
			return subjects;
		} catch (Exception e) {
			logger.error("failed to find subjects", e);
			return new ArrayList<String>();// empty
		}

	}

	/**
	 * Find sessions by payload.
	 *
	 * @param payload
	 *            the payload
	 * @return the list
	 */
	private List<String> findSessionsByPayload(Payload payload) {
		List<String> results = new ArrayList<String>();
		try {

			List<SessionFilter> filters = payload.getSessionfilters();

			for (SessionFilter payloadSessionFilter : filters) {
				Filter filter = filterLoader.load(payloadSessionFilter.getFiltertype());
				if (filter != null) {
					List<String> sessions = filter.filter(payloadSessionFilter, payload);
					payloadSessionFilter.setExperiments(sessions);
					results.addAll(sessions);
				}
			}
			for (String result : results) {
				logger.info("session whitelist " + result);
			}
			return results;
		} catch (Exception e) {
			logger.error("failed to find sessions", e);
			return new ArrayList<String>();// empty
		}

	}

	/**
	 * Find sessions by visit and study group.
	 *
	 * @param user
	 *            the user
	 * @param subjects
	 *            the subjects
	 * @return the list
	 */
	// https://cr12-valid02.radiologics.com/data/experiments?xsiType=xnat:subjectAssessorData&columns=ID,project,visit_id,xnat:subjectData/group&xnat:subjectData/group=TIM&visit_id=v1,v2
	/*
	 * private List<String>
	 * findSessionsByVisitAndStudyGroup(org.nrg.xft.security.UserI user, Payload
	 * payload) { List<String> results = new ArrayList<String>(); try {
	 * 
	 * if (payload.getGroups().size() == 0) { throw new Exception(
	 * "Missing groups"); } if (payload.getVisits().size() == 0) { throw new
	 * Exception("Missing visit"); } // String query = "select //
	 * e.label,s.label,e.visit_id,s._group,e.project from // xnat_experimentdata
	 * e "; String query =
	 * "select distinct(e.id) as experiment_id from xnat_experimentdata e ";
	 * query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id "; query
	 * += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id "; query +=
	 * " where e.project = :project "; query += " and s._group in  (:groups) ";
	 * query += " and e.visit_id in (:visits) "; query += " ORDER BY e.id DESC "
	 * ; payload.addLog(query); MapSqlParameterSource parameters = new
	 * MapSqlParameterSource();
	 * 
	 * parameters.addValue("groups", payload.getGroups());
	 * parameters.addValue("visits", payload.getVisits());
	 * parameters.addValue("project", payload.getSproject());
	 * 
	 * NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
	 * new JdbcTemplate(XDAT.getDataSource())); results =
	 * jdbcTemplate.queryForList(query, parameters, String.class);
	 * 
	 * payload.addLog(query);
	 * 
	 * return results; } catch (Exception e) { logger.error(
	 * "failed to find sessions", e); return new ArrayList<String>();// empty }
	 * 
	 * }
	 */

	/**
	 * Gets the filtered subjects.
	 *
	 * @param user
	 *            the user
	 * @param subjects
	 *            the subjects
	 * @return the filtered subjects
	 */
	private List<XnatSubjectdata> getFilteredSubjects(UserI user, List<String> subjects) {
		List<XnatSubjectdata> filteredSubjects = new ArrayList<XnatSubjectdata>();
		for (String subject : subjects) {
			XnatSubjectdata sub = XnatSubjectdata.getXnatSubjectdatasById(subject, user, false);
			if (sub != null) {
				filteredSubjects.add(sub);
			}
		}
		return filteredSubjects;
	}

	/**
	 * Correct i dand label.
	 *
	 * @param newEXPT
	 *            experiment for correction
	 * @return corrected id
	 * @throws Exception
	 *             the exception
	 */
	private String correctIDandLabel(XnatExperimentdataI newEXPT) throws Exception {

		String newid = XnatExperimentdata.CreateNewID();
		newEXPT.setId(newid);

		// correct shared projects
		for (XnatExperimentdataShareI share : newEXPT.getSharing_share()) {
			if (share.getLabel() != null) {
				String label2 = share.getLabel();
				share.setLabel(label2);
			}
		}
		return newid;
	}

	/**
	 * correctIDandLabel.
	 *
	 * @param newRecon
	 *            the new recon
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	private String correctIDandLabel(XnatReconstructedimagedataI newRecon) throws Exception {
		String newid = XnatExperimentdata.CreateNewID();
		newRecon.setId(newid);
		return newid;
	}

	/**
	 * Correct i dand label.
	 *
	 * @param newSubject
	 *            the new subject
	 * @return corrected id
	 * @throws Exception
	 *             the exception
	 */
	private String correctIDandLabel(XnatSubjectdataI newSubject) throws Exception {

		String newid = XnatSubjectdata.CreateNewID();
		// correct ID
		newSubject.setId(newid);

		// correct shared projects
		for (XnatProjectparticipantI share : newSubject.getSharing_share()) {
			if (share.getLabel() != null) {
				String label2 = share.getLabel();
				share.setLabel(label2);
			}
		}
		return newid;
	}

	/**
	 * Modify expt resource.
	 *
	 * @param payload
	 *            the payload
	 * @param resource
	 *            the resource
	 * @param orig
	 *            the orig
	 * @param exp
	 *            the exp
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnknownPrimaryProjectException
	 *             the unknown primary project exception
	 * @throws InvalidArchiveStructure
	 *             the invalid archive structure
	 * @throws ElementNotFoundException
	 *             the element not found exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 * @throws XFTInitException
	 *             the XFT init exception
	 */
	private void modifyExptResource(Payload payload, XnatAbstractresourceI resource, XnatExperimentdata orig,
			XnatExperimentdata exp) throws IOException, UnknownPrimaryProjectException, InvalidArchiveStructure,
					ElementNotFoundException, FieldNotFoundException, XFTInitException {
		String filepath = orig.getArchiveRootPath() + "arc001/";// +
																// orig.getArchiveDirectoryName();
		String newFilepath = exp.getArchiveRootPath() + "arc001/";// +
																	// exp.getArchiveDirectoryName();

		if (resource instanceof XnatResource) {
			String path = ((XnatResource) resource).getUri();
			String newURI = path.replace(filepath, newFilepath);
			((XnatResource) resource).setUri(newURI);
			modifyExptResourceFiles(path, newURI, payload);
			// payload.addManifestEntry(new
			// PayloadManifestEntry(newURI,PayloadManifestEntry.SUCCESS,""));

		} else if (resource instanceof XnatResourceseries) {
			String path = ((XnatResourceseries) resource).getPath();
			String newURI = path.replace(filepath, newFilepath);
			((XnatResourceseries) resource).setPath(newURI);
			modifyExptResourceFiles(path, newURI, payload);
			// payload.addManifestEntry(new
			// PayloadManifestEntry(newURI,PayloadManifestEntry.SUCCESS,""));

		}
	}

	/**
	 * Modify expt resource files.
	 *
	 * @param catalogFile
	 *            the catalog file
	 * @param newCatalogFile
	 *            the new catalog file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnknownPrimaryProjectException
	 *             the unknown primary project exception
	 * @throws InvalidArchiveStructure
	 *             the invalid archive structure
	 * @throws ElementNotFoundException
	 *             the element not found exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 * @throws XFTInitException
	 *             the XFT init exception
	 */
	private void modifyExptResourceFiles(String catalogFile, String newCatalogFile, Payload payload)
			throws IOException, UnknownPrimaryProjectException, InvalidArchiveStructure, ElementNotFoundException,
			FieldNotFoundException, XFTInitException {

		// this is path to catalog
		String newCatalogFileParentDir = newCatalogFile.substring(0, newCatalogFile.lastIndexOf(File.separatorChar));
		new File(newCatalogFileParentDir).mkdirs();
		File sourceCatalog = new File(catalogFile);
		File destCatalog = new File(newCatalogFile);

		File source = sourceCatalog.getParentFile();
		File dest = destCatalog.getParentFile();

		// copy the actual files for resource and catalog.
		try {
			if (source.exists()) {
				payload.addLog("copy:" + source + " to " + dest);
				FileUtils.copyDirectory(source, dest);
			}
		} catch (IOException e) {
			logger.error("", e);
			throw e;
			// don't continue if the file copy failed
		}
	}

	/**
	 * Modify subject resource.
	 *
	 * @param payload
	 *            the payload
	 * @param resource
	 *            the resource
	 * @param subject
	 *            the subject
	 * @param newSubject
	 *            the new subject
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws UnknownPrimaryProjectException
	 *             the unknown primary project exception
	 * @throws InvalidArchiveStructure
	 *             the invalid archive structure
	 * @throws ElementNotFoundException
	 *             the element not found exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 * @throws XFTInitException
	 *             the XFT init exception
	 */
	private void modifySubjectResource(Payload payload, XnatAbstractresourceI resource, XnatSubjectdata subject,
			XnatSubjectdata newSubject) throws IOException, UnknownPrimaryProjectException, InvalidArchiveStructure,
					ElementNotFoundException, FieldNotFoundException, XFTInitException {

		String filepath = subject.getArchiveRootPath() + "subjects/" + subject.getArchiveDirectoryName();
		String newFilepath = newSubject.getArchiveRootPath() + "subjects/" + newSubject.getArchiveDirectoryName();

		if (resource instanceof XnatResource) {
			String path = ((XnatResource) resource).getUri();
			String newURI = path.replace(filepath, newFilepath);
			((XnatResource) resource).setUri(newURI);
			modifySubjResourceFiles(path, newURI, payload);
			// payload.addManifestEntry(new
			// PayloadManifestEntry(newURI,PayloadManifestEntry.SUCCESS,""));

		} else if (resource instanceof XnatResourceseries) {
			String path = ((XnatResourceseries) resource).getPath();
			String newURI = path.replace(filepath, newFilepath);
			((XnatResourceseries) resource).setPath(newURI);
			modifySubjResourceFiles(path, newURI, payload);
			// payload.addManifestEntry(new
			// PayloadManifestEntry(newURI,PayloadManifestEntry.SUCCESS,""));
		}

	}

	/**
	 * Modify subj resource files.
	 *
	 * @param catalogFile
	 *            the catalog file
	 * @param newCatalogFile
	 *            the new catalog file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void modifySubjResourceFiles(String catalogFile, String newCatalogFile, Payload payload)
			throws IOException {

		String newCatalogFileParentDir = newCatalogFile.substring(0, newCatalogFile.lastIndexOf(File.separatorChar));
		new File(newCatalogFileParentDir).mkdirs();
		File sourceCatalog = new File(catalogFile);
		File destCatalog = new File(newCatalogFile);

		File source = sourceCatalog.getParentFile();
		File dest = destCatalog.getParentFile();

		// copy the actual files
		try {
			if (source.exists()) {
				payload.addLog("copy:" + source + " to " + dest);
				FileUtils.copyDirectory(source, dest);
			}
		} catch (IOException e) {
			logger.error("", e);
			throw e;
			// don't continue if the file copy failed
		}
	}

	/**
	 * Copy subject.
	 *
	 * @param user
	 *            the user
	 * @param payload
	 *            the payload
	 * @param targetproject
	 *            the targetproject
	 * @param subject
	 *            the subject
	 * @param ed
	 *            the ed
	 * @return the xnat subjectdata
	 * @throws Exception
	 *             the exception
	 */
	/*
	 * 
	 */

	private XnatSubjectdata copySubject(UserI user, Payload payload, XnatProjectdata targetproject,
			XnatSubjectdata subject, EventDetails ed) throws Exception {

		XnatSubjectdata destSubject = XnatSubjectdata.GetSubjectByProjectIdentifier(targetproject.getId(),
				subject.getLabel(), new XDATUser(user.getUsername()), true);
		if (destSubject != null) {
			payload.addManifestEntry(PayloadType.ERROR, Payload.STAGING_FAILED, new ManifestEntry("Subject",
					ManifestEntry.FAIL, "destination exists", "destination subject exists: " + destSubject.getLabel()));
			logger.error("destination subject exists");
			throw new RuntimeException("destination subject exists");
		}

		XFTItem item = subject.getItem().copy();

		XnatSubjectdata newSubject = (XnatSubjectdata) BaseElement.GetGeneratedItem(item);
		newSubject.setProject(targetproject.getId());

		correctIDandLabel(newSubject);
		if (!newSubject.getId().equals(subject.getId())) {
			// fix subject level stuff
			filterSubjectDemographics(newSubject, payload);
			filterSubjectResources(newSubject, payload);
			filterSessionsById(newSubject, payload.getExperiments(), payload);
			calculateSessionDelay(newSubject, subject);
			calculateAgeAtScan(newSubject, subject);

			for (final XnatAbstractresourceI res : newSubject.getResources_resource()) {
				modifySubjectResource(payload, (XnatAbstractresource) res, subject, newSubject);
			}

			if (newSubject instanceof XnatSubjectdata) {

				// final List<XnatSubjectassessordataI> newexpts =
				// newSubject.getExperiments_experiment();

				for (XnatSubjectassessordataI assess : newSubject.getExperiments_experiment()) {
					// filter experiment stuff
					String origId = assess.getId();
					XnatSubjectassessordataI orig = (XnatSubjectassessordataI) XnatExperimentdata
							.getXnatExperimentdatasById(origId, user, true);
					filterExperimentFields((XnatExperimentdata) orig, (XnatExperimentdata) assess, payload);
					filterExperimentResources((XnatExperimentdata) orig, (XnatExperimentdata) assess, payload);

					if (assess instanceof XnatImagesessiondata) {
						assess.setProject(newSubject.getProject());
						copyExperiment(user, payload, (XnatExperimentdata) orig, (XnatExperimentdata) assess, ed);
					} else {
						assess.setProject(newSubject.getProject());
						copySubjectAssessor(user, payload, subject, newSubject, orig, assess, ed);
					}
				}

			}
		}

		try {
			SaveItemHelper.authorizedSave(newSubject.getItem(), user, false, false, ed);
			payload.addManifestEntry(PayloadType.LOCALSUBJECT, Payload.STAGING_IN_PROGRESS,
					new ManifestEntry(newSubject.getLabel(), ManifestEntry.SUCCESS, "Preparing Subject", ""));

			manifestUpdater.update(user, payload);
		} catch (Exception e) {
			payload.addManifestEntry(PayloadType.ERROR, Payload.STAGING_FAILED,
					new ManifestEntry(newSubject.getLabel(), ManifestEntry.FAIL, "Preparing Subject", e.getMessage()));

			manifestUpdater.update(user, payload);
			logger.error("", e);
			throw e;
		}

		return newSubject;
	}

	/**
	 * Copy subject assessor.
	 *
	 * @param user
	 *            the user
	 * @param payload
	 *            the payload
	 * @param subject
	 *            the subject
	 * @param newSubject
	 *            the new subject
	 * @param xnatSubjectassessordataI
	 *            the xnat subjectassessordata i
	 * @param assess
	 *            the assess
	 * @param ed
	 *            the ed
	 * @throws Exception
	 *             the exception
	 */
	private void copySubjectAssessor(UserI user, Payload payload, XnatSubjectdata subject, XnatSubjectdata newSubject,
			XnatSubjectassessordataI xnatSubjectassessordataI, XnatSubjectassessordataI assess, EventDetails ed)
					throws Exception {
		correctIDandLabel(assess);

		for (final XnatAbstractresourceI res : assess.getResources_resource()) {
			modifySubjectResource(payload, (XnatAbstractresource) res, subject, newSubject);
		}
		payload.addManifestEntry(PayloadType.LOCALEXPERIMENT, Payload.STAGING_IN_PROGRESS,
				new ManifestEntry(assess.getLabel(), ManifestEntry.SUCCESS, "Preparing Experiment", ""));
		manifestUpdater.update(user, payload);
	}

	/**
	 * Calculate age at scan.
	 *
	 * @param newSubject
	 *            the new subject
	 * @param subject
	 *            the subject
	 */
	private void calculateAgeAtScan(XnatSubjectdata newSubject, XnatSubjectdata subject) {
		// TODO Auto-generated method stub
		this.simpleExportAgeAtScanCalculationsService.calc(subject, newSubject);
	}

	/**
	 * Calculate session delay.
	 *
	 * @param subject
	 *            the subject
	 * @param newSubject
	 *            the new subject
	 */
	private void calculateSessionDelay(XnatSubjectdata subject, XnatSubjectdata newSubject) {
		this.simpleExportDateCalculationsService.calc(subject, newSubject);
	}

	
	


	/**
	 * Filter subject demographics. Fields that exists in the payload docu
	 *
	 * @param sub
	 *            the sub
	 * @param payload
	 *            the payload
	 * @throws IndexOutOfBoundsException
	 *             the index out of bounds exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 */
	private void filterSubjectDemographics(XnatSubjectdata sub, Payload payload)
			throws IndexOutOfBoundsException, FieldNotFoundException {

		XnatDemographicdata subDemographics = (XnatDemographicdata) sub.getDemographics();
		if (subDemographics != null) {
			List fields = subDemographics.getItem().getAllXMLFieldNames();

			for (Object object : fields) {
				String fieldName = (String) object;
				if (payload.getDemographics().get(fieldName) != null && !payload.getDemographics().get(fieldName)) {
					try {
						logger.info(sub.getLabel()+":"+"fields:XnatDemographicdata: strip " + fieldName);
						subDemographics.getItem().setProperty(fieldName, null);
					} catch (ElementNotFoundException | XFTInitException | InvalidValueException e) {
						logger.info(sub.getLabel()+":"+"fields:XnatDemographicdata: not found " + fieldName);
					}
				} else {
					logger.info(sub.getLabel()+":"+"fields:XnatDemographicdata: keep " + fieldName);

				}
			}
		}
	}

	/**
	 * Filter scantypes.
	 *
	 * @param orig
	 *            the orig
	 * @param exp
	 *            the exp
	 * @param payload
	 *            the payload
	 * @throws IndexOutOfBoundsException
	 *             the index out of bounds exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 */
	private void filterScantypes(XnatExperimentdata orig, XnatExperimentdata exp, Payload payload)
			throws IndexOutOfBoundsException, FieldNotFoundException {
		while (findAndRemoveScantypes(orig, exp, payload))
			;
		return;
	}

	/**
	 * Filter subject resources.
	 *
	 * @param sub
	 *            the sub
	 * @param resourceType
	 *            the resource type
	 * @throws IndexOutOfBoundsException
	 *             the index out of bounds exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 */
	private void filterSubjectResources(XnatSubjectdata sub, Payload payload)
			throws IndexOutOfBoundsException, FieldNotFoundException {
		while (findAndRemoveSubjectResources(sub, payload))
			;
		return;
	}

	/**
	 * Find and remove subject resources.
	 *
	 * @param sub
	 *            the sub
	 * @param resourceType
	 *            the resource type
	 * @return true, if successful
	 */
	private boolean findAndRemoveSubjectResources(XnatSubjectdata sub, Payload payload) {
		boolean found = false;
		List<String> resourceType = payload.getResources();
		List<XnatAbstractresourceI> resource = sub.getResources_resource();
		for (int i = 0; i < resource.size(); i++) {
			if (!resourceType.contains(resource.get(i).getLabel())) {
				payload.addLog(sub.getLabel()+":"+"findAndRemoveSubjectResources " + i + " " + resource.get(i).getLabel());
				sub.removeResources_resource(i);
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	 * Filter experiment resources.
	 *
	 * @param orig
	 *            the orig
	 * @param exp
	 *            the exp
	 * @param payload
	 *            the payload
	 * @throws IndexOutOfBoundsException
	 *             the index out of bounds exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 */
	private void filterExperimentResources(XnatExperimentdata orig, XnatExperimentdata exp, Payload payload)
			throws IndexOutOfBoundsException, FieldNotFoundException {
		SessionFilter sessionFilter = this.getSessionFilter(payload, orig.getId(), orig.getXSIType());
		while (findAndRemoveExperimentResources(exp, sessionFilter.getResources(), payload))
			;

		return;
	}

	/**
	 * Find and remove experiment resources.
	 *
	 * @param exp
	 *            the exp
	 * @param resourceType
	 *            the resource type
	 * @return true, if successful
	 */
	private boolean findAndRemoveExperimentResources(XnatExperimentdata exp, List<String> resourceType,
			Payload payload) {
		boolean found = false;
		List<XnatAbstractresourceI> resource = exp.getResources_resource();
		for (int i = 0; i < resource.size(); i++) {
			if (resourceType != null && !resourceType.contains(resource.get(i).getLabel())) {
				payload.addLog(exp.getLabel()+":"+"findAndRemoveExperimentResources strip " + i + " " + resource.get(i).getLabel());
				exp.removeResources_resource(i);
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	 * Filter sessions by id.
	 *
	 * @param sub
	 *            the sub
	 * @param sessions
	 *            the sessions
	 * @throws IndexOutOfBoundsException
	 *             the index out of bounds exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 */
	private void filterSessionsById(XnatSubjectdata sub, List<String> sessions, Payload payload)
			throws IndexOutOfBoundsException, FieldNotFoundException {
		while (findAndRemoveSessionsById(sub, sessions, payload))
			;
		return;
	}

	/**
	 * Find and remove scantypes.
	 *
	 * @param exp
	 *            the exp
	 * @param payload
	 *            the payload
	 * @return true, if successful
	 */
	private boolean findAndRemoveScantypes(XnatExperimentdata orig, XnatExperimentdata exp, Payload payload) {
		boolean found = false;
		List<XnatImagescandataI> scans = ((XnatImagesessiondata) exp).getScans_scan();
		SessionFilter sessionFilter = this.getSessionFilter(payload, orig.getId(), orig.getXSIType());

		for (int i = 0; i < scans.size(); i++) {
			if (sessionFilter.getScantypes() != null && sessionFilter.getScantypes().size() > 0) {// no
																									// scantype
																									// filters
																									// implies
																									// all
																									// coppied.
				if (!sessionFilter.getScantypes().contains(scans.get(i).getType())) {
					payload.addLog(exp.getLabel()+":"+"findAndRemoveScantypes: remove scantype " + i + " " + scans.get(i).getType() + " "
							+ scans.get(i).getXSIType());

					
					((XnatImagesessiondata) exp).removeScans_scan(i);
					found = true;
					return true;
				} else {
					
					payload.addLog(exp.getLabel()+":"+"findAndRemoveScantypes: allow scantype " + i + " " + scans.get(i).getType() + " "
							+ scans.get(i).getXSIType());
				}
			} else {
				
				payload.addLog(exp.getLabel()+":"+"findAndRemoveScantypes: no scantype filter " + i + " " + scans.get(i).getType() + " "
						+ scans.get(i).getXSIType());

			}
		}
		return found;
	}

	private SessionFilter getSessionFilter(Payload payload, String id, String xsiType) {
		SessionFilter filter = null;
		for (SessionFilter sessionFilter : payload.getSessionfilters()) {
			if (xsiType.equals(sessionFilter.getDatatype()) && sessionFilter.getExperiments().contains(id)) {
				filter = sessionFilter;
				break;
			}
		}
		if (filter == null) {
			logger.error("No filter found for experiment " + id + " of type " + xsiType);
			throw new RuntimeException("No filter found for experiment " + id + " of type " + xsiType);
		}
		return filter;
	}

	/**
	 * Find and remove sessions by id.
	 *
	 * @param sub
	 *            the sub
	 * @param sessions
	 *            the sessions
	 * @return true, if successful
	 */
	private boolean findAndRemoveSessionsById(XnatSubjectdata sub, List<String> sessions, Payload payload) {
		boolean found = false;
		List<XnatSubjectassessordataI> experiments = sub.getExperiments_experiment();
		for (int i = 0; i < experiments.size(); i++) {
			//payload.addLog("check sessions " + i + " " + experiments.get(i).getId());

			if (!sessions.contains(experiments.get(i).getId())) {
				payload.addLog(sub.getLabel()+":"+"findAndRemoveSessions removing " + i + " " + experiments.get(i).getLabel());
				sub.removeExperiments_experiment(i);
				found = true;
				break;
			}else{
				payload.addLog(sub.getLabel()+":"+"findAndRemoveSessions keeping " + i + " " + experiments.get(i).getLabel());

			}
		}
		return found;
	}

	/**
	 * Filter recons.
	 *
	 * @param exp
	 *            the exp
	 * @param scan_types
	 *            the scan_types
	 * @throws IndexOutOfBoundsException
	 *             the index out of bounds exception
	 * @throws FieldNotFoundException
	 *             the field not found exception
	 */
	private void filterRecons(XnatExperimentdata exp, List<String> scan_types, Payload payload)
			throws IndexOutOfBoundsException, FieldNotFoundException {
		while (findAndRemoveRecons(exp, scan_types, payload));
		return;
	}

	/**
	 * Find and remove recons.
	 *
	 * @param exp
	 *            the exp
	 * @param scan_types
	 *            the scan_types
	 * @return true, if successful
	 */
	private boolean findAndRemoveRecons(XnatExperimentdata exp, List<String> scan_types, Payload payload) {
		boolean found = false;
		List<XnatReconstructedimagedataI> recons = ((XnatImagesessiondata) exp).getReconstructions_reconstructedimage();
		for (int i = 0; i < recons.size(); i++) {
			if (scan_types != null && !scan_types.contains(recons.get(i).getType())) {
				payload.addLog(exp.getLabel()+":"+"findAndRemoveRecons " + i + " " + recons.get(i).getType());
				((XnatImagesessiondata) exp).removeReconstructions_reconstructedimage(i);
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	 * Copy experiment.
	 *
	 * @param user
	 *            the user
	 * @param payload
	 *            the payload
	 * @param orig
	 *            the orig
	 * @param exp
	 *            the exp
	 * @param ed
	 *            the ed
	 * @return the xnat experimentdata
	 * @throws Exception
	 *             the exception
	 */
	private XnatExperimentdata copyExperiment(UserI user, Payload payload, XnatExperimentdata orig,
			XnatExperimentdata exp, EventDetails ed) throws Exception {

		try {
			correctIDandLabel(exp);

			if (!orig.getId().equals(exp.getId())) {

				for (final XnatAbstractresourceI res : exp.getResources_resource()) {
					modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
				}

				if (exp instanceof XnatImagesessiondata) {
					resetPrearchive((XnatImagesessiondata) exp);
					filterScantypes(orig, exp, payload);
					fixScanTypes(orig, exp, payload);

					for (final XnatImagescandataI scan : ((XnatImagesessiondata) exp).getScans_scan()) {
						scan.setImageSessionId(exp.getId());
						for (final XnatAbstractresourceI res : scan.getFile()) {
							modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
						}
					}

					filterRecons(exp, payload.getRecons(), payload);
					for (final XnatReconstructedimagedataI recon : ((XnatImagesessiondata) exp)
							.getReconstructions_reconstructedimage()) {
						recon.setImageSessionId("NULL");
						correctIDandLabel(recon);
						for (final XnatAbstractresourceI res : recon.getIn_file()) {
							modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
						}
						for (final XnatAbstractresourceI res : recon.getOut_file()) {
							modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
						}
					}

					for (final XnatImageassessordataI assess : ((XnatImagesessiondata) exp).getAssessors_assessor()) {
						assess.setImagesessionId("NULL");
						correctIDandLabel(assess);

						for (final XnatAbstractresourceI res : assess.getResources_resource()) {
							modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
						}

						for (final XnatAbstractresourceI res : assess.getIn_file()) {
							modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
						}

						for (final XnatAbstractresourceI res : assess.getOut_file()) {
							modifyExptResource(payload, (XnatAbstractresource) res, orig, exp);
						}
					}

					simpleExportAnonymizerService.anonymize((XnatImagesessiondata) exp, exp.getProject());
					payload.addManifestEntry(PayloadType.LOCALEXPERIMENT, Payload.STAGING_IN_PROGRESS,new ManifestEntry(exp.getLabel(), ManifestEntry.SUCCESS, "Preparing Experiment", ""));
					manifestUpdater.update(user, payload);
				} else {
					payload.addManifestEntry(PayloadType.LOCALEXPERIMENT, Payload.STAGING_IN_PROGRESS,new ManifestEntry(exp.getLabel(), ManifestEntry.SUCCESS, "Preparing Experiment", ""));
					manifestUpdater.update(user, payload);
				}
			}
		} catch (Exception ex) {
			payload.addManifestEntry(PayloadType.ERROR, Payload.STAGING_FAILED, new ManifestEntry(exp.getLabel(),
					ManifestEntry.FAIL, "Failed Preparing Experiment", ex.getMessage()));
			manifestUpdater.update(user, payload);
			throw new Exception(ex);
		}
		return exp;
	}

	/**
	 * Filter experiment fields.
	 *
	 * @param orig
	 *            the orig
	 * @param exp
	 *            the exp
	 * @param payload
	 *            the payload
	 */
	private void filterExperimentFields(XnatExperimentdata orig, XnatExperimentdata exp, Payload payload) {

		SessionFilter sessionFilter = this.getSessionFilter(payload, orig.getId(), orig.getXSIType());

		// List<SessionFilter> filters = payload.getSessionfilters();
		// for (SessionFilter sessionFilter : filters) {
		// if (exp.getXSIType().equals(sessionFilter.getDatatype())&&
		// sessionFilter.getExperiments().contains(orig.getId())) {
		// prune fields.
		Map<String, Boolean> filterFields = sessionFilter.getFields();
		if (filterFields != null) {
			for (String keys : filterFields.keySet()) {
				if (!filterFields.get(keys)) {
					try {
						logger.info(exp.getLabel()+":"+"filterExperimentFields:" + exp.getXSIType() + ": strip " + keys);
						exp.getItem().setProperty(keys, null);
						exp.getItem().removeEmptyItems();
						

					} catch (ElementNotFoundException | XFTInitException | InvalidValueException
							| FieldNotFoundException e) {
						logger.info(exp.getLabel()+":"+
								"filterExperimentFields:" + exp.getXSIType() + " experiment key not found " + keys);
					
					}
				} else {
					logger.info(exp.getLabel()+":"+"filterExperimentFields:" + exp.getXSIType() + " keep " + keys);
				}
			}
		} else {
			payload.addLog(exp.getLabel()+":"+"filterExperimentFields:" + exp.getXSIType() + "No field filtering found");
		}

		if (exp instanceof XnatImagesessiondata) {
			List<XnatImagescandata> scans = ((XnatImagesessiondata) exp).getScans_scan();

			for (XnatImagescandata scan : scans) {
				if (filterFields != null) {
					for (String scankeys : filterFields.keySet()) {
						if (!filterFields.get(scankeys)) {
							try {
								logger.info(exp.getLabel()+":"+"filterExperimentFields:" + scan.getXSIType() + ": strip " + scankeys);
								scan.getItem().setProperty(scankeys, null);
								scan.getItem().removeEmptyItems();

							} catch (ElementNotFoundException | XFTInitException | InvalidValueException
									| FieldNotFoundException e) {
								logger.info(exp.getLabel()+":"+
										"filterExperimentFields:" + scan.getXSIType() + " key not found " + scankeys);

							}
						} else {
							logger.info(exp.getLabel()+":"+"filterExperimentFields:" + exp.getXSIType() + " keep " + scankeys);

						}
					}
				} else {
					logger.info(exp.getLabel()+":"+"filterExperimentFields:" + exp.getXSIType() + "No field filtering found");

				}

			}
		}

		// }
		// }
	}

	/**
	 * Reset prearchive.
	 *
	 * @param exp
	 *            the exp
	 */
	private void resetPrearchive(XnatImagesessiondata exp) {
		exp.setPrearchivepath(null);

	}

}
