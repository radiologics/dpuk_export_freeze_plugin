/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;

public class AliasToken implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9168832787803691276L;
	String alias;
	String secret;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public AliasToken() {
		super();
	}

}
