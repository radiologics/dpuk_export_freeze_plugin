/*
 * org.nrg.xnat.services.transfer.simple.RemoteTransferRequest
 * XNAT http://www.xnat.org
 * Copyright (c) 2015, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 
 */
package org.nrg.xnat.services.transfer.remote;

import java.io.Serializable;

import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.Transfer;

public class RemoteTransferRequest implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5873183784235078352L;
	public RemoteTransferRequest(final UserI user, Transfer transfer) {
        _user = user;
        _transfer=transfer;
    }
    public UserI getUser() {
        return _user;
    }
    public Transfer getTransfer() {
		return  _transfer;
	}
    
    private final UserI _user;
    private final Transfer _transfer;
	
}
