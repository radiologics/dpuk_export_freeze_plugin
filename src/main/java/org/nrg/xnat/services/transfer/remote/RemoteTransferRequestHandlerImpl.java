/*
 * org.nrg.xnat.services.transfer.simple.SimpleTransferRequestHandlerImpl
 * 
 */
package org.nrg.xnat.services.transfer.remote;

import java.util.List;

import javax.mail.MessagingException;

import org.nrg.xdat.XDAT;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xnat.services.transfer.RemoteTransferService;
import org.nrg.xnat.services.transfer.domain.ManifestEntry;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RemoteTransferRequestHandlerImpl implements RemoteTransferRequestHandler {

	@Autowired
	RemoteTransferService remoteTransferService;

	@SuppressWarnings("unused")
	public void handle(final RemoteTransferRequest request) {
		boolean success = true;
		try {
			logger.info("handle RemoteTransferRequest");
			remoteTransferService.transfer(request.getUser(), request.getTransfer());
			emailSuccess(request);

		} catch (Exception e) {
			final String message = String.format("An error occurred processing a request from user %s to perform some operation ",request.getUser().getUsername());
			logger.error("", e);
			emailFailedUser(request);
			emailFailedAdmin(request);
		}

	}
	/**
	/**
	 * Format failure email to requesting user.
	 *
	 * @param request the request
	 */
	void emailSuccess(final RemoteTransferRequest request) {
		String subject="Data Freeze sharing from "+XDAT.getSiteConfigPreferences().getSiteId()+" to DPUK Hub succeeded";
		StringBuilder sb = new StringBuilder();
		
		try {
			
			Payload payload=request.getTransfer().getPayload();

			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>Data Freeze sharing to DPUK Hub was successfull from "+payload.getDproject()+" on "+TurbineUtils.GetFullServerPath()+" to "+payload.getConnection().getUrl()+"/data/projects/"+payload.getDproject()+" requested by "+request.getUser().getUsername()+".</p>");
			sb.append("<table>");
			sb.append("<th> Source Project </th>");
			sb.append("<th> Target Project </th>");
			sb.append("<th> Label </th>");
			sb.append("<th> Message </th>");
			sb.append("<th> Status </th>");
			List<ManifestEntry> entries = request.getTransfer().getPayload().getManifest().getEntries();
			
			for (ManifestEntry payloadManifestEntry : entries) {
				 sb.append("<tr>");
				 sb.append("<td> " + payload.getSproject() + " </td>");
				 sb.append("<td> " + payload.getDproject() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getEntry() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getMessage() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getStatus() + " </td>");
				 sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			XDAT.getMailService().sendHtmlMessage(XDAT.getSiteConfigPreferences().getAdminEmail(), request.getUser().getEmail(), subject,
					sb.toString());
		} catch (MessagingException me) {
			logger.error("Failed to send email.", me);
		} catch (Exception e) {
			logger.error("Failed to send email.", e);
		}

	}
	
	/**
	 * Email failed user.
	 *
	 * @param request the request
	 */
	void emailFailedUser(final RemoteTransferRequest request) {


		try {
			String subject="Data Freeze sharing from "+XDAT.getSiteConfigPreferences().getSiteId()+" to DPUK Hub failed";
			StringBuilder sb = new StringBuilder();
		
			Payload payload=request.getTransfer().getPayload();

			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>Data Freeze sharing to DPUK Hub failed for project "+payload.getSproject()+" on "+TurbineUtils.GetFullServerPath()+" to "+payload.getConnection().getUrl()+"/data/projects/"+payload.getDproject()+" requested by "+request.getUser().getUsername()+". Please contact your administrator.</p>");
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			XDAT.getMailService().sendHtmlMessage(XDAT.getSiteConfigPreferences().getAdminEmail(), request.getUser().getEmail(), subject,sb.toString());
		} catch (MessagingException me) {
			logger.error("Failed to send email.", me);
		} catch (Exception e) {
			logger.error("Failed to send email.", e);
		}
	}
	
	/**
	/**
	 * Format failure email to admin user.
	 *
	 * @param request the request
	 */
	void emailFailedAdmin(final RemoteTransferRequest request) {

		String subject="Data Freeze sharing from "+XDAT.getSiteConfigPreferences().getSiteId()+" to DPUK Hub failed";
		StringBuilder sb = new StringBuilder();
		

		try {
			
			Payload payload=request.getTransfer().getPayload();
			
			sb.append("<html>");
	        sb.append("<body>");
			sb.append("<p>Data Freeze sharing to DPUK Hub failed for project "+payload.getSproject()+" on "+TurbineUtils.GetFullServerPath()+" to "+payload.getConnection().getUrl()+"/data/projects/"+payload.getDproject()+" requested by "+request.getUser().getUsername()+". Please review the following transcript of results.</p>");
			sb.append("For details see: "+TurbineUtils.GetFullServerPath()+"/data/projects/"+payload.getSproject()+"/resources/datafreeze/files/"+payload.getJsonFilename()+"</p>");
			sb.append("<table>");
			sb.append("<th> Source Project </th>");
			sb.append("<th> Target Project </th>");
			sb.append("<th> Label </th>");
			sb.append("<th> Message </th>");
			sb.append("<th> Status </th>");
			List<ManifestEntry> entries = request.getTransfer().getPayload().getManifest().getEntries();
			
			for (ManifestEntry payloadManifestEntry : entries) {
				 sb.append("<tr>");
				 sb.append("<td> " + payload.getSproject() + " </td>");
				 sb.append("<td> " + payload.getDproject() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getEntry() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getMessage() + " </td>");
				 sb.append("<td> " + payloadManifestEntry.getStatus() + " </td>");
				 sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append("</body>");
            sb.append("</html>");
			logger.debug(sb.toString());
			XDAT.getMailService().sendHtmlMessage(XDAT.getSiteConfigPreferences().getAdminEmail(),XDAT.getSiteConfigPreferences().getAdminEmail(), subject,
					sb.toString());
		} catch (MessagingException me) {
			logger.error("Failed to send email.", me);
		} catch (Exception e) {
			logger.error("Failed to send email.", e);
		} 
	}
	
	
	private final static Logger logger = LoggerFactory.getLogger(RemoteTransferRequestHandlerImpl.class);
}
