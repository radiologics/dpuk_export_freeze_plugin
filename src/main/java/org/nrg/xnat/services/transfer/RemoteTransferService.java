/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer;

import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.Transfer;

public interface RemoteTransferService {
	
public void transfer(UserI userI, Transfer transfer) throws UnknownPrimaryProjectException, Exception;
}
