package org.nrg.xnat.services.transfer.local;

import java.util.Comparator;
import java.util.Date;

import org.nrg.xdat.model.XnatSubjectassessordataI;

public class ExperimentDateComparator implements Comparator<XnatSubjectassessordataI>{

    public ExperimentDateComparator(){
    }
    
    @Override
    public int compare( final XnatSubjectassessordataI o, final XnatSubjectassessordataI a) {
    	Date edate = (Date) a.getDate();
    	Date odate=(Date) o.getDate();
        return odate.compareTo(edate);
    }
}