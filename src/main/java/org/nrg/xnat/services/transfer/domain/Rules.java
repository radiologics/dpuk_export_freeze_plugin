/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;
import java.util.List;

/*
 * List of rules to apply to data. Might be groovy scripts or something else. Not implemented yet.
 * 
 */
public class Rules implements Serializable {

	List<String> rules;
	
	public Rules(List<String> rules) {
		this.rules=rules;
	}
	
	
	public List<String> getRules() {
		return rules;
	}
	public void setRules(List<String> rules) {
		this.rules = rules;
	}
	

}
