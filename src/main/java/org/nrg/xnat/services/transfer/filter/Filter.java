/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import java.util.List;

import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.SessionFilter;

public interface Filter {
	public List<String> filter(SessionFilter payloadSessionFilter, Payload payload);
		
}
