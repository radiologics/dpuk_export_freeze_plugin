/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class FilterLoaderService implements FilterLoader{

	@Autowired
	ApplicationContext baseContext;
	
	@Override
	public Filter load(String filter)  {
		
		
		String filtername= filter+"Filter";
		try {
			filtername=StringUtils.deleteWhitespace(filtername);
			return (Filter) Class.forName("org.nrg.xnat.services.transfer.filter."+filtername).newInstance();
		} catch (BeansException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	
}
