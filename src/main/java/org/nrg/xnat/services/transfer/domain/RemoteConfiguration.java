package org.nrg.xnat.services.transfer.domain;

public class RemoteConfiguration {

	String anonscript;
	String location;
	public String getAnonscript() {
		return anonscript;
	}
	public void setAnonscript(String anonscript) {
		this.anonscript = anonscript;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
