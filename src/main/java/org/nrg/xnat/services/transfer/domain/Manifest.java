/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Manifest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4019600688857500457L;
	
	
	List<ManifestEntry> entries;

	public Manifest(){
		entries=new ArrayList<ManifestEntry>();
	}
	
	public List<ManifestEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<ManifestEntry> entries) {
		this.entries = entries;
	}
	public void addEntry(ManifestEntry entry) {
		this.entries.add(entry);
	}
	
	
}
