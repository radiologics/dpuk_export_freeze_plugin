/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;

public class ManifestEntry implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5434351283509106207L;
	
	public static final String FAIL ="FAIL";
	public static final String SUCCESS ="SUCCESS";
	

	String entry;
	String status;
	String message;
	String detail;
	
	/** The manifest. */
	
	
	
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public ManifestEntry() {
		super();
	}
	public ManifestEntry(String entry, String status, String message,String detail) {
		super();
		this.entry = entry;
		this.status = status;
		this.message = message;
		this.detail=detail;
	}
	public String getEntry() {
		return entry;
	}
	public void setEntry(String entry) {
		this.entry = entry;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
