package org.nrg.xnat.services.transfer.remote;

public interface RemoteTransferRequestHandler {
    public void handle(final RemoteTransferRequest request) ;
}
