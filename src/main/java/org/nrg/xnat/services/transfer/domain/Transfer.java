/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true) 
public class Transfer implements Serializable{

	Payload payload;
	Rules rules;
	public Transfer(Payload payload, Rules rules) {
		super();
		this.payload = payload;
		this.rules = rules;
	}
	public Payload getPayload() {
		// TODO Auto-generated method stub
		return this.payload;
	}
	public Rules getRules() {
		return rules;
	}
	public void setRules(Rules rules) {
		this.rules = rules;
	}
	public void setPayload(Payload payload) {
		this.payload = payload;
	}
}
