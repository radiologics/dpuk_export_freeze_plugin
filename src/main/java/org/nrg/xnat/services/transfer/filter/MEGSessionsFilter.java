/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.SessionFilter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


public class MEGSessionsFilter extends AbstractFilter  {

	
	public static Logger logger = Logger.getLogger(MEGSessionsFilter.class);

	

	/** The filtertype. */
	@Override
	//xnat_imagesessiondata  where session_type='session_type';
	public List<String> filter(SessionFilter filter,Payload payload) {
		List<String> results = new ArrayList<String>();
		try {

			if (payload.getGroups()!=null && payload.getGroups().size() == 0) {
				throw new Exception("Missing groups");
			}
			if (payload.getVisits()!=null && payload.getVisits().size() == 0) {
				throw new Exception("Missing visit");
			}
			
			List<String> scantypes=new ArrayList<String>();

			//get the scantype filters.
			if(filter!=null){
					scantypes.addAll(filter.getScantypes());
				
			}
			
			MapSqlParameterSource parameters = new MapSqlParameterSource();

			String query = "select distinct(meg.id) as experiment_id from xnat_megsessiondata  meg  ";
			query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=meg.id  ";
			query += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id  "; 
			query += " LEFT JOIN xnat_experimentdata e ON e.id=meg.id  ";
			query += " LEFT JOIN xnat_imagescandata scan ON e.id=scan.image_session_id ";

			query += " where e.project = :project ";
			if(scantypes!=null && scantypes.size()>0){
				query += " and scan.type in  (:scantype) ";
				parameters.addValue("scantype",scantypes);
			}
			
			if(payload.getGroups().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)){
				query += " and (s._group in (:groups) or s._group is null)";
				
			}else{
				query += " and s._group in  (:groups) ";
			}
			if(payload.getVisits().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)){
				query += " and (e.visit_id in  (:visits) or e.visit_id is null)";
				
			}else{
				query += " and e.visit_id in  (:visits) ";
			}
			
			query += " ORDER BY meg.id DESC ";
			logger.debug(query);
			parameters.addValue("groups", payload.getGroups());
			parameters.addValue("visits", payload.getVisits());
			parameters.addValue("project", payload.getSproject());

			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			results = jdbcTemplate.queryForList(query, parameters, String.class);


			for (String result : results) {
				logger.debug("MEGSessionFilter whitelist "+result);
			}
			return results;
		} catch (Exception e) {
			logger.error("failed to find sessions", e);
			return new ArrayList<String>();// empty
		}

	}
	
}
