/**
 *Copyright (c) 2015 (c) Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// TODO: Auto-generated Javadoc
/**
 * The Class Payload.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Payload implements Serializable {

	public enum PayloadType {
		LOCALSUBJECT, LOCALEXPERIMENT, REMOTESUBJECT, REMOTEEXPERIMENT, LOCALPROJECT, REMOTEPROJECT, ERROR, LOG
	}

	//public final static String NULL_VALUE="[all]";

	/** datatype:. */
	private static final long serialVersionUID = 5159493885404538079L;

	/** The delete on failure. */
	Boolean deleteOnFailure;

	/** The sessions. */
	List<String> experiments;

	/** The subjects. */
	List<String> subjects;

	/** The dproject. */
	String dproject;

	/** The sproject. */
	String sproject;

	String rproject;
	

	/** The status. */
	String status;

	/** The createdon. */
	String createdon;

	/** The createdby. */
	String createdby;

	/** The node. */
	String node;

	/** The visits. */
	List<String> visits;// visit1,visit2 etc

	/** The groups. */
	List<String> groups;// group1,group2 etc

	/** The demographics. */
	Hashtable<String, Boolean> demographics;

	/** The manifest. */
	Manifest manifest;
	List<String> log;
	
	
	
	String cohort;
	
	public String getCohort() {
		return cohort;
	}

	public void setCohort(String cohort) {
		this.cohort = cohort;
	}

	public List<String> getLog() {
		return log;
	}

	public void setLog(List<String> log) {
		this.log = log;
	}




	/** The connection. */
	RemoteConnection connection;

	/** The sessionfilters. */
	List<SessionFilter> sessionfilters;

	/** The totalsubjects. */
	Integer totalsubjects;
	/** The totalexperiments. */
	Integer totalexperiments;

	Integer currentlocalsubjects;
	Integer currentlocalexperiments;
	Integer currentremotesubjects;
	Integer currentremoteexperiments;


	
	
	public String getRproject() {
		return rproject;
	}

	public void setRproject(String rproject) {
		this.rproject = rproject;
	}
	public Integer getCurrentlocalsubjects() {
		return currentlocalsubjects;
	}

	public void setCurrentlocalsubjects(Integer currentlocalsubjects) {
		this.currentlocalsubjects = currentlocalsubjects;
	}

	public Integer getCurrentlocalexperiments() {
		return currentlocalexperiments;
	}

	public void setCurrentlocalexperiments(Integer currentlocalexperiments) {
		this.currentlocalexperiments = currentlocalexperiments;
	}

	public Integer getCurrentremotesubjects() {
		return currentremotesubjects;
	}

	public void setCurrentremotesubjects(Integer currentremotesubjects) {
		this.currentremotesubjects = currentremotesubjects;
	}

	public Integer getCurrentremoteexperiments() {
		return currentremoteexperiments;
	}

	public void setCurrentremoteexperiments(Integer currentremoteexperiments) {
		this.currentremoteexperiments = currentremoteexperiments;
	}

	
	
	
	/** The Constant STAGING_IN_PROGRESS. */
	public static final String STAGING_IN_PROGRESS = "Preparing Data";

	/** The Constant STAGING_FAILED. */
	public static final String STAGING_FAILED = "Failed Preparing Data";

	/** The Constant STAGING_COMPLETE. */
	public static final String STAGING_COMPLETE = "Ready To Share";

	/** The Constant SHARING_IN_PROGRESS. */
	public static final String SHARING_IN_PROGRESS = "Sharing Data";

	/** The Constant SHARING_FAILED. */
	public static final String SHARING_FAILED = "Failed Sharing Data";

	/** The Constant SHARING_COMPLETE. */
	public static final String SHARING_COMPLETE = "Shared";

	/**
	 * Sets the totalexperiments.
	 *
	 * @param totalexperiments
	 *            the new totalexperiments
	 */
	public void setTotalexperiments(Integer totalexperiments) {
		this.totalexperiments = totalexperiments;
	}

	/**
	 * Gets the delete on failure.
	 *
	 * @return the delete on failure
	 */
	public Boolean getDeleteOnFailure() {
		return deleteOnFailure;
	}

	/**
	 * Sets the delete on failure.
	 *
	 * @param deleteOnFailure
	 *            the new delete on failure
	 */
	public void setDeleteOnFailure(Boolean deleteOnFailure) {
		this.deleteOnFailure = deleteOnFailure;
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public RemoteConnection getConnection() {
		return connection;
	}

	/**
	 * Sets the connection.
	 *
	 * @param connection
	 *            the new connection
	 */
	public void setConnection(RemoteConnection connection) {
		this.connection = connection;
	}

	/**
	 * Gets the manifest.
	 *
	 * @return the manifest
	 */
	public Manifest getManifest() {
		return manifest;
	}

	/**
	 * Sets the manifest.
	 *
	 * @param manifest
	 *            the new manifest
	 */
	public void setManifest(Manifest manifest) {
		this.manifest = manifest;
	}

	/**
	 * Adds the manifest entry.
	 *
	 * @param status
	 *            the status
	 * @param entry
	 *            the entry
	 */
	public void addManifestEntry(PayloadType type, String status, ManifestEntry entry) {
		this.status = status;
		this.manifest.addEntry(entry);
		this.addTotals(type);
	}
	public void addLog(String logentry) {
		log.add(logentry);
		//this.manifest.addEntryLog(log);
	}
	
	private void addTotals(PayloadType type) {

		switch (type) {
		case LOCALSUBJECT:
			currentlocalsubjects = currentlocalsubjects + 1;
			break;
		case LOCALEXPERIMENT:
			currentlocalexperiments = currentlocalexperiments + 1;
			break;
		case REMOTESUBJECT:
			currentremotesubjects = currentremotesubjects + 1;
			break;
		case REMOTEEXPERIMENT:
			currentremoteexperiments = currentremoteexperiments + 1;
			break;
		default:
			break;
		}
	}

	/** The recons. */
	// not used yet, all filtered for now
	List<String> recons;

	/** The resources. */
	List<String> resources; // DICOM for now.

	/**
	 * Gets the resources.
	 *
	 * @return the resources
	 */
	public List<String> getResources() {
		return resources;
	}

	/**
	 * Sets the resources.
	 *
	 * @param resources
	 *            the new resources
	 */
	public void setResources(List<String> resources) {
		this.resources = resources;
	}

	/**
	 * Gets the demographics.
	 *
	 * @return the demographics
	 */
	public Hashtable<String, Boolean> getDemographics() {
		return demographics;
	}

	/**
	 * Sets the demographics.
	 *
	 * @param demographics
	 *            the demographics
	 */
	public void setDemographics(Hashtable<String, Boolean> demographics) {
		this.demographics = demographics;
	}

	/**
	 * Gets the recons.
	 *
	 * @return the recons
	 */
	public List<String> getRecons() {
		return recons;
	}

	/**
	 * Sets the recons.
	 *
	 * @param recons
	 *            the new recons
	 */
	public void setRecons(List<String> recons) {
		this.recons = recons;
	}

	/**
	 * Instantiates a new payload.
	 */
	public Payload() {
		this.manifest = new Manifest();
		totalsubjects=new Integer(0);
		totalexperiments=new Integer(0);
		currentlocalsubjects=new Integer(0);
		currentlocalexperiments=new Integer(0);
		currentremotesubjects=new Integer(0);
		currentremoteexperiments=new Integer(0);
		this.log=new ArrayList<String>();
	}

	

	/**
	 * Gets the visits.
	 *
	 * @return the visits
	 */
	public List<String> getVisits() {
		return visits;
	}

	/**
	 * Sets the visits.
	 *
	 * @param visits
	 *            the new visits
	 */
	public void setVisits(List<String> visits) {
		this.visits = visits;
	}

	/**
	 * Gets the groups.
	 *
	 * @return the groups
	 */
	public List<String> getGroups() {
		return groups;
	}

	/**
	 * Sets the groups.
	 *
	 * @param groups
	 *            the new groups
	 */
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

	/**
	 * Gets the subjects.
	 *
	 * @return the subjects
	 */
	public List<String> getSubjects() {
		return subjects;
	}

	/**
	 * Sets the subjects.
	 *
	 * @param subjects
	 *            the new subjects
	 */
	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
		this.totalsubjects = (this.subjects != null) ? this.subjects.size() : new Integer(0);

	}

	/**
	 * Gets the sessionfilters.
	 *
	 * @return the sessionfilters
	 */
	public List<SessionFilter> getSessionfilters() {
		return sessionfilters;
	}

	/**
	 * Sets the sessionfilters.
	 *
	 * @param sessionfilters
	 *            the new sessionfilters
	 */
	public void setSessionfilters(List<SessionFilter> sessionfilters) {
		this.sessionfilters = sessionfilters;
	}

	/**
	 * Gets the dproject.
	 *
	 * @return the dproject
	 */
	public String getDproject() {
		return dproject;
	}

	/**
	 * Sets the dproject.
	 *
	 * @param dproject
	 *            the new dproject
	 */
	public void setDproject(String dproject) {
		this.dproject = dproject;
	}

	/**
	 * Gets the sproject.
	 *
	 * @return the sproject
	 */
	public String getSproject() {
		return sproject;
	}

	/**
	 * Sets the sproject.
	 *
	 * @param sproject
	 *            the new sproject
	 */
	public void setSproject(String sproject) {
		this.sproject = sproject;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the createdon.
	 *
	 * @return the createdon
	 */
	public String getCreatedon() {
		return createdon;
	}

	/**
	 * Sets the createdon.
	 *
	 * @param createdon
	 *            the new createdon
	 */
	public void setCreatedon(String createdon) {
		this.createdon = createdon;
	}

	/**
	 * Gets the createdby.
	 *
	 * @return the createdby
	 */
	public String getCreatedby() {
		return createdby;
	}

	/**
	 * Sets the createdby.
	 *
	 * @param createdby
	 *            the new createdby
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	/**
	 * Gets the node.
	 *
	 * @return the node
	 */
	public String getNode() {
		return node;
	}

	/**
	 * Sets the node.
	 *
	 * @param node
	 *            the new node
	 */
	public void setNode(String node) {
		this.node = node;
	}

	/**
	 * Merge.
	 *
	 * @param dpayload
	 *            the dpayload
	 */
	public void merge(Payload dpayload) {

		// merge demographics from config defaults
		if (dpayload.getDemographics() != null) {
			Map<String, Boolean> defaultdemographics = dpayload.getDemographics();
			Map<String, Boolean> demographics = this.getDemographics();
			for (String dkey : defaultdemographics.keySet()) {
				if (!demographics.containsKey(dkey)) {
					demographics.put(dkey, defaultdemographics.get(dkey));
				}
			}
		}

		// merge filters fields from config defaults
		if (dpayload.getSessionfilters() != null) {
			for (SessionFilter filter : this.getSessionfilters()) {
				for (SessionFilter dfilter : dpayload.getSessionfilters()) {
					if (StringUtils.equals(dfilter.getFiltertype(), filter.getFiltertype())) {
						Map<String, Boolean> dfields = dfilter.getFields();
						Map<String, Boolean> fields = filter.getFields();
						for (String dkey : dfields.keySet()) {
							if (!fields.containsKey(dkey)) {
								fields.put(dkey, dfields.get(dkey));
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Gets the totalsubjects.
	 *
	 * @return the totalsubjects
	 */
	public Integer getTotalsubjects() {
		return totalsubjects;
	}

	/**
	 * Gets the totalexperiments.
	 *
	 * @return the totalexperiments
	 */
	public Integer getTotalexperiments() {
		return totalexperiments;
	}

	public List<String> getExperiments() {
		return experiments;
	}

	public void setExperiments(List<String> experiments) {
		this.experiments = experiments;
	}

	public void setTotalsubjects(Integer totalsubjects) {
		this.totalsubjects = totalsubjects;

	}
	public String getJsonFilename() {
		return this.getDproject() + "_" + this.getCreatedon() + "_" + this.getCreatedby() + ".json";
	}
}
