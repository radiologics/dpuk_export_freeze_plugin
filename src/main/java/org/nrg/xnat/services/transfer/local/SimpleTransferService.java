/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.event.EventUtils.CATEGORY;
import org.nrg.xft.event.EventUtils.TYPE;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.SimpleExportService;
import org.nrg.xnat.services.transfer.TransferService;
import org.nrg.xnat.services.transfer.TransformService;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Rules;
import org.nrg.xnat.services.transfer.domain.Transfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleTransferService implements TransferService {

	@Autowired
	private TransformService transformService;
	
	@Autowired
	private SimpleExportService exportService;
	

	public SimpleTransferService() {
		super();
		
	}
	
	public SimpleTransferService(TransformService transformService,
			SimpleExportService exportService) {
		super();
		this.transformService = transformService;
		this.exportService = exportService;
	}

	@Override
	public void transfer(UserI user,Transfer transfer) throws UnknownPrimaryProjectException, Exception {
		// TODO Auto-generated method stub
		//need validation and security check
		
		Payload payload=transfer.getPayload();
		Rules rules=transfer.getRules();
		
		//1. transform data based on rules
		transformService.transform(user,payload,rules,new EventDetails(CATEGORY.PROJECT_ADMIN,TYPE.STORE_XML,"transform subjects","process","comment"));
		//2 export data locally.
		exportService.export(user,payload, new EventDetails(CATEGORY.PROJECT_ADMIN,TYPE.STORE_XML,"copy subjects","process","comment"));
		//3 anonymize data in place. service.anonymize(user,experimentdata)

	};
}
