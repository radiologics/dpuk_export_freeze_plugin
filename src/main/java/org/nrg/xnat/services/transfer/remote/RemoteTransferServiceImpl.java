package org.nrg.xnat.services.transfer.remote;

import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.event.EventUtils.CATEGORY;
import org.nrg.xft.event.EventUtils.TYPE;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.RemoteExportService;
import org.nrg.xnat.services.transfer.RemoteTransferService;
import org.nrg.xnat.services.transfer.TransformService;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Rules;
import org.nrg.xnat.services.transfer.domain.Transfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RemoteTransferServiceImpl implements RemoteTransferService {

	@Autowired
	private TransformService transformService;
	
	@Autowired
	private RemoteExportService remoteExportService;
	

	public RemoteTransferServiceImpl() {
		super();
		
	}
	
	public RemoteTransferServiceImpl(TransformService transformService,
			RemoteExportService remoteExportService) {
		super();
		this.transformService = transformService;
		this.remoteExportService = remoteExportService;
	}

	@Override
	public void transfer(UserI user,Transfer transfer) throws UnknownPrimaryProjectException, Exception {
		// TODO Auto-generated method stub
		//need validation and security check
		
		Payload payload=transfer.getPayload();
		Rules rules=transfer.getRules();
		
		//1. transform data based on rules
		//transformService.transform(user,payload,rules,new EventDetails(CATEGORY.PROJECT_ADMIN,TYPE.STORE_XML,"transform subjects","process","comment"));
		//2 export data locally.
		remoteExportService.export(user,payload, new EventDetails(CATEGORY.PROJECT_ADMIN,TYPE.STORE_XML,"copy subjects","process","comment"));
		//3 anonymize data in place. service.anonymize(user,experimentdata)

	};
}
