/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;

@com.fasterxml.jackson.annotation.JsonIgnoreProperties
public class RemoteProjectRelationship implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4837828396927532720L;
	
	
	RemoteConnection connection;
	
	String project1;
	String project2;
	String type;
	String properties;
	public RemoteConnection getConnection() {
		return connection;
	}
	public void setConnection(RemoteConnection connection) {
		this.connection = connection;
	}
	public String getProject1() {
		return project1;
	}
	public void setProject1(String project1) {
		this.project1 = project1;
	}
	public String getProject2() {
		return project2;
	}
	public void setProject2(String project2) {
		this.project2 = project2;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getProperties() {
		return properties;
	}
	public void setProperties(String properties) {
		this.properties = properties;
	}

	

}
