/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.XnatSubjectdata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * 
 * 
 * @author james@radiologics.com
 *
 */

@Service
public class SimpleExportDelayCalculationsServiceImpl implements SimpleExportDelayCalculationsService {

	

	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.local.SimpleExportDelayCalculationsService#calc(org.nrg.xdat.om.XnatSubjectdata, org.nrg.xdat.om.XnatSubjectdata)
	 */
	// have to rename files.
	@Override
	public void calc(final XnatSubjectdata orig, XnatSubjectdata subject){
		final List<XnatSubjectassessordataI> newexpts = orig.getExperiments_experiment();
		List <XnatSubjectassessordataI> sessions=new ArrayList<XnatSubjectassessordataI>();
		
		
		for (XnatSubjectassessordataI assess : newexpts) {
			if (assess.getDate()!=null) {
				sessions.add(assess);
			}
		}
		
		if(sessions.size()>0){
			Collections.sort(sessions,new  ExperimentDateComparator());	
			calculateDelay(sessions.get(0),newexpts);
		}
		
	}

	
	/**
	 * Calculate delay between the earliest session and the others and assign delay.
	 *
	 * @param earliest the earliest
	 * @param newexpts the newexpts
	 */
	private void calculateDelay(XnatSubjectassessordataI earliest,List<XnatSubjectassessordataI> newexpts) {
		Date earliestDate=(Date)earliest.getDate();
		for (XnatSubjectassessordataI xnatSubjectassessordataI : newexpts) {
				Date currentDate=(Date)xnatSubjectassessordataI.getDate();
				long delay=this.getDateDiff(earliestDate, currentDate);
				if(delay <=1){delay=0;}   
				logger.debug("setdelay "+delay);
				xnatSubjectassessordataI.setDelay(new Long(delay).intValue());
		}
		return;
	}

	
	public long getDateDiff(Calendar c1, Calendar c2)
    {
        long time1 = c1.getTime().getTime();
        long time2 = c2.getTime().getTime();

        if (time1 > time2)
            return -1;

        return ((time2 - time1) / 86400000) + 1;
    }
	
	public long getDateDiff(Date o,Date d) {
        try {
           

            Calendar dobC = new GregorianCalendar();
            dobC.setTime(o);
            Calendar acqC = new GregorianCalendar();
            acqC.setTime(d);
            long days = getDateDiff(dobC, acqC);
            return Math.round(Math.floor(days));
        } catch (Exception e) {
            return 0;
        }
    }
	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(SimpleExportDelayCalculationsServiceImpl.class);

}
