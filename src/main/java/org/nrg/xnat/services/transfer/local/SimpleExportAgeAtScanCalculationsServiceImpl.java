/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import java.util.Date;
import java.util.List;

import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.XnatSubjectdata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * 
 * @author james@radiologics.com
 *
 */

@Service
public class SimpleExportAgeAtScanCalculationsServiceImpl implements SimpleExportAgeAtScanCalculationsService {

	

	/**
	 * 
	 * 
	 * 
	 */
	@Override
	public void calc(final XnatSubjectdata orig, XnatSubjectdata subject){
		final List<XnatSubjectassessordataI> newexpts = subject.getExperiments_experiment();
		
		for (XnatSubjectassessordataI assess : newexpts) {
			if(assess.getAge()==null){
				try{
					String age=orig.getAge((Date)assess.getDate());
					assess.setAge(Double.valueOf(age));
					logger.info("set age at scan "+ Double.valueOf(age));
				}catch(Exception ex){
					logger.info("set age at scan failed "+ex.getMessage());//no age means age not set.
				}
			}
		}
	}

	/** The Constant logger. */
	private final static Logger logger = LoggerFactory.getLogger(SimpleExportAgeAtScanCalculationsServiceImpl.class);

}
