/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.Payload;

public interface SimpleManifestUpdater {

	void update(UserI user, Payload payload);
	public void verify(UserI user,Payload payload) throws Exception;

}