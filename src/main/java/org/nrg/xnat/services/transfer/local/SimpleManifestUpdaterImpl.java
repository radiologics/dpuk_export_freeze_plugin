/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.bean.CatCatalogBean;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xdat.model.XnatResourcecatalogI;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectResourceModifierBuilder;
import org.nrg.xnat.helpers.resource.direct.ResourceModifierA.UpdateMeta;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.utils.CatalogUtils;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

@Service
public class SimpleManifestUpdaterImpl   implements SimpleManifestUpdater {
	
	private final String DATAFREEZE="datafreeze";
	public void update(UserI user, Payload payload) {
		this.update(user, payload.getSproject(),payload);
		this.update(user, payload.getDproject(),payload);
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.nrg.xnat.services.transfer.local.ManifestUpdater#update(org.nrg.xnat.
	 * services.transfer.domain.Payload)
	 */
	
	private void update(UserI user,String project, Payload payload) {
		String name = payload.getDproject() + "_" + payload.getCreatedon() + "_" + payload.getCreatedby() + ".json";

		XnatProjectdata proj;
		try {
			proj = XnatProjectdata.getProjectByIDorAlias(project, new XDATUser(user.getUsername()),
					false);
			DirectResourceModifierBuilder builder = new DirectResourceModifierBuilder();
			builder.setProject(proj);
			List<XnatResourcecatalogI> resources = proj.getResources_resource();

			for (XnatResourcecatalogI xnatAbstractresourceI : resources) {
				final String type = xnatAbstractresourceI.getLabel();
				if (DATAFREEZE.equals(type)) {
					final File catalogFile = CatalogUtils.getCatalogFile(proj.getArchiveRootPath(),
							xnatAbstractresourceI);
					CatCatalogBean cat = CatalogUtils.getCatalog(catalogFile);
					boolean foundManifest=false;
					for (CatEntryI match : CatalogUtils.getEntriesByRegex(cat, ".*.json")) {
						if (match.getName().equals(name)) {
							foundManifest=true;
							String parentPath = (new File(xnatAbstractresourceI.getUri())).getParent();
							File json = CatalogUtils.getFile(match, parentPath);

							String decodedpayload = new ObjectMapper().writeValueAsString(payload);
							logger.info("update:" + json.getAbsolutePath());
							File temp = File.createTempFile("xnat", ".json");

							FileUtils.writeStringToFile(temp, decodedpayload);

							final EventMetaI i = EventUtils.DEFAULT_EVENT(user, null);
							UpdateMeta um = new UpdateMeta(i, true);
							builder.buildResourceModifier(true, new XDATUser(user.getUsername()), i).addFile(
									(List<? extends FileWriterWrapperI>) Lists.newArrayList(new StoredFile(temp, true)),
									xnatAbstractresourceI.getXnatAbstractresourceId(), null, json.getName(), buildResourceInfo(um, user),
									false);
						}
					}
					if(!foundManifest){
						logger.error("Failed to find manifest file: "+ name);
					}
				}

			}
		} catch (Exception e) {
			logger.error("manifest file not found! "+ name);
		}
	}

	public XnatResourceInfo buildResourceInfo(EventMetaI ci, UserI user) {
		Date d = EventUtils.getEventDate(ci, false);
		return XnatResourceInfo.buildResourceInfo(null, null, null, null, user, d, d, EventUtils.getEventId(ci));
	}

	
	
	
	public void verify(UserI user,Payload payload) throws Exception {
		
		
		int status_subj=	Integer.compare(payload.getCurrentlocalsubjects(), payload.getTotalsubjects());
		int status_exp=		Integer.compare(payload.getCurrentlocalexperiments(), payload.getTotalexperiments());
		if(status_subj!=0){
			logger.error("subjects: "+payload.getCurrentlocalsubjects()+" / "+ payload.getTotalsubjects());
			throw new RuntimeException("Manifest verification failed. Subject mismatch: "+"subjects: "+payload.getCurrentlocalsubjects()+" / "+ payload.getTotalsubjects());
		}
		if(status_exp!=0){
			logger.error("experiments: "+payload.getCurrentlocalexperiments()+" / "+ payload.getTotalexperiments());
			throw new RuntimeException("Manifest verification failed. Experiments mismatch: "+"experiments: " +payload.getCurrentlocalexperiments()+" / "+ payload.getTotalexperiments());
		}			
		
	}
	
	public static Logger logger = Logger.getLogger(SimpleManifestUpdaterImpl.class);

}