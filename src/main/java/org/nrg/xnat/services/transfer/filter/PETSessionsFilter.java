/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.SessionFilter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


/**
 * The Class PETSessionFilter.
 */
public class PETSessionsFilter extends AbstractFilter {

	/** The logger. */
	public static Logger logger = Logger.getLogger(PETSessionsFilter.class);

	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.filter.Filter#filter(org.nrg.xnat.services.transfer.domain.Payload)
	 */
	@Override
	public List<String> filter(SessionFilter filter,Payload payload) {
		List<String> results = new ArrayList<String>();
		try {

			List<String> scantypes = new ArrayList<String>();
			List<String> tracers = new ArrayList<String>();

			if (filter != null) {
				scantypes.addAll(filter.getScantypes());
				tracers.addAll(filter.getTracers());
			}

			if (payload.getGroups().size() == 0) {
				throw new Exception("Missing groups");
			}
			if (payload.getVisits().size() == 0) {
				throw new Exception("Missing visits");
			}

			MapSqlParameterSource parameters = new MapSqlParameterSource();

			String query = "select distinct(pet.id) as experiment_id from xnat_petsessiondata  pet  ";
			query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=pet.id  ";
			query += " LEFT JOIN xnat_subjectdata s ON sa.subject_id=s.id  "; 
			query += " LEFT JOIN xnat_experimentdata e ON e.id=pet.id  ";
			query += " LEFT JOIN xnat_imagescandata scan ON e.id=scan.image_session_id ";
			query += " where e.project = :project ";

			if (scantypes != null && scantypes.size() > 0) {
				query += " and scan.type in  (:scantype) ";
				parameters.addValue("scantype", scantypes);
			}

			
			if(payload.getGroups().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)){
				query += " and (s._group in (:groups) or s._group is null)";
			}else{
				query += " and s._group in  (:groups) ";
			}
			if(payload.getVisits().contains(DataFreezeUtils.NULL_VALUE_UI_REPRESENTATION)){
				query += " and (e.visit_id in  (:visits) or e.visit_id is null)";
			}else{
				query += " and e.visit_id in  (:visits) ";
			}
			
			if(tracers.contains("")){
				query += " and (pet.tracer_name in  (:tracers) or pet.tracer_name is null)";
			}else{
				query += " and pet.tracer_name in  (:tracers) ";
			}
			
			query += " ORDER BY pet.id DESC ";

			parameters.addValue("groups", payload.getGroups());
			parameters.addValue("visits", payload.getVisits());
			
			parameters.addValue("project", payload.getSproject());
			
			parameters.addValue("tracers", tracers);

			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(
					new JdbcTemplate(XDAT.getDataSource()));
			results = jdbcTemplate.queryForList(query, parameters, String.class);

			logger.debug(query);
			
			for(String tracer :tracers){
				logger.debug("tracer: "+tracer);
			}
			
			for (String result : results) {
				logger.debug("PETSessionFilter whitelist "+result);
			}
			return results;
		} catch (Exception e) {
			logger.error("failed to find sessions", e);
			return new ArrayList<String>();// empty
		}

	}
	
	
}
