/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.remote;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatExperimentdataShareI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xdat.model.XnatReconstructedimagedataI;
import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXWriter;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.representations.ZipRepresentation;
import org.nrg.xnat.services.transfer.RemoteExportService;
import org.nrg.xnat.services.transfer.domain.ManifestEntry;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Payload.PayloadType;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.restlet.data.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
/*
 * Transfer data remotely to another xnat instance.
 */
import org.springframework.stereotype.Service;

/**
 * @author james@radiologics.com
 *
 */
@Service
public class RemoteExportServiceImpl implements RemoteExportService {

	private static final String TIMESTAMP = "yyyyMMdd_HHmmss";
	private static final String PATTERN = "_v[0-9]{1,8}_\\d+$";
	public static Logger logger = Logger.getLogger(RemoteExportServiceImpl.class);
	private static final long serialVersionUID = 8092462697472805786L;

	java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(PATTERN);

	@Autowired
	RemoteRESTService remoteService;

	
	@Autowired
	RemoteManifestUpdater remoteManifestUpdater;
	
	
	public RemoteExportServiceImpl() {
		super();
	}

	/**
	 * Remote export service to copy subjects from source to destination project
	 */

	@Override
	public void export(UserI user, Payload payload, EventDetails ed) throws Exception {
		String dproject="";
		
		
		try{
			dproject = payload.getDproject();
				
			String sproject = payload.getSproject();
			XnatProjectdata sproj = XnatProjectdata.getProjectByIDorAlias(sproject, (XDATUser) user, true);
	
			List<XnatSubjectdata> subjects = new ArrayList<XnatSubjectdata>();
	
			XnatProjectdata proj = XnatProjectdata.getProjectByIDorAlias(payload.getSproject(), (XDATUser) user, true);
	
			if (proj != null){
				subjects.addAll(proj.getParticipants_participant());
			}
			remoteManifestUpdater.update(user,PayloadType.REMOTEPROJECT,Payload.SHARING_IN_PROGRESS,new ManifestEntry("project: "+dproject,ManifestEntry.SUCCESS,"Start Sharing Data",""),payload);
	
			for (XnatSubjectdata subj : subjects) {
				transferSubject(user, payload, dproject, subj, ed);
			}
			
			//verifyManifest....
			remoteManifestUpdater.verify(user, payload);
			//add last entry.
			remoteManifestUpdater.update(user,PayloadType.REMOTEPROJECT,Payload.SHARING_COMPLETE,new ManifestEntry("project: "+dproject,ManifestEntry.SUCCESS,"Finished Sharing Data",""),payload);
			remoteManifestUpdater.updateRemote(user, payload);
		}catch(Exception ex){
			remoteManifestUpdater.update(user,PayloadType.REMOTEPROJECT,Payload.SHARING_FAILED,new ManifestEntry("project: "+dproject,ManifestEntry.FAIL,"Failed Sharing Data",ex.getMessage()),payload);
			remoteManifestUpdater.updateRemote(user, payload);
			throw new Exception(ex);
		}
			
		//upload json to hub
		
	}
	
	
	File buildxar(UserI user, Payload payload,XnatImagesessiondata orig, String targetproject,XnatSubjectdata targetsubject, XnatImagesessiondata target) throws Exception {

		File xarFile;
		try {
			File experimentPath = new File(orig.getArchiveRootPath() + "arc001/" + orig.getArchiveDirectoryName());


			ZipRepresentation rep=new ZipRepresentation(MediaType.APPLICATION_ZIP,(orig).getArchiveDirectoryName(),0);

			List<File> files = (List<File>) FileUtils.listFiles(experimentPath,null,true);

			String expCachePath = ArcSpecManager.GetInstance().getGlobalCachePath() + "USERS" + File.separator+ user.getID()+File.separator+orig.getId()+File.separator+(new Date()).getTime();
			new File(expCachePath).mkdirs();
			File outF = new File(expCachePath, "expt_" + (new Date()).getTime() + ".xml");
			outF.deleteOnExit();
			FileOutputStream fos = new FileOutputStream(outF);
			SAXWriter writer = new SAXWriter(fos, true);
			writer.setAllowSchemaLocation(true);
			writer.setLocation(expCachePath);
			writer.setRelativizePath(((XnatSubjectassessordata) orig).getArchiveDirectoryName() + "/");
			
			orig.setId("");
			orig.setProject(target.getProject());
			orig.setSubjectId(targetsubject.getLabel());
			for (XnatImagescandataI scan : orig.getScans_scan()) {
				scan.setImageSessionId("");
			}
			writer.write(orig.getItem());
			
			rep.addEntry(((XnatSubjectassessordata)target).getLabel() + ".xml",outF);
			rep.addAll(files);
			
			rep.setDownloadName(orig.getId()+".xar");
			xarFile = new File(expCachePath, (new Date()).getTime()+".xar");
			xarFile.deleteOnExit();
			rep.write(new FileOutputStream(xarFile));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to retrieve/save session XML."+e.getMessage());
		}
		return xarFile;
		

		// return

	}

	/**
	 * @param newEXPT
	 *            experiment for correction
	 * @return corrected id
	 * @throws Exception
	 */
	private XnatExperimentdata correctIDandLabel(String targetproject,XnatSubjectdata targetsubject,XnatImagesessiondata origExperiment) throws Exception {

		
		XFTItem item = origExperiment.getItem().copy();

		XnatImagesessiondata targetExperiment = (XnatImagesessiondata) BaseElement.GetGeneratedItem(item);
		String newid = "";
		targetExperiment.setId(newid);
		targetExperiment.setProject(targetproject);
		targetExperiment.setSubjectId(targetsubject.getLabel());
		// correct shared projects
		for (XnatExperimentdataShareI share : targetExperiment.getSharing_share()) {
			if (share.getLabel() != null) {
				String label2 = share.getLabel();
				share.setLabel(label2);
			}
		}
		return targetExperiment;
	}

	private String correctIDandLabel(XnatReconstructedimagedataI newRecon) throws Exception {
		String newid = "";
		newRecon.setId(newid);
		return newid;
	}

	/**
	 * @param newEXPT
	 *            experiment for correction
	 * @param dateAppend
	 *            date to be appended
	 * @return corrected id
	 * @throws Exception
	 */
	private XnatSubjectdata correctIDandLabel(String targetproject,XnatSubjectdata originalSubject) throws Exception {
		XFTItem item = originalSubject.getItem().copy();

		XnatSubjectdata targetSubject = (XnatSubjectdata) BaseElement.GetGeneratedItem(item);
		
		String newid = "";
		// correct ID
		targetSubject.setId(newid);
		targetSubject.setProject(targetproject);
		// correct shared projects
		for (XnatProjectparticipantI share : targetSubject.getSharing_share()) {
			if (share.getLabel() != null) {
				String label2 = share.getLabel();
				share.setLabel(label2);
			}
		}
		return targetSubject;
	}
	private XnatSubjectassessordata correctIDandLabel(String targetproject,XnatSubjectdata targetSubject,XnatSubjectassessordata originalSubjectAssessor) throws Exception {

		XFTItem item = originalSubjectAssessor.getItem().copy();

		XnatSubjectassessordata targetSubjectAssessor = (XnatSubjectassessordata) BaseElement.GetGeneratedItem(item);
		
		
		String newid = "";
		// correct ID
		targetSubjectAssessor.setId(newid);
		targetSubjectAssessor.setProject(targetproject);
		targetSubjectAssessor.setSubjectId(targetSubject.getLabel());
		
		return targetSubjectAssessor; 
	}

	

	private XnatSubjectdata transferSubject(UserI user, Payload payload, String targetProject,
			XnatSubjectdata originalSubject, EventDetails ed) throws Exception {

		
		logger.info("originalSubject:" + originalSubject +" from "+ originalSubject.getProject());
		XnatSubjectdata targetSubject =correctIDandLabel(targetProject, originalSubject);
		boolean status=remoteService.importSubject(payload.getConnection(), payload, targetSubject);
		String  subjectEntryStatus= (status) ? ManifestEntry.SUCCESS: ManifestEntry.FAIL;
		remoteManifestUpdater.update(user,PayloadType.REMOTESUBJECT,Payload.SHARING_IN_PROGRESS,new ManifestEntry("subject: "+targetSubject.getLabel(),subjectEntryStatus,"Sharing Subject",""),payload);

		logger.info("targetSubject:" + targetSubject +" to "+ targetSubject.getProject());

		
		final List<XnatSubjectassessordataI> expts = originalSubject.getExperiments_experiment();
		
		for (XnatSubjectassessordataI originalAssessor : expts) {
			if (originalAssessor instanceof XnatImagesessiondata) {
				XnatExperimentdata targetExperiment=correctIDandLabel(targetProject,targetSubject,(XnatImagesessiondata) originalAssessor);
				try{
					 File xar=buildxar( user, payload, (XnatImagesessiondata) originalAssessor, targetProject, targetSubject, (XnatImagesessiondata) targetExperiment);
					 remoteService.importXar(payload.getConnection(), payload, xar);
					 remoteManifestUpdater.update(user,PayloadType.REMOTEEXPERIMENT,Payload.SHARING_IN_PROGRESS,new ManifestEntry(targetExperiment.getLabel(),ManifestEntry.SUCCESS,"Sharing Experiment",""),payload);

				}catch(Exception ex){//TODO
					 logger.error("importXar failed,",ex);
					 remoteManifestUpdater.update(user,PayloadType.ERROR,Payload.SHARING_FAILED,new ManifestEntry(targetExperiment.getLabel(),ManifestEntry.FAIL,"Failed Sharing Experiment",ex.getMessage()),payload);

					 throw new Exception("importXar failed,");
				}
			
			} else {
				XnatSubjectassessordata targetAssessor = correctIDandLabel(targetProject,targetSubject,(XnatSubjectassessordata)originalAssessor);
				try{	
						remoteService.importSubjectAssessor(payload.getConnection(), payload, targetSubject,(XnatSubjectassessordata) targetAssessor);
						remoteManifestUpdater.update(user,PayloadType.REMOTEEXPERIMENT,Payload.SHARING_IN_PROGRESS,new ManifestEntry(targetSubject.getLabel(),ManifestEntry.SUCCESS,"Sharing Experiment",""),payload);

				}catch(Exception ex){//TODO
					 	logger.error("importSubjectAssessor failed,",ex);
						remoteManifestUpdater.update(user,PayloadType.ERROR,Payload.SHARING_FAILED,new ManifestEntry(targetSubject.getLabel(),ManifestEntry.FAIL,"Failed Sharing Experiment",ex.getMessage()),payload);
					 	throw new Exception("importSubjectAssessor failed,");
				 }
			}
		}

		return null;
	}

	
	
	
}
