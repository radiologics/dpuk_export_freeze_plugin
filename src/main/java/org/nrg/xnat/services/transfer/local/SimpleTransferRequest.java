/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import java.io.Serializable;

import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.Transfer;

public class SimpleTransferRequest implements Serializable {

    
   

    /**
	 * 
	 */
	private static final long serialVersionUID = -1956760856746602128L;
	public SimpleTransferRequest(final UserI user, Transfer transfer) {
        _user = user;
        _transfer=transfer;
        
    }

    public UserI getUser() {
        return _user;
    }
    public Transfer getTransfer() {
		// TODO Auto-generated method stub
		return  _transfer;
	}
    


    private final UserI _user;
    private final Transfer _transfer;
	
}
