/*
 * org.nrg.xnat.services.messaging.prearchive.MoveSessionRequestListener
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 12/19/13 3:01 PM
 */
package org.nrg.xnat.services.transfer.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class RemoteTransferRequestListener {

	@Autowired
	RemoteTransferRequestHandler remoteTransferRequestHandler;

	@SuppressWarnings("unused")
    @JmsListener(containerFactory = "exportFreezeListenerContainerFactory", destination = "remoteTransferRequest")
	public void onRequest(final RemoteTransferRequest request) {
		try {
			if (_log.isDebugEnabled()) {
				_log.debug("Received request from user {} to perform {} some operation",
						request.getUser().getUsername());
			}
			remoteTransferRequestHandler.handle(request);
			_log.info("do stuff with request.......");
		} catch (Exception e) {
			final String message = String.format(
					"An error occurred processing a request from user %s to perform some operation ",
					request.getUser().getUsername());
			_log.error(message, e);
		}
	}

	private final static Logger _log = LoggerFactory.getLogger(RemoteTransferRequestListener.class);
}
