/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

public interface SimpleTransferRequestHandler {
    public void handle(SimpleTransferRequest request) ;
}
