/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import org.nrg.xdat.om.XnatSubjectdata;


public interface SimpleExportAgeAtScanCalculationsService {
	public void calc(final XnatSubjectdata original, XnatSubjectdata subject)   ;
}
