/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer;

import org.nrg.xdat.om.base.BaseXnatExperimentdata.UnknownPrimaryProjectException;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.Payload;

public interface SimpleExportService {

	public void export(UserI user, Payload payload, EventDetails e) throws UnknownPrimaryProjectException, Exception;
}
