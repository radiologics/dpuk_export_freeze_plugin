/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;

@com.fasterxml.jackson.annotation.JsonIgnoreProperties
public class RemoteStudyFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 997418112820990186L;
	
	
	RemoteConnection connection;


	public RemoteConnection getConnection() {
		return connection;
	}


	public void setConnection(RemoteConnection connection) {
		this.connection = connection;
	}
	
	

}
