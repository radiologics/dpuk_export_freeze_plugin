/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;


@com.fasterxml.jackson.annotation.JsonIgnoreProperties
public class RemoteConnection implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3457916201213405949L;
	String url;
	String username;
	String password;
	String jsessionid;
	//AliasToken alias;
	
	
	
	public String getJsessionid() {
		return jsessionid;
	}
	public void setJsessionid(String jsessionid) {
		this.jsessionid = jsessionid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
