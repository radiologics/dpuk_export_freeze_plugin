/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;



@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true) 
public class RemoteProject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 997418112820990186L;
	String id;
	String name;
	String description;
	String accessibility;
	
	RemoteConnection connection;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public RemoteConnection getConnection() {
		return connection;
	}
	public void setConnection(RemoteConnection connection) {
		this.connection = connection;
	}
	
	public String getAccessibility() {
		return accessibility;
	}
	public void setAccessibility(String accessibility) {
		this.accessibility = accessibility;
	}

}
