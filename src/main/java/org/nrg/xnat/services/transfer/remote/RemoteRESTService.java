package org.nrg.xnat.services.transfer.remote;

import java.io.File;
import java.net.URISyntaxException;

import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.RemoteConnection;
import org.nrg.xnat.services.transfer.domain.RemoteProject;
import org.nrg.xnat.services.transfer.domain.RemoteProjectRelationship;
import org.nrg.xnat.services.transfer.domain.RemoteStudyFilter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

public interface RemoteRESTService {
	public boolean importSubject(RemoteConnection connection, Payload payload,XnatSubjectdata subject);
	public boolean importSubjectAssessor(RemoteConnection connection,Payload payload,XnatSubjectdata subject,XnatSubjectassessordata assessor );
	public boolean importXar(RemoteConnection connection,Payload payload,File xar);
	//public boolean importXarJersey(RemoteConnection connection, Payload payload, File xar);
	
	public ResponseEntity<String> importProject(RemoteProject remoteProject) throws RestClientException, URISyntaxException;
	public ResponseEntity<String> getStudies(RemoteStudyFilter studyFilter) throws Exception;
	public RemoteConnection getConnection(RemoteConnection connection);
	public ResponseEntity<String> importProjectRelationship(RemoteProjectRelationship projectRelationship);
	public boolean importPayload(RemoteConnection connection, String project,File payloadfile,String name);

}