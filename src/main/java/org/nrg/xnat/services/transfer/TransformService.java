/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer;

import org.nrg.xft.event.EventDetails;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Rules;

public interface TransformService {

	   public void transform(UserI user,Payload payload,Rules rules,EventDetails e);
}
