/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.services.transfer.domain.Payload;

public interface SubjectFilter {
	 void setNextChain(SubjectFilter nextSubjectFilter);
	 void filter(XnatSubjectdata subject,Payload payload);
}
