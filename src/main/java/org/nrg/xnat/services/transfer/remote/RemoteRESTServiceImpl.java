package org.nrg.xnat.services.transfer.remote;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.services.transfer.domain.AliasToken;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.RemoteConnection;
import org.nrg.xnat.services.transfer.domain.RemoteProject;
import org.nrg.xnat.services.transfer.domain.RemoteProjectRelationship;
import org.nrg.xnat.services.transfer.domain.RemoteStudyFilter;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


// TODO: Auto-generated Javadoc
/**
 * The Class RemoteRESTServiceImpl.
 */
@Service
//@EnableRetry
//TODO update to retry when we upgrade spring to 4
public class RemoteRESTServiceImpl  extends AbstractRemoteRESTService implements RemoteRESTService {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(RemoteRESTServiceImpl.class);

	/** The max tries. */
	static int maxTries = 10;
	
	/** The sleep. */
	static long sleep=60000;//wait 1 minute
	
	/**
	 * Instantiates a new remote rest service impl.
	 */
	public  RemoteRESTServiceImpl() {
		super();
	}
	
	/**
	 * importXar with retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param xar the xar
	 * @return true, if successful
	 * @throws RuntimeException the runtime exception
	 */
	public boolean importXar(RemoteConnection connection, Payload payload, File xar) throws RuntimeException{
		int count = 0;
		while(true) {
		    try {
		    	 
		         return this.importXarWithoutRetry(connection, payload, xar);
		    } catch (RuntimeException e) {
		    	try {
		    		logger.error("importXar: retrycount "+ count+" "+ e.getMessage());
					Thread.sleep(sleep);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        // handle exception
		        if (++count == maxTries) throw e;
		    }
		}
	}
	
	
	
	/**
	 * Import xar without retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param xar the xar
	 * @return true, if successful
	 * @throws RuntimeException the runtime exception
	 */
	//@Retryable(maxAttempts=5,value=RuntimeException.class,backoff= @Backoff(delay=100, maxDelay=500))
	//TODO update to retry when we upgrade spring to 4
	private boolean importXarWithoutRetry(RemoteConnection connection, Payload payload, File xar) throws RuntimeException{
		//this.setAliasToken(connection);
		this.setJsessionid(connection);
		
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		body.add("field", "value");
		body.add("import-handler","XAR");
		body.add("file", new FileSystemResource(xar));
		
		HttpEntity<?> httpEntity = new HttpEntity<Object>(body, this.getAuthHeaders(connection));
		
		ResponseEntity<String> response = getResttemplate().exchange(connection.getUrl()+"/data/services/import", HttpMethod.POST, httpEntity, String.class);
		logger.info(response);
		logger.info(response.getBody());
		logger.info(response.getHeaders().get("Set-Cookie"));
		boolean status= ((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;
		
		logger.info("importXar"+xar.getName());
		logger.info("importXar request, HttpStatus: "+response.getStatusCode().value());
		if(!status){
			logger.error("importXar request failed. Retrying... HttpStatus: "+response.getStatusCode().value());
			throw new RuntimeException("importXar request failed. Retrying..."+response.getStatusCode().value());
		}else{
			logger.info("importXar request SUCCESS. HttpStatus: "+response.getStatusCode().value());
			return status;
		}
	}
	

	
	/**
	 * import Subject with retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param subject the subject
	 * @return true, if successful
	 */
	public boolean importSubject(RemoteConnection connection,Payload payload,XnatSubjectdata subject ){
		int count = 0;
		while(true) {
		    try {
		         return this.importSubjectWithoutRetry( connection, payload, subject );
		    } catch (RuntimeException e) {
		    	try {
			    	logger.error("importSubject: retrycount "+ count);
					Thread.sleep(sleep);
				} catch (InterruptedException e1) {
					logger.error("",e1);
				}
		        // handle exception
		        if (++count == maxTries) throw e;
		    }
		}
	}
	
	/**
	 * Import subject without retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param subject the subject
	 * @return true, if successful
	 */
	//TODO @Retryable(maxAttempts=5) update to retry when we upgrade spring to 4
	private boolean importSubjectWithoutRetry(RemoteConnection connection,Payload payload,XnatSubjectdata subject ){
		//do we need the assessor data and how.
		this.setJsessionid(connection);
		//MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		String subjectXml=subject.getItem().toXML_String();
		
		HttpEntity<?> httpEntity = new HttpEntity<String>(subjectXml, this.getAuthHeaders(connection));
		
		ResponseEntity<String> response = getResttemplate().exchange(connection.getUrl()+"/data/archive/projects/"+subject.getProject()+"/subjects/"+subject.getLabel()+"?inbody=true", HttpMethod.PUT, httpEntity, String.class);
		logger.info(response);
		logger.info(response.getBody());
		logger.info(response.getHeaders().get("Set-Cookie"));
		return 	((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;

	}
	
	
	/**
	 * import subject assessor with retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param subject the subject
	 * @param assessor the assessor
	 * @return true, if successful
	 */
	public boolean importSubjectAssessor(RemoteConnection connection,Payload payload,XnatSubjectdata subject,XnatSubjectassessordata assessor ){
		int count = 0;
			while(true) {
			    try {

			         return this.importSubjectAssessorWithoutRetry(  connection, payload, subject, assessor );
			    } catch (RuntimeException e) {
			    	try {
				    	logger.error("importSubjectAssessor: retrycount "+ count);
						Thread.sleep(sleep);
					} catch (InterruptedException e1) {
						logger.error("",e1);
					}
			        // handle exception
			        if (++count == maxTries) throw e;
			    }
			}
		}
	
	/**
	 * Import subject assessor without retry.
	 *
	 * @param connection the connection
	 * @param payload the payload
	 * @param subject the subject
	 * @param assessor the assessor
	 * @return true, if successful
	 */
	private boolean importSubjectAssessorWithoutRetry(RemoteConnection connection,Payload payload,XnatSubjectdata subject,XnatSubjectassessordata assessor ){
		this.setJsessionid(connection);
		String assessorXml=assessor.getItem().toXML_String();
		
		HttpEntity<?> httpEntity = new HttpEntity<String>(assessorXml, this.getAuthHeaders(connection));
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(connection.getUrl()+"/data/archive/projects/"+assessor.getProject()+"/subjects/"+subject.getLabel()+"/experiments/"+assessor.getLabel()+"?inbody=true", HttpMethod.PUT, httpEntity, String.class);
		logger.info(response);
		logger.info(response.getBody());
		logger.info(response.getHeaders().get("Set-Cookie"));
		return  ((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;

		}
	
	
	
	/**
	 * Import project without retry.
	 *
	 * @param remoteProject the remote project
	 * @return true, if successful
	 * @throws URISyntaxException 
	 * @throws RestClientException 
	 */
	
	public ResponseEntity<String> importProject(RemoteProject remoteProject) throws RuntimeException{
		RemoteConnection connection=remoteProject.getConnection();
		this.setJsessionid(connection);
		ResponseEntity<String> response=null;
		
	    URIBuilder builder=new      URIBuilder();
	    builder.setPath(connection.getUrl()+"/data/projects/"+remoteProject.getId())
	    		.addParameter("id", remoteProject.getId())
	    		.addParameter("description", remoteProject.getDescription())
	    		.addParameter("name", remoteProject.getName())
	    		.addParameter("accessibility", remoteProject.getAccessibility());
	    
	    
		HttpEntity<?> httpEntity = new HttpEntity<Object>(this.getAuthHeaders(remoteProject.getConnection()));
		RestTemplate restTemplate = new RestTemplate();
		try{
			response = restTemplate.exchange(builder.build(), HttpMethod.PUT, httpEntity, String.class);
			
		}catch(RuntimeException | URISyntaxException e){//quick fix (real fix is spring 3.1)
			if (e.getMessage().contains("no Content-Type found")) {
				return null;
			}
			logger.error("Failed to import remote project",e);
			throw new RuntimeException("Failed to import remote project",e);
		}
		logger.info(response);
		return response;
	}
	
	
	
	
	
	
	/**
	 * Get the remote studies.
	 *
	 * @param remoteStudyFilter the remote study filter
	 * @return the studies
	 * @throws Exception the exception
	 */
	public ResponseEntity<String> getStudies(RemoteStudyFilter remoteStudyFilter) throws Exception {
		RemoteConnection connection=remoteStudyFilter.getConnection();
		this.setJsessionid(remoteStudyFilter.getConnection());
		MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();     
		HttpEntity<?> httpEntity = new HttpEntity<Object>(body, this.getAuthHeaders(connection));
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(connection.getUrl()+"/data/study/filterbyowner/false?format=json", HttpMethod.POST, httpEntity, String.class);
		logger.info(response);
		logger.info(response.getBody());
		logger.info(response.getHeaders().get("Set-Cookie"));
		
		return response;
	}

	
	/**
	 * Sets the alias token.
	 *
	 * @param connection the connection
	 * @return the string
	 */
	public RemoteConnection getConnection(RemoteConnection connection){
		this.setJsessionid(connection);
		HttpEntity<String> request = new HttpEntity<String>(this.getAuthHeaders(connection));
		ResponseEntity<String> response = getResttemplate().exchange(connection.getUrl()+"/data/services/tokens/issue", HttpMethod.GET, request, String.class);
		logger.info(response);
		logger.info(response.getBody());
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			AliasToken aliasToken = (AliasToken) objectMapper.readValue(response.getBody(), AliasToken.class);
			connection.setUsername(aliasToken.getAlias());
			connection.setPassword(aliasToken.getSecret());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("",e);
			throw new RuntimeException("Failed to get remote connection.",e);
		}
		return connection;
	}

	/* (non-Javadoc)
	 * @see org.nrg.xnat.services.transfer.remote.RemoteRESTService#importProjectRelationship(org.nrg.xnat.services.transfer.domain.RemoteProjectRelationship)
	 */
	@Override
	public ResponseEntity<String> importProjectRelationship(RemoteProjectRelationship projectRelationship) {
		RemoteConnection connection=projectRelationship.getConnection();
		this.setJsessionid(connection);

		HttpEntity<?> httpEntity = new HttpEntity<Object>(this.getAuthHeaders(projectRelationship.getConnection()));
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(projectRelationship.getConnection().getUrl()+"/data/projects/"+projectRelationship.getProject1()+"/relations/"+projectRelationship.getProject2()+"/type/"+projectRelationship.getType(), HttpMethod.PUT, httpEntity, String.class);
		logger.info(response);
		return response;

	}
	
	
	public boolean importPayload(RemoteConnection connection, String project,File payloadfile,String name){
		this.setJsessionid(connection);
		
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();     
		body.add("file", new FileSystemResource(payloadfile));
		
		HttpEntity<?> httpEntity = new HttpEntity<Object>(body, this.getAuthHeaders(connection));
		
		ResponseEntity<String> response = getResttemplate().exchange(connection.getUrl()+"/data/projects/"+project+"/resources/datafreeze/files/"+name, HttpMethod.POST, httpEntity, String.class);
		logger.info(response);
		logger.info(response.getBody());
		logger.info(response.getHeaders().get("Set-Cookie"));
		boolean status= ((response.getStatusCode().value()==HttpStatus.OK.value()) || (response.getStatusCode().value()==HttpStatus.CREATED.value()))?true:false;
		
		logger.warn("importPayload"+name);
		
		return status;
		
	}
			
	
}
