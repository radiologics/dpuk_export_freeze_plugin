/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import org.nrg.xft.event.EventDetails;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.TransformService;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Rules;
import org.springframework.stereotype.Service;

@Service
public class SimpleTransformService  implements TransformService {

	public SimpleTransformService(){
		
	}
	
	@Override
	public void transform(UserI user, Payload payload,Rules rules, EventDetails e) {
		// TODO Auto-generated method stub
		
	}

}
