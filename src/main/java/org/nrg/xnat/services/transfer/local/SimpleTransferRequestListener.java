/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.local;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
@Component
public class SimpleTransferRequestListener {

	@Autowired
	SimpleTransferRequestHandler simpleTransferRequestHandler;
    @SuppressWarnings("unused")
    @JmsListener(containerFactory = "exportFreezeListenerContainerFactory", destination = "simpleTransferRequest")
    public void onRequest(final SimpleTransferRequest request) {
        try {
            if (_log.isDebugEnabled()) {
                _log.debug("Received request from user {} to perform {} some operation", request.getUser().getUsername());
            }
            simpleTransferRequestHandler.handle(request);
            _log.info("Finished request");
        } catch (Exception e) {
            final String message = String.format("An error occurred processing a request from user %s to perform some operation ", request.getUser().getUsername());
            _log.error(message, e);
        }
    }

    private final static Logger _log = LoggerFactory.getLogger(SimpleTransferRequestListener.class);
}
