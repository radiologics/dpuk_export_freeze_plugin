/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;

public class Demographics implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8037044958109042591L;
	Boolean age;
	Boolean sex;
	Boolean handedness;
	Boolean education;
	public Boolean getAge() {
		return age;
	}
	public void setAge(Boolean age) {
		this.age = age;
	}
	public Boolean getSex() {
		return sex;
	}
	public void setSex(Boolean sex) {
		this.sex = sex;
	}
	public Boolean getHandedness() {
		return handedness;
	}
	public void setHandedness(Boolean handedness) {
		this.handedness = handedness;
	}
	public Boolean getEducation() {
		return education;
	}
	public void setEducation(Boolean education) {
		this.education = education;
	}
	
}
