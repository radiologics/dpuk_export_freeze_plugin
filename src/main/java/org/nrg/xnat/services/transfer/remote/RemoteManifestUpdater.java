/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.remote;

import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.transfer.domain.ManifestEntry;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.Payload.PayloadType;

public interface RemoteManifestUpdater {

	 public void update(UserI user,PayloadType type,String status,ManifestEntry entry,Payload payload);
	 public void updateRemote(UserI user,Payload payload);
	 public void verify(UserI user, Payload payload) throws Exception;

}