/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import org.apache.commons.lang.StringUtils;
import org.nrg.xnat.services.transfer.domain.Payload;
import org.nrg.xnat.services.transfer.domain.SessionFilter;

/**
 * The Class AbstractFilter.
 */
abstract public class AbstractFilter implements Filter {

	/**
	 * Gets the filter name.
	 *
	 * @return the filter name
	 */
	public String getFilterName() {
		String name = this.getClass().getSimpleName();
		name = StringUtils.replace(name, "Filter", "");
		return name;
	}

	/**
	 * Gets the filter.
	 *
	 * @param payload the payload
	 * @return the filter
	 */
	public SessionFilter getFilter(Payload payload) {
		for (SessionFilter filter : payload.getSessionfilters()) {
			if (this.getFilterName().equals(StringUtils.deleteWhitespace(filter.getFiltertype()))) {
				return filter;
			}
		}
		return null;
	}
}
