/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties({"include","totalscans"})
public class SessionFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4049134091298540352L;
	Map<String, String> mappings;
	List<String> scantypes;
	List<String> sessiontypes;
	List<String> tracers;
	List<String> resources;
	String datatype;
	String filtertype;
	Map<String,Boolean> fields;
	Integer experimentcount;
	List<String> experiments;
	
	public List<String> getExperiments() {
		return experiments;
	}
	public void setExperiments(List<String> experiments) {
		this.experiments = experiments;
	}
	public Integer getExperimentcount() {
		return experimentcount;
	}
	public void setExperimentcount(Integer experimentcount) {
		this.experimentcount = experimentcount;
	}
	public Map<String, Boolean> getFields() {
		return fields;
	}
	public void setFields(Map<String, Boolean> fields) {
		this.fields = fields;
	}
	

	
	
	public String getFiltertype() {
		return filtertype;
	}
	public void setFiltertype(String filtertype) {
		this.filtertype = filtertype;
	}
	public Map<String, String> getMappings() {
		return mappings;
	}
	public void setMappings(Map<String, String> mappings) {
		this.mappings = mappings;
	}
	public List<String> getSessiontypes() {
		return sessiontypes;
	}
	public void setSessiontypes(List<String> sessiontypes) {
		this.sessiontypes = sessiontypes;
	}
	public List<String> getTracers() {
		return tracers;
	}
	public void setTracers(List<String> tracers) {
		this.tracers = tracers;
	}
	public List<String> getResources() {
		return resources;
	}
	public void setResources(List<String> resources) {
		this.resources = resources;
	}
	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	

	

	public List<String> getScantypes() {
		return scantypes;
	}
	public void setScantypes(List<String> scantypes) {
		this.scantypes = scantypes;
	}
	
	
}