/**
 *Copyright (c) 2015 Radiologics, Inc
 *
 *@author james@radiologics.com 
 */
package org.nrg.xnat.services.transfer.filter;

import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.services.transfer.domain.Payload;

public class ScanTypeFilter implements SubjectFilter {

	 private SubjectFilter subjectFilter;
	@Override
	public void setNextChain(SubjectFilter nextSubjectFilter) {
		this.subjectFilter=nextSubjectFilter;
		
	}

	@Override
	public void filter(XnatSubjectdata subject,Payload payload) {
		//do stuff and pass it on.
		this.subjectFilter.filter(subject, payload);
	}

	
}
