//Copyright  2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.datafreeze.handlers;

import java.util.List;

import org.nrg.framework.utilities.Reflection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * @author Mohana Ramaratnam
 * Handler to get Datatype specific details 
 */
public class ExperimentHandler {
	
    private static final Logger logger = LoggerFactory.getLogger(ExperimentHandler.class);

     private static List<DataFreezeExperimentHandlerI> handlers=null;
	
	   public synchronized static List<DataFreezeExperimentHandlerI> getHandlers() throws InstantiationException, IllegalAccessException{
	    	if(handlers==null){
	    		
		    	handlers=Lists.newArrayList();
		    	
		    	List<Class<?>> classes;
		        try {
		            classes = Reflection.getClassesForPackage("org.nrg.xnat.datafreeze.handlers");
		        } catch (Exception exception) {
		        	logger.error("",exception);
		            throw new RuntimeException(exception);
		        }
		        //TODO Modify this code to check if clazz implements the interface
		        for (Class<?> clazz : classes) {
		            if (clazz.getName().matches("(.*).DataFreeze(.*)Handler")) {
		            	handlers.add((DataFreezeExperimentHandlerI)clazz.newInstance());
		            }
		        }
	    	} 
	        return handlers;
	    }

}
