//Copyright  2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.datafreeze.handlers;

import java.util.Hashtable;

import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

/**
 * @author Mohana
 * Gets all MRSession specific details for a supplied filter of groups, visits
 */
public class DataFreezeMRSessionHandler implements DataFreezeExperimentHandlerI {
	
	 Hashtable<String,String> I_CAN_HANDLE;
	 public DataFreezeMRSessionHandler() {
		 I_CAN_HANDLE = new Hashtable<String,String>();
		 I_CAN_HANDLE.put("xnat:mrSessionData","1");
	 }
	 
	   	public boolean canHandle(String xsiType) {
	   		boolean handle = false;
	   		if (xsiType!=null) {
	   			if (I_CAN_HANDLE.containsKey(xsiType)) {
	   				handle = true;
	   			}
	   		}
	   		return handle;
	   	}

	   	public XFTTable build(UserI user, Hashtable<String,Object> params) throws Exception {
		XFTTable results = null;
		String groupsAsCSV = (String)params.get("groups");
   		String visitsAsCSV = (String)params.get("visits");
		String project  = (String)params.get("project");
		String groupsAsCSVForSQL = DataFreezeUtils.toCSVForSQL(groupsAsCSV);
		String visitsAsCSVForSQL = DataFreezeUtils.toCSVForSQL(visitsAsCSV);
		String query ="	select im.type, im.series_description from xnat_mrscandata scan";
       		query += " LEFT JOIN xnat_imagescandata im ON im.xnat_imagescandata_id=scan.xnat_imagescandata_id";
       		query += " LEFT JOIN xnat_mrSessionData m ON im.image_session_id=m.id";
       		query += " LEFT JOIN xnat_experimentdata e ON m.id = e.id";
       		query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id";
       		query += " LEFT JOIN xnat_subjectdata s on s.id=sa.subject_id";
       		query += " where e.project='"+project+"'";
       		if (DataFreezeUtils.hasNullValue(groupsAsCSV)) {
				if (!groupsAsCSVForSQL.equals("")) { 
					query += "  and (s._group in (" + groupsAsCSVForSQL + ") or s._group is NULL) ";
				}else {
					query += "  and  s._group is NULL ";
				}
			}else {
				query += "  and s._group in (" + groupsAsCSVForSQL + ") ";
			}
			if (DataFreezeUtils.hasNullValue(visitsAsCSV)) {
				if (!visitsAsCSVForSQL.equals("")) { 
					query += "  and (e.visit_id in (" + visitsAsCSVForSQL + ") or e.visit_id is NULL) ";
				}else {
					query += "  and  e.visit_id is NULL ";
				}
			}else {
				query += "  and e.visit_id in (" + visitsAsCSVForSQL + ") ";
			}
			query += " GROUP BY im.type, im.series_description ORDER BY im.type";
       		results = XFTTable.Execute(query, user.getDBName(), user.getUsername());
       		return results;
   	}
}
