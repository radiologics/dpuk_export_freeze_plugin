//Copyright  2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.datafreeze.handlers;

import java.util.Hashtable;

import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

/**
 * @author Mohana Ramaratnam
 * Gets PETSession specific details for a filter of project, group, visit.
 */
public class DataFreezePETSessionHandler implements DataFreezeExperimentHandlerI {
	
	
	
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DataFreezePETSessionHandler.class);

	
	 Hashtable<String,String> I_CAN_HANDLE;
	 public DataFreezePETSessionHandler() {
		 I_CAN_HANDLE = new Hashtable<String,String>();
		 I_CAN_HANDLE.put("xnat:petSessionData","1");
		 I_CAN_HANDLE.put("xnat:petmrSessionData","2");
	 }
	 
	   	public boolean canHandle(String xsiType) {
	   		boolean handle = false;
	   		if (xsiType!=null) {
	   			if (I_CAN_HANDLE.containsKey(xsiType)) {
	   				handle = true;
	   			}
	   		}
	   		return handle;
	   	}
    	public XFTTable build(UserI user, Hashtable<String,Object> params) throws Exception {
    		XFTTable results = null;
    		String groupsAsCSV = (String)params.get("groups");
       		String visitsAsCSV = (String)params.get("visits");
    		String project  = (String)params.get("project");
    		String groupsAsCSVForSQL = DataFreezeUtils.toCSVForSQL(groupsAsCSV);
    		String visitsAsCSVForSQL = DataFreezeUtils.toCSVForSQL(visitsAsCSV);
    		
    		String query = "select tracer_name,type,series_description,session_counts from";
    		query += " (select p.tracer_name,im.type, im.series_description from xnat_petsessiondata p";
    		query += " LEFT JOIN xnat_imagescandata im ON im.image_session_id=p.id";
    		query += " LEFT JOIN xnat_petscandata scan ON scan.xnat_imagescandata_id = im.xnat_imagescandata_id";
    		query += " LEFT JOIN xnat_experimentdata e ON p.id = e.id";
    		query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id";
    		query += " LEFT JOIN xnat_subjectdata s on s.id=sa.subject_id";
    		query += " where e.project='" + project + "'";

    		if (DataFreezeUtils.hasNullValue(groupsAsCSV)) {
				if (!groupsAsCSVForSQL.equals("")) { 
					query += "  and (s._group in (" + groupsAsCSVForSQL + ") or s._group is NULL) ";
				}else {
					query += "  and  s._group is NULL ";
				}
			}else {
				query += "  and s._group in (" + groupsAsCSVForSQL + ") ";
			}
			if (DataFreezeUtils.hasNullValue(visitsAsCSV)) {
				if (!visitsAsCSVForSQL.equals("")) { 
					query += "  and (e.visit_id in (" + visitsAsCSVForSQL + ") or e.visit_id is NULL) ";
				}else {
					query += "  and  e.visit_id is NULL ";
				}
			}else {
				query += "  and e.visit_id in (" + visitsAsCSVForSQL + ") ";
			}
			query += "  group by p.tracer_name, im.type,im.series_description order by p.tracer_name)  AS tracer_scans";
			query += "  LEFT JOIN "; 
			query += "  (select tracer_name,count(*) as session_counts from xnat_petsessiondata LEFT JOIN xnat_experimentdata e USING(id)";
			query += " where e.project='" + project + "'";
			query += "  group by tracer_name order by tracer_name) AS tracer_session USING(tracer_name)"; 

			params.put("displayTitle", "SCAN TYPES");
			logger.info("PET SESSION QUERY:\n " + query);
       		results = XFTTable.Execute(query, user.getDBName(), user.getUsername());
       		
    		return results;
    	}
}
