//Copyright  2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.datafreeze.handlers;

import java.util.Hashtable;

import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;

/**
 * @author Mohana Ramaratnam
 * 
 */
public  interface DataFreezeExperimentHandlerI {
    	public boolean canHandle(String xsiType);
    	public XFTTable build(UserI user, Hashtable<String,Object> params) throws Exception;
}
