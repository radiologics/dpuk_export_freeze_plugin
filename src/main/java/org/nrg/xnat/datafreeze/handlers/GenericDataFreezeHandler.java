package org.nrg.xnat.datafreeze.handlers;

import java.util.Hashtable;

import org.nrg.xft.XFTTable;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.datafreeze.utils.DataFreezeUtils;

public class GenericDataFreezeHandler implements DataFreezeExperimentHandlerI {
	 
	//TODO - in the future if we need a Generic Data Handler.
   	public boolean canHandle(String xsiType) {
   		return false;
   	}
	   	
	   	public XFTTable build(UserI user, Hashtable<String,Object> params) throws Exception {
		XFTTable results = null;
		String groupsAsCSV = (String)params.get("groups");
   		String visitsAsCSV = (String)params.get("visits");
		String project  = (String)params.get("project");
		String xsiType = (String)params.get("xsiType");
		String groupsAsCSVForSQL = DataFreezeUtils.toCSVForSQL(groupsAsCSV);
		String visitsAsCSVForSQL = DataFreezeUtils.toCSVForSQL(visitsAsCSV);
		String query ="	select im.type, im.series_description from xnat_imagescandata im";
       		query += " LEFT JOIN xnat_imageSessionData m ON im.image_session_id=m.id";
       		query += " LEFT JOIN xnat_experimentdata e ON m.id = e.id";
    		query += " LEFT JOIN xdat_meta_element xme ON e.extension = xme.xdat_meta_element_id ";
    		query += " LEFT JOIN xdat_element_security xes ON xes.element_name=xme.element_name ";
       		query += " LEFT JOIN xnat_subjectassessordata sa ON sa.id=e.id";
       		query += " LEFT JOIN xnat_subjectdata s on s.id=sa.subject_id";
       		query += " where e.project='"+project+"'";
       		if (DataFreezeUtils.hasNullValue(groupsAsCSV)) {
				if (!groupsAsCSVForSQL.equals("")) { 
					query += "  and (s._group in (" + groupsAsCSVForSQL + ") or s._group is NULL) ";
				}else {
					query += "  and  s._group is NULL ";
				}
			}else {
				query += "  and s._group in (" + groupsAsCSVForSQL + ") ";
			}
			if (DataFreezeUtils.hasNullValue(visitsAsCSV)) {
				if (!visitsAsCSVForSQL.equals("")) { 
					query += "  and (e.visit_id in (" + visitsAsCSVForSQL + ") or e.visit_id is NULL) ";
				}else {
					query += "  and  e.visit_id is NULL ";
				}
			}else {
				query += "  and e.visit_id in (" + visitsAsCSVForSQL + ") ";
			}
			query += " and xes.element_name='"+xsiType+"' ";
			query += " GROUP BY im.type, im.series_description ORDER BY im.type";
       		results = XFTTable.Execute(query, user.getDBName(), user.getUsername());
       		return results;
   	}


}
