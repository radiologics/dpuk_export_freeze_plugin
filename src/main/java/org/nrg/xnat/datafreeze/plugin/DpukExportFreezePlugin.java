package org.nrg.xnat.datafreeze.plugin;


import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import org.apache.activemq.command.ActiveMQQueue;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;

@XnatPlugin(value = "dpukExportFreezePlugin", name = "DPUK Export/Freeze", description = "DPUK Export/Freeze plugin for XNAT 1.7 that provides a data-freeze action for projects and the ability to share (push) data-freezes to a configured central xnat instance.")
@ComponentScan({"org.nrg.xnat.services.transfer"})
@Configuration
@EnableJms
public class DpukExportFreezePlugin {

    @Bean
    public DefaultJmsListenerContainerFactory exportFreezeListenerContainerFactory( @Qualifier("springConnectionFactory")
                                                                                    ConnectionFactory connectionFactory ) {
            DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
            factory.setConnectionFactory(connectionFactory);
            factory.setConcurrency("1-1");
            return factory;
    }

    @Bean(name="simpleTransferRequest")
    public Destination simpleTransferRequest(@Value("simpleTransferRequest") String simpleTransferRequest)
            throws JMSException {
        return new ActiveMQQueue(simpleTransferRequest);
    }

    @Bean(name="remoteTransferRequest")
    public Destination remoteTransferRequest(@Value("remoteTransferRequest") String remoteTransferRequest)
            throws JMSException {
        return new ActiveMQQueue(remoteTransferRequest);
    }
}
