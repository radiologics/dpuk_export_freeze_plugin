//Copyright (c) 2015 Radiologics, Inc
/**
 * 
 */
package org.nrg.xnat.datafreeze.utils;

import java.util.List;

import org.nrg.xft.XFTTable;

/**
 * @author Mohana Ramaratnam
 *
 */
public class DataFreezeUtils {
	public final static String NULL_VALUE_UI_REPRESENTATION="[all]";
	public final static String SEPARATOR = ",";


	/**
	 * Convenience method to display the groups as a comma separated string
	 * @param groupIds
	 * @return CSV String
	 */
	public static String toCSV(List<String> groupIds) {
		String rtn="";
		for (String group:groupIds) {
			rtn += group + SEPARATOR;
		}
		if (rtn.endsWith(SEPARATOR))
			rtn = rtn.substring(0,rtn.lastIndexOf(SEPARATOR));
		return rtn;
	}
	
	/**
	 * Convenience method to check if value supplied for a group is [none] (a mapping for NULL value)
	 * @param groupIds
	 * @return
	 */
	public static boolean hasNullValue(List<String> groupIds) {
		boolean rtn = false;
		for (String group:groupIds) {
			if (group.equals(NULL_VALUE_UI_REPRESENTATION)) {
				rtn = true;
				break;
			}
		}	
		return rtn;
	}

	/**
	 * Convenience method to check if value supplied for a group is [none] (a mapping for NULL value)
	 * @param groupIds
	 * @return
	 */
	public static boolean hasNullValue(String groupIdsCSV) {
		boolean rtn = false;
		String[] groupIds = groupIdsCSV.split(SEPARATOR);
		for (String group:groupIds) {
			if (group.equals(NULL_VALUE_UI_REPRESENTATION)) {
				rtn = true;
				break;
			}
		}	
		return rtn;
	}

	
	/**
	 * Convenience method to construct comma-separated and single-quoted string of groupIds
	 * @param groupIds
	 * @return
	 */
	public static String toCSVForSQL(List<String> groupIds) {
		String rtn="";
		for (String group:groupIds) {
			if (!group.equals(NULL_VALUE_UI_REPRESENTATION))
			   rtn += "'" + group +"'" + SEPARATOR;
		}
		if (rtn.endsWith(SEPARATOR))
			rtn = rtn.substring(0,rtn.lastIndexOf(SEPARATOR));
		return rtn;
	}

	/**
	 * Convenience method to construct comma-separated and single-quoted string of groupIds
	 * @param groupIds
	 * @return
	 */
	public static String toCSVForSQL(String groupIdsAsCSV) {
		String rtn="";
		String[] groupIds = groupIdsAsCSV.split(SEPARATOR);
		for (String group:groupIds) {
			if (!group.equals(NULL_VALUE_UI_REPRESENTATION))
			   rtn += "'" + group +"'" + SEPARATOR;
		}
		if (rtn.endsWith(SEPARATOR))
			rtn = rtn.substring(0,rtn.lastIndexOf(SEPARATOR));
		return rtn;
	}

	/**
	 * Sums up a column of a table 
	 * @param table
	 * @param columnName
	 * @return
	 */

	public static int getColumnSum(XFTTable table, String columnName) {
		int sum = 0;
		Integer columnIndex = table.getColumnIndex(columnName);
		if (columnIndex == null) {
			return sum;
		}
		if (table!=null) {
	    	  table.resetRowCursor();
		        while (table.hasMoreRows())
		        {
		            Object[] row = table.nextRow();
		            sum+= Integer.parseInt(""+row[columnIndex]);
		        }
	      }
		return sum;
	}

}
